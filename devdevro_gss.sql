-- phpMyAdmin SQL Dump 
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 08, 2015 at 11:07 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `devdevro_gss`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_functions`
--

CREATE TABLE IF NOT EXISTS `access_functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `access_functions`
--

INSERT INTO `access_functions` (`id`, `access_function`, `created_at`, `updated_at`) VALUES
(1, 'Users', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Customer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Asset', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Vendor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Order Request', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Order', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Completed Request', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_number` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `zip` int(10) unsigned DEFAULT NULL,
  `loan_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `agent` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `property_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `electric_status` tinyint(1) DEFAULT NULL,
  `water_status` tinyint(1) DEFAULT NULL,
  `gas_status` tinyint(1) DEFAULT NULL,
  `utility_note` longtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `property_address` text COLLATE utf8_unicode_ci,
  `lock_box` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brokage` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email_address` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbon_copy_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outbuilding_shed` tinyint(1) DEFAULT NULL,
  `outbuilding_shed_note` text COLLATE utf8_unicode_ci,
  `special_direction_note` text COLLATE utf8_unicode_ci,
  `swimming_pool` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assets_customer_id_foreign` (`customer_id`),
  KEY `assets_city_id_foreign` (`city_id`),
  KEY `assets_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `asset_number`, `customer_id`, `address`, `city_id`, `state_id`, `zip`, `loan_number`, `property_type`, `agent`, `property_status`, `electric_status`, `water_status`, `gas_status`, `utility_note`, `status`, `created_at`, `updated_at`, `property_address`, `lock_box`, `access_code`, `brokage`, `customer_email_address`, `carbon_copy_email`, `outbuilding_shed`, `outbuilding_shed_note`, `special_direction_note`, `swimming_pool`, `latitude`, `longitude`) VALUES
(8, 111223, 47, NULL, 7, 2, 75643, '', 'single-family', 'Test Agent', 'active', 1, 1, 1, '', 1, '2014-10-22 07:42:45', '2014-10-22 07:42:45', '123 Elm St', '7543', '7538', 'Test Broker', NULL, NULL, 1, 'None', 'Gate Access', 'pool', 60.9094, -161.431),
(9, 47409, 72, NULL, 2, 1, 32432, '24324', 'condo', 'Testing', 'active', 1, 1, 1, 'Testing', 1, '2014-12-09 03:52:23', '2014-12-09 03:52:23', 'Testing Address', '3248932', '8432903', '3928420', NULL, NULL, 1, 'Testing note', 'TEsting', 'pool', 33.2904, -87.1981),
(10, 295100, 73, NULL, 1, 1, 3242143, 'wqeqwq', 'condo', 'edasdas', 'active', 1, 1, 1, 'dfasdasfas', 1, '2014-12-25 04:59:35', '2015-01-01 03:18:02', '2118, National Drive', '41341212', '14321412', '3413241', '', NULL, 1, 'dsfasfdsf', 'fadsfdasfas', 'pool', 29.9746, -92.1343),
(11, 30711, 76, NULL, 2, 1, 75300, '12323', 'condo', 'GSS', 'active', 1, 1, 1, 'adfdsaads', 1, '2015-01-03 03:16:38', '2015-01-03 03:16:38', 'test Street, test Road etc', '123', '12345', '12345567', NULL, NULL, 1, 'fddsfd', 'dfdsfdsfdsa', 'spa', 33.2904, -87.1981),
(12, 97012, 77, NULL, 6, 2, 3434, '3', 'condo', '3', 'active', 1, 1, 1, '', 1, '2015-01-07 14:59:35', '2015-01-07 14:59:35', 'dfgdfsggdfgddf', '343', '34', '34', NULL, NULL, 1, 'dfdsfds', 'hg hghg', 'pool', 51.88, -176.658),
(13, 76413, 77, NULL, 2, 1, 1001, '3', 'condo', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-08 20:11:58', '2015-01-08 20:11:58', 'San Francisco, CA ', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'pool', 37.7749, -122.419);

-- --------------------------------------------------------

--
-- Table structure for table `assign_requests`
--

CREATE TABLE IF NOT EXISTS `assign_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `requested_service_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `assign_requests_request_id_foreign` (`request_id`),
  KEY `assign_requests_requested_service_id_foreign` (`requested_service_id`),
  KEY `assign_requests_vendor_id_foreign` (`vendor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `assign_requests`
--

INSERT INTO `assign_requests` (`id`, `request_id`, `requested_service_id`, `vendor_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 78, 3, '2015-01-08 14:44:20', '2015-01-08 14:45:20'),
(2, 2, 2, 78, 3, '2015-01-08 18:14:32', '2015-01-08 18:16:07'),
(3, 2, 3, 78, 3, '2015-01-08 18:14:32', '2015-01-08 18:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cities_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Abbeville', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Abernant', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Adamsville', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'Addison', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(5, 'Adger', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(6, 'Adak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(7, 'Akiachak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(8, 'Chefornak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(9, 'Buckland', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(10, 'Douglas', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(11, 'Aguila', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(12, 'Arlington', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(13, 'Catalina', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(14, 'Central', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(15, 'Concho', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(16, 'Acampo', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(17, 'Adelanto', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(18, 'Angwin', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(19, 'Baker', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(20, 'Big Bar', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_type_id` int(10) unsigned DEFAULT NULL,
  `image_path` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `images_image_type_id_foreign` (`image_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `image_types`
--

CREATE TABLE IF NOT EXISTS `image_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `invoices_order_id_foreign` (`order_id`),
  KEY `invoices_request_id_foreign` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_requests`
--

CREATE TABLE IF NOT EXISTS `maintenance_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `asset_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `maintenance_requests_customer_id_foreign` (`customer_id`),
  KEY `maintenance_requests_asset_id_foreign` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `maintenance_requests`
--

INSERT INTO `maintenance_requests` (`id`, `customer_id`, `asset_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 77, 12, 1, '2015-01-08 13:46:25', '2015-01-08 13:46:25'),
(2, 77, 12, 1, '2015-01-08 18:13:10', '2015-01-08 18:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_09_08_154521_user_types', 1),
('2014_09_08_155251_user_roles', 1),
('2014_09_08_155516_services', 1),
('2014_09_08_155709_states', 1),
('2014_09_08_155835_image_types', 1),
('2014_09_08_155947_notification_types', 1),
('2014_09_08_160153_role_functions', 1),
('2014_09_08_160725_users', 1),
('2014_09_08_161328_user_role_details', 1),
('2014_09_08_162630_cities', 1),
('2014_09_08_162953_assets', 1),
('2014_09_08_163438_maintenance_requests', 1),
('2014_09_08_163714_requested_services', 1),
('2014_09_08_164310_recurrings', 1),
('2014_09_08_164501_orders', 1),
('2014_09_08_165243_order_details', 1),
('2014_09_08_165602_vendor_services', 1),
('2014_09_08_165821_invoices', 1),
('2014_09_08_170246_order_images', 1),
('2014_09_08_170800_activities', 1),
('2014_09_08_171156_notifications', 1),
('2014_09_08_171607_images', 1),
('2014_09_08_171814_assign_requests', 1),
('2014_09_08_173019_special_prices', 1),
('2014_09_05_133221_update_asset', 2),
('2014_09_09_104018_update_asset1', 3),
('2014_09_09_122802_update_requested_service', 4),
('2014_09_09_150452_update_services', 5),
('2014_09_09_160756_update_orders', 6),
('2014_09_10_161556_update_users', 7),
('2014_09_10_162302_update_users1', 8),
('2014_09_11_140422_update_users2', 9),
('2014_09_15_120131_service_images', 10),
('2014_09_12_134717_update_requested_services', 11),
('2014_09_11_135947_update_role_functions', 12),
('2014_09_12_150713_create_access_functions', 13),
('2014_09_12_152122_update_role_function', 13),
('2014_09_17_160522_services_new_columns', 13),
('2014_09_19_093230_create_password_reminders_table', 14),
('2014_09_22_170135_userupdate22914', 15),
('2014_09_22_170511_updateassets22914', 15),
('2014_09_23_131509_UpdateUser', 16),
('2014_09_23_204319_update_service', 16),
('2014_09_24_144341_UpdateUserRoleDetail', 17),
('2014_09_24_110638_updateRequestedServices24914', 18),
('2014_09_24_111731_updateOrderDetails24914', 18),
('2014_09_24_112801_updateOrderDetails249142', 18),
('2014_09_25_154718_updateOrderImage', 18),
('2014_09_25_160502_updateOrderImage25914', 18),
('2014_09_26_132118_UpdateUserTable', 19),
('2014_09_26_140935_UpdateUserUsernameTable', 19),
('2014_09_30_073153_NotificationForeignKeyDrop', 19),
('2014_09_30_073721_UpdateRequestedFields', 19),
('2014_10_01_055955_update_service', 20),
('2014_10_01_073100_updateuser11014', 21),
('2014_10_01_081007_updaterolefunction11014', 21),
('2014_10_02_064130_AddUpdatedAddInNotification', 22),
('2014_10_02_135402_ChangeRecepientDataType', 22),
('2014_10_02_162224_EmailTemplateFieldInNotificationType', 22),
('2014_10_02_174049_NotificationTypeActivityTypeLength', 22),
('2014_10_02_182850_ChangeActivityIdToNotificationTypeId', 23),
('2014_10_01_130530_AddNotificationForeignKey', 24),
('2014_10_02_132113_updateuser21014', 25),
('2014_10_02_132651_updateasset21014', 25),
('2014_10_02_122708_updatespecialprice21014', 26),
('2014_09_30_060553_service_desc', 27),
('2014_12_05_190722_UpdateServiceColumn', 28);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned DEFAULT NULL,
  `recepient_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `notification_type_id` int(10) unsigned NOT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `notifications_activity_id_foreign` (`notification_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_id`, `recepient_id`, `message`, `notification_type_id`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 1, '1,2,3,39,41,42', 'has been registered.', 1, 1, '2014-10-02 20:39:54', '2014-10-02 20:39:54'),
(2, 1, '1,2,3,39,41,42', 'has been registered.', 1, 1, '2014-10-02 20:42:42', '2014-10-02 20:42:42'),
(3, 1, '1,2,3,39,41,42', 'New Customer has been registered.', 1, 1, '2014-10-22 07:20:42', '2014-10-22 07:20:42'),
(4, 1, '1,2', 'New Customer has been registered.', 1, 1, '2014-10-22 07:38:45', '2014-10-22 07:38:45'),
(5, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 00:15:59', '2014-12-09 00:15:59'),
(6, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 02:46:36', '2014-12-09 02:46:36'),
(7, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 02:52:53', '2014-12-09 02:52:53'),
(8, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 02:53:47', '2014-12-09 02:53:47'),
(9, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 02:56:15', '2014-12-09 02:56:15'),
(10, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 03:09:42', '2014-12-09 03:09:42'),
(11, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 03:13:06', '2014-12-09 03:13:06'),
(12, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 03:24:32', '2014-12-09 03:24:32'),
(13, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 03:26:54', '2014-12-09 03:26:54'),
(14, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-09 03:32:22', '2014-12-09 03:32:22'),
(15, 1, '1', 'New Customer has been registered.', 1, 1, '2014-12-25 04:18:17', '2014-12-25 04:18:17'),
(16, 1, '1', 'New Customer has been registered.', 1, 1, '2015-01-01 03:01:30', '2015-01-01 03:01:30'),
(17, 1, '1', 'New Customer has been registered.', 1, 1, '2015-01-01 03:10:02', '2015-01-01 03:10:02'),
(18, 1, '1', 'New Customer has been registered.', 1, 1, '2015-01-03 03:06:39', '2015-01-03 03:06:39'),
(19, 1, '1', 'New Customer has been registered.', 1, 1, '2015-01-07 13:48:23', '2015-01-07 13:48:23'),
(20, 1, '1', 'New Customer has been registered.', 1, 1, '2015-01-07 14:52:49', '2015-01-07 14:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `notification_types`
--

CREATE TABLE IF NOT EXISTS `notification_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_template` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `notification_types`
--

INSERT INTO `notification_types` (`id`, `activity_type`, `status`, `created_at`, `updated_at`, `email_template`) VALUES
(1, 'New Customer registered', 1, '2014-10-02 13:23:19', '0000-00-00 00:00:00', 'emails.notifications.new_registration');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_request_id_foreign` (`request_id`),
  KEY `orders_vendor_id_foreign` (`vendor_id`),
  KEY `orders_customer_id_foreign` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `request_id`, `vendor_id`, `total_amount`, `status`, `created_at`, `updated_at`, `customer_id`) VALUES
(1, 1, 78, NULL, 1, '2015-01-08 14:44:58', '2015-01-08 14:44:58', 77),
(2, 1, 78, NULL, 1, '2015-01-08 14:45:20', '2015-01-08 14:45:20', 77),
(3, 2, 78, NULL, 1, '2015-01-08 18:16:07', '2015-01-08 18:16:07', 77);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `requested_service_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_details_order_id_foreign` (`order_id`),
  KEY `order_details_requested_service_id_foreign` (`requested_service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `order_date`, `status`, `created_at`, `updated_at`, `requested_service_id`) VALUES
(1, 1, '2015-01-08 07:44:58', 1, '2015-01-08 14:44:58', '2015-01-08 14:44:58', NULL),
(2, 2, '2015-01-08 07:45:20', 1, '2015-01-08 14:45:20', '2015-01-08 14:45:20', NULL),
(3, 3, '2015-01-08 11:16:07', 1, '2015-01-08 18:16:07', '2015-01-08 18:16:07', NULL),
(4, 3, '2015-01-08 11:16:07', 1, '2015-01-08 18:16:07', '2015-01-08 18:16:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_images`
--

CREATE TABLE IF NOT EXISTS `order_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `type` enum('before','after') COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_details_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_images_order_id_foreign` (`order_id`),
  KEY `order_images_order_details_id_foreign` (`order_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_config`
--

CREATE TABLE IF NOT EXISTS `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `module` varchar(40) NOT NULL,
  `cfgkey` varchar(40) NOT NULL,
  `cfgval` varchar(40) NOT NULL,
  `cfgtype` varchar(40) NOT NULL,
  `cfgopts` text NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `quickbooks_log`
--

INSERT INTO `quickbooks_log` (`quickbooks_log_id`, `quickbooks_ticket_id`, `batch`, `msg`, `log_datetime`) VALUES
(1, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/vendor/consolibyte/quickbooks/docs/web_connector/example_web_connector.php'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 11:15:22'),
(2, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/qb'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 15:31:03'),
(3, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/qb'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-24 14:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_oauth`
--

CREATE TABLE IF NOT EXISTS `quickbooks_oauth` (
  `quickbooks_oauth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_username` varchar(255) NOT NULL,
  `app_tenant` varchar(255) NOT NULL,
  `oauth_request_token` varchar(255) DEFAULT NULL,
  `oauth_request_token_secret` varchar(255) DEFAULT NULL,
  `oauth_access_token` varchar(255) DEFAULT NULL,
  `oauth_access_token_secret` varchar(255) DEFAULT NULL,
  `qb_realm` varchar(32) DEFAULT NULL,
  `qb_flavor` varchar(12) DEFAULT NULL,
  `qb_user` varchar(64) DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_oauth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) NOT NULL,
  `msg` text,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quickbooks_queue`
--

INSERT INTO `quickbooks_queue` (`quickbooks_queue_id`, `quickbooks_ticket_id`, `qb_username`, `qb_action`, `ident`, `extra`, `qbxml`, `priority`, `qb_status`, `msg`, `enqueue_datetime`, `dequeue_datetime`) VALUES
(1, NULL, 'quickbooks', 'CustomerAdd', '1', NULL, NULL, 0, '1', NULL, '2014-09-24 11:43:15', '2014-09-24 11:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_recur`
--

CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `ticket` char(36) NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) DEFAULT NULL,
  `lasterror_msg` varchar(255) DEFAULT NULL,
  `ipaddr` char(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(255) NOT NULL,
  `qb_company_file` varchar(255) DEFAULT NULL,
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quickbooks_user`
--

INSERT INTO `quickbooks_user` (`qb_username`, `qb_password`, `qb_company_file`, `qbwc_wait_before_next_update`, `qbwc_min_run_every_n_seconds`, `status`, `write_datetime`, `touch_datetime`) VALUES
('quickbooks', '725cbb4eb5719aabdf405267531b8e8ac9ac454c', '', 0, 0, 'e', '2014-09-22 11:15:21', '2014-09-22 11:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `recurrings`
--

CREATE TABLE IF NOT EXISTS `recurrings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_service_id` int(10) unsigned DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `assignment_type` enum('single','multiple') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `recurrings_request_service_id_foreign` (`request_service_id`),
  KEY `recurrings_vendor_id_foreign` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `requested_services`
--

CREATE TABLE IF NOT EXISTS `requested_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `required_date` date DEFAULT NULL,
  `required_time` time DEFAULT NULL,
  `service_men` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_note` text COLLATE utf8_unicode_ci,
  `customer_note` text COLLATE utf8_unicode_ci,
  `vendor_note` text COLLATE utf8_unicode_ci,
  `verified_vacancy` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys_trash_out` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trash_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storage_shed` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requested_services_requested_id_foreign` (`request_id`),
  KEY `requested_services_service_id_foreign` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `requested_services`
--

INSERT INTO `requested_services` (`id`, `request_id`, `service_id`, `status`, `created_at`, `updated_at`, `required_date`, `required_time`, `service_men`, `service_note`, `customer_note`, `vendor_note`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`) VALUES
(1, 1, 26, 1, '2015-01-08 13:46:25', '2015-01-08 13:46:25', '2015-01-01', '01:00:00', '5', 'I need this work done', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 26, 1, '2015-01-08 18:13:10', '2015-01-08 18:13:10', '2015-01-08', '01:00:00', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 27, 1, '2015-01-08 18:13:10', '2015-01-08 18:13:10', NULL, NULL, NULL, '', NULL, NULL, '12', NULL, 'adfasd', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_functions`
--

CREATE TABLE IF NOT EXISTS `role_functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_function` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_function_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `role_functions`
--

INSERT INTO `role_functions` (`id`, `status`, `created_at`, `updated_at`, `role_function`, `access_function_id`, `deleted_at`) VALUES
(1, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'User', 1, NULL),
(2, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Access Level', 1, NULL),
(3, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Access Rights', 1, NULL),
(4, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Customer', 2, NULL),
(5, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Asset', 3, NULL),
(6, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Vendor', 4, NULL),
(7, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Maintenance Request', 5, NULL),
(8, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Service', 6, NULL),
(9, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Order', 7, NULL),
(10, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Completed Request', 8, NULL),
(11, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Invoice', 9, NULL),
(12, 1, '2014-10-03 14:43:07', '0000-00-00 00:00:00', 'Special Price', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_price` int(10) unsigned DEFAULT NULL,
  `vendor_price` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `req_date` tinyint(1) DEFAULT NULL,
  `number_of_men` tinyint(1) DEFAULT NULL,
  `verified_vacancy` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys_trash_out` tinyint(4) NOT NULL DEFAULT '0',
  `trash_size` tinyint(4) NOT NULL DEFAULT '0',
  `storage_shed` tinyint(4) NOT NULL DEFAULT '0',
  `lot_size` tinyint(4) NOT NULL DEFAULT '0',
  `emergency` tinyint(4) NOT NULL DEFAULT '0',
  `desc` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `customer_price`, `vendor_price`, `status`, `created_at`, `updated_at`, `service_code`, `req_date`, `number_of_men`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`, `emergency`, `desc`) VALUES
(25, 'interior / Exterior Painting', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:44:44', 'INT123', 1, 1, 1, 1, 0, 0, 0, 0, 0, ''),
(26, 'Flooring (Carpet/ Wood/ Tile)', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:45:14', 'FLO123', 1, 1, 0, 0, 0, 0, 0, 0, 0, ''),
(27, 'Drywall - New/ Repairs', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:45:43', 'DRY123', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, ''),
(28, 'Countertops/ Cabinets', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:13', 'COU123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(29, ' Carpentry', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:32', 'CAR123', NULL, NULL, 1, 1, 1, 1, 1, 1, 0, ''),
(30, 'Cleaning/ Trush Removal', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:46', 'CLE123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(31, 'Electrical', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'ELE123', NULL, 1, 1, 0, 1, 1, 0, 1, 0, '\r\n'),
(32, 'Garage Doors', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'GAR123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(33, 'Plumbing', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'PLU123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(34, 'Fences', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'FEN123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(35, 'Ceramic Tile', 0, NULL, 1, '2014-10-01 06:12:37', '0000-00-00 00:00:00', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, NULL),
(36, 'Roofing', 0, NULL, 1, '2014-10-01 06:12:37', '0000-00-00 00:00:00', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, NULL),
(38, 'Test Service', 100, 80, 1, '2014-12-06 02:33:11', '2014-12-06 02:33:11', 'test123', 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Test'),
(39, 'Rekey', 20, 5, 1, '2015-01-01 03:21:31', '2015-01-01 03:21:31', 'Rek123', 1, 1, 1, 1, 1, 1, 1, 0, 1, 'This is a test description'),
(40, 'test Service', 100, 60, 1, '2015-01-03 03:21:41', '2015-01-03 03:21:41', 'Code123', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 'test description');

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE IF NOT EXISTS `service_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `requested_id` int(10) unsigned DEFAULT NULL,
  `image_name` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `image_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `service_images_requested_id_foreign` (`requested_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `requested_id`, `image_name`, `status`, `image_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'nhuit56bed98fd36.jpg', 1, 0, '2015-01-08 13:46:25', '2015-01-08 13:46:25'),
(2, 2, 'nhuv4s5440049c2f.jpg', 1, 0, '2015-01-08 18:13:10', '2015-01-08 18:13:10'),
(3, 3, 'nhuv5p97c00bb012.jpg', 1, 0, '2015-01-08 18:13:10', '2015-01-08 18:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `special_prices`
--

CREATE TABLE IF NOT EXISTS `special_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `special_price` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `special_prices_customer_id_foreign` (`customer_id`),
  KEY `special_prices_service_id_foreign` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `special_prices`
--

INSERT INTO `special_prices` (`id`, `customer_id`, `service_id`, `special_price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, 31, 60, 1, '2015-01-01 03:24:22', '2015-01-01 03:24:22', NULL),
(2, 48, 33, 12, 1, '2015-01-06 17:25:28', '2015-01-06 17:25:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alabama', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Alaska', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Arizona', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'California', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_1` text COLLATE utf8_unicode_ci,
  `address_2` text COLLATE utf8_unicode_ci,
  `zipcode` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `user_role_id` int(10) unsigned DEFAULT NULL,
  `profile_status` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `profile_picture` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_type_id_foreign` (`type_id`),
  KEY `users_user_role_id_foreign` (`user_role_id`),
  KEY `users_city_id_foreign` (`city_id`),
  KEY `users_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=79 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `email`, `username`, `password`, `phone`, `address_1`, `address_2`, `zipcode`, `profile_image`, `type_id`, `user_role_id`, `profile_status`, `status`, `remember_token`, `created_at`, `updated_at`, `city_id`, `state_id`, `profile_picture`, `latitude`, `longitude`, `deleted_at`) VALUES
(1, 'Admin', 'admin', NULL, 'bilal.virtual@gmail.com', 'admin', '$2y$10$C5CALRtIlX4zuk5iusvrpOpJ377VDmzljpmQZf0Y5cb3TdFM/r.Lq', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, '1xhA5NAazJURuTcD86vXK3iRWmk0k61p4YJXLoenRcliZhKybLzWixGBcQ54', '2014-10-01 06:12:36', '2015-01-08 20:49:26', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Chassidy', 'Goolsby', 'GSS ', 'officemgr@gssreo.com', 'officemgr@gssreo.com', '$2y$10$1XyZm9qQfRbtpDquhWhWRuqj0i8x.4mVrae2H8tYUdFVn6NZGKT1a', '4694532032', '118 National Dr', '', '75448', NULL, 2, 2, 1, 1, NULL, '2014-10-22 07:20:42', '2014-10-22 07:41:07', 2, 1, NULL, NULL, NULL, NULL),
(48, 'Shery', 'khan', 'testing', 'abc123456yudkasj@gmail.com', 'shery', '$2y$10$vnfAIYTA53ksHCZrLWd7CuVMeS5OWvRJaDV4TsRIL4S7YFH8roYcq', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, NULL, '2014-10-22 07:38:45', '2014-10-22 07:38:45', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Shery', 'Test', 'Test', 'sheheryar@devronix.com', 'shery123', '$2y$10$EIW5eMxWy7r7Edcx.fDST.u8oXEib2jcW/.DXl/BV6dT5spAWbLBu', '23123142343', 'dfsafads', 'fasdfa', '75300', NULL, 2, 2, 1, 1, NULL, '2014-12-09 00:15:58', '2014-12-09 00:17:23', 3, 1, NULL, NULL, NULL, NULL),
(71, 'Muhammad', 'Bilal', 'abc', 'bilal.virtual+1@gmail.com', 'bilal.virtual+1', '$2y$10$3mR6VPLN/BYRzZjRlCxo8.dPjtZfm6D2O/eQPGJ3V5Z9xhfrLYEYi', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, 'yIlGrHitmc6FOHCmAw39OXbDZMtruCj0UlLeJzOnvcbF3bpVnBOHkWXG0E5w', '2014-12-09 03:26:54', '2014-12-09 03:39:49', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Muhammad', 'Bilal', 'abc', 'bilal.virtual+2@gmail.com', 'bilal.virtual+2', '$2y$10$DqyA4uQoRQV8BzHFncQoG.8ojtzy7F2TYEdJnUzXnfGXH/0FAitze', '23432879', 'this is testing address', '', '32893', NULL, 2, 2, 1, 1, NULL, '2014-12-09 03:32:21', '2014-12-09 03:49:11', 1, 1, NULL, NULL, NULL, NULL),
(73, 'test', 'test', 'test', 'sheheryar.alam.khan@gmail.com', 'cust', '$2y$10$U/pXnVPoJQb6AjSctd5G9uyqIywuWG3fbmzDeol0l3bQhhtMHQ.sK', '343242', 'sfdadsfdfdsadsafdasfadsfdasfadsfads', 'adfafadsfa', '143343', NULL, 2, 2, 1, 1, 'GTVBXNmLlN1vvwMlZomV9ZWGPDIQZjRDryp7Mz6fA3mjev7ArqGFr9ixvgAv', '2014-12-25 04:18:17', '2015-01-06 17:24:04', 2, 1, 'profile-emp-cust.jpg', NULL, NULL, NULL),
(74, 'Sheheryar', 'Khan', 'test company', 'test@gmail.com', 'customer', '$2y$10$vFXPfySUwwI6AqOoROOZ7O9GD6VeqpPOOnok96zRJShqj3vn..jGS', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, NULL, '2015-01-01 03:01:29', '2015-01-01 03:01:29', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'test', 'test', 'GSS REO', 'test@gmail.gss', 'testcustomer', '$2y$10$oys37Sn6s/R8Rp1sRqIxUOJJPo3Zgwi6ulaMCIOPmKkyj6JzdRKgG', '232132131', 'Test Test street, Test State ', 'Test City  test country', '876897', NULL, 2, 2, 1, 1, 'P8oZbLySxK9xA9OQIK9tykzHoiueUPEfE5Onu0NKgL1XK9hWz4nOqepvuyUo', '2015-01-01 03:10:02', '2015-01-01 03:16:19', 19, 4, 'profile-emp-testcustomer.jpg', NULL, NULL, NULL),
(76, 'Customer', 'One', 'GSS REO', 'sheheryar.dev@gmail.com', 'customer.dev', '$2y$10$C5CALRtIlX4zuk5iusvrpOpJ377VDmzljpmQZf0Y5cb3TdFM/r.Lq', '2545774247', 'Test Test street, Test State', 'Test City  test country', '75300', NULL, 2, 2, 1, 1, 'NfEgdDBMrIxStRLTO1OmBSc7llOxrBDe8gjfRDosWK8n91seNhTL606XaDGG', '2015-01-03 03:06:39', '2015-01-03 03:37:20', 5, 1, 'profile-dynammiKunstMap-customer.dev.png', NULL, NULL, NULL),
(77, 'testin', 'testing', 'devronix', 'azeem.devronix@gmail.com', 'azeem', '$2y$10$3tcl7NoTOyAUBp3HJwVoGe9aqcdpifRa94fph.bc7xi.WN/Xn3dYO', '2432', '324 jfhg ghf fgfgh fgf', '324 dsfadsfasdfads', '3432', NULL, 2, 2, 1, 1, 'rW6jvuPgMJjoEwy9WloXdt47jEx5tDKGG5pL4IdxbpjR05UBxY5lz0eYn35w', '2015-01-07 13:48:23', '2015-01-08 20:12:41', 1, 1, NULL, NULL, NULL, NULL),
(78, 'testin', 'testing', 'devronix', 'farhan.devronix@gmail.com', 'farhan', '$2y$10$3tcl7NoTOyAUBp3HJwVoGe9aqcdpifRa94fph.bc7xi.WN/Xn3dYO', '2432', '324 jfhg ghf fgfgh fgf', '324 dsfadsfasdfads', '3432', NULL, 3, 3, 1, 1, 'hQTlOYQJnddzadKohiwFos484uMmru7msbiz4sPfIKzNIh4eCibA8Rw2HNW6', '2015-01-07 14:52:49', '2015-01-08 20:12:56', 6, 2, NULL, 51.88, -176.658, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'This is the admin role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Customer', 'This is the customer role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Vendors', 'This is the vendor role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'Manager', 'This is the manager manaigin the services', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(5, 'Admin Assitant', 'He will be assisting the Admin', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_details`
--

CREATE TABLE IF NOT EXISTS `user_role_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `role_function_id` int(10) unsigned DEFAULT NULL,
  `add` tinyint(1) DEFAULT NULL,
  `edit` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_role_details_role_id_foreign` (`role_id`),
  KEY `user_role_details_role_function_id_foreign` (`role_function_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=61 ;

--
-- Dumping data for table `user_role_details`
--

INSERT INTO `user_role_details` (`id`, `role_id`, `role_function_id`, `add`, `edit`, `delete`, `status`, `created_at`, `updated_at`, `view`) VALUES
(1, 1, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(2, 1, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(3, 1, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(4, 1, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(5, 1, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(6, 1, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(7, 1, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(8, 1, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(9, 1, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(10, 1, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(11, 1, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(12, 2, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(13, 2, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(14, 2, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(15, 2, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(16, 2, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(17, 2, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(18, 2, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(19, 2, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(20, 2, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(21, 2, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(22, 2, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(23, 3, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(24, 3, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(25, 3, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(26, 3, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(27, 3, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(28, 3, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(29, 3, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(30, 3, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(31, 3, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(32, 3, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(33, 3, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(34, 4, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(35, 4, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-03 13:11:02', 1),
(36, 4, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-03 13:11:02', 1),
(37, 4, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(38, 4, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(39, 4, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(40, 4, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(41, 4, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(42, 4, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(43, 4, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(44, 4, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(45, 5, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(46, 5, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(47, 5, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(48, 5, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(49, 5, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(50, 5, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(51, 5, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(52, 5, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(53, 5, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(54, 5, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(55, 5, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(56, 1, 12, 1, 1, 1, NULL, '2014-10-03 14:43:57', '0000-00-00 00:00:00', 1),
(57, 2, 12, 0, 0, 0, NULL, '2014-10-03 14:47:25', '0000-00-00 00:00:00', 0),
(58, 3, 12, 0, 0, 0, NULL, '2014-10-03 14:47:36', '0000-00-00 00:00:00', 0),
(59, 4, 12, 0, 0, 0, NULL, '2014-10-03 14:47:39', '0000-00-00 00:00:00', 0),
(60, 5, 12, 0, 0, 0, NULL, '2014-10-03 14:47:42', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'customer', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'vendors', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'user', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_services`
--

CREATE TABLE IF NOT EXISTS `vendor_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `vendor_services_vendor_id_foreign` (`vendor_id`),
  KEY `vendor_services_service_id_foreign` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `vendor_services`
--

INSERT INTO `vendor_services` (`id`, `vendor_id`, `service_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 78, 25, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(2, 78, 26, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(3, 78, 27, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(4, 78, 28, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(5, 78, 29, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(6, 78, 30, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assign_requests`
--
ALTER TABLE `assign_requests`
  ADD CONSTRAINT `assign_requests_requested_service_id_foreign` FOREIGN KEY (`requested_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assign_requests_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assign_requests_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_image_type_id_foreign` FOREIGN KEY (`image_type_id`) REFERENCES `image_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `maintenance_requests`
--
ALTER TABLE `maintenance_requests`
  ADD CONSTRAINT `maintenance_requests_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `maintenance_requests_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_notification_type_id_foreign` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_requested_service_id_foreign` FOREIGN KEY (`requested_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_images`
--
ALTER TABLE `order_images`
  ADD CONSTRAINT `order_images_order_details_id_foreign` FOREIGN KEY (`order_details_id`) REFERENCES `order_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_images_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recurrings`
--
ALTER TABLE `recurrings`
  ADD CONSTRAINT `recurrings_request_service_id_foreign` FOREIGN KEY (`request_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recurrings_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `requested_services`
--
ALTER TABLE `requested_services`
  ADD CONSTRAINT `requested_services_requested_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `requested_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_requested_id_foreign` FOREIGN KEY (`requested_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `special_prices`
--
ALTER TABLE `special_prices`
  ADD CONSTRAINT `special_prices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `special_prices_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_role_details`
--
ALTER TABLE `user_role_details`
  ADD CONSTRAINT `user_role_details_role_function_id_foreign` FOREIGN KEY (`role_function_id`) REFERENCES `role_functions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_role_details_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vendor_services`
--
ALTER TABLE `vendor_services`
  ADD CONSTRAINT `vendor_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vendor_services_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
