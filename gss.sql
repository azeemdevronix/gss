-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2015 at 12:48 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gss`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_functions`
--

CREATE TABLE IF NOT EXISTS `access_functions` (
`id` int(10) unsigned NOT NULL,
  `access_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `access_functions`
--

INSERT INTO `access_functions` (`id`, `access_function`, `created_at`, `updated_at`) VALUES
(1, 'Users', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Customer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Asset', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Vendor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Order Request', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Order', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Completed Request', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
`id` int(10) unsigned NOT NULL,
  `asset_number` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `zip` int(10) unsigned DEFAULT NULL,
  `loan_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `agent` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `property_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `electric_status` tinyint(1) DEFAULT NULL,
  `water_status` tinyint(1) DEFAULT NULL,
  `gas_status` tinyint(1) DEFAULT NULL,
  `utility_note` longtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `property_address` text COLLATE utf8_unicode_ci,
  `lock_box` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brokage` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email_address` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbon_copy_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outbuilding_shed` tinyint(1) DEFAULT NULL,
  `outbuilding_shed_note` text COLLATE utf8_unicode_ci,
  `special_direction_note` text COLLATE utf8_unicode_ci,
  `swimming_pool` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `asset_number`, `customer_id`, `address`, `city_id`, `state_id`, `zip`, `loan_number`, `property_type`, `agent`, `property_status`, `electric_status`, `water_status`, `gas_status`, `utility_note`, `status`, `created_at`, `updated_at`, `property_address`, `lock_box`, `access_code`, `brokage`, `customer_email_address`, `carbon_copy_email`, `outbuilding_shed`, `outbuilding_shed_note`, `special_direction_note`, `swimming_pool`, `latitude`, `longitude`) VALUES
(8, 111223, 47, NULL, 7, 2, 0, '', 'single-family', 'Test Agent', 'active', 1, 1, 1, '', 1, '2014-10-22 07:42:45', '2015-01-14 14:32:23', '123 Elm St', '7543', '7538', 'Test Broker', '', NULL, 1, 'Nonetdst', 'Gate Access', 'pool', 60.9094, -161.431),
(9, 47409, 72, NULL, 2, 1, 32432, '24324', 'condo', 'Testing', 'active', 1, 1, 1, 'Testing', 1, '2014-12-09 03:52:23', '2015-01-14 14:21:19', 'Testing Address', '3248932', '8432903', '3928420', '', NULL, 1, 'Testing note', 'TEsting test', 'pool', 33.2904, -87.1981),
(10, 295100, 73, NULL, 1, 1, 3242143, 'wqeqwq', 'condo', 'edasdas', 'active', 1, 1, 1, 'dfasdasfas', 1, '2014-12-25 04:59:35', '2015-01-01 03:18:02', '2118, National Drive', '41341212', '14321412', '3413241', '', NULL, 1, 'dsfasfdsf', 'fadsfdasfas', 'pool', 29.9746, -92.1343),
(11, 30711, 76, NULL, 2, 1, 75300, '12323', 'condo', 'GSS', 'active', 1, 1, 1, 'adfdsaads', 1, '2015-01-03 03:16:38', '2015-01-03 03:16:38', 'test Street, test Road etc', '123', '12345', '12345567', NULL, NULL, 1, 'fddsfd', 'dfdsfdsfdsa', 'spa', 33.0884, -96.6833),
(12, 97012, 77, NULL, 21, 5, 75013, '3', 'condo', '3', 'active', 1, 1, 1, '', 1, '2015-01-07 14:59:35', '2015-01-14 17:15:38', '825 Market Street, Building M, Suite 250  Allen, TX 75013', '343', '34', '34', '', NULL, 1, 'dfdsfds', 'hg hghg', 'pool', 33.0884, -96.6833),
(13, 76413, 77, NULL, 2, 1, 1001, '3', 'condo', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-08 20:11:58', '2015-01-08 20:11:58', 'San Francisco, CA ', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'pool', 37.7749, -122.419),
(14, 22314, 61, NULL, 21, 5, 75013, '3', 'condo', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-12 02:50:01', '2015-01-12 02:50:01', '825 Market Street, Building M, Suite 250', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'spa', NULL, NULL),
(15, 58415, 77, NULL, 21, 5, 75013, '3', 'condo', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-12 03:00:02', '2015-01-12 03:00:02', '825 Market Street, Building M, Suite 250', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'pool', NULL, NULL),
(16, 11116, 77, NULL, 21, 5, 75013, '3', 'single-family', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-12 03:32:58', '2015-01-12 03:32:58', '825 Market Street, Building M, Suite 250', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'pool', 33.0884, -96.6833),
(17, 51817, 77, NULL, 21, 5, 75013, '3', 'condo', '3', 'active', 1, 1, 1, 'test', 1, '2015-01-13 02:27:58', '2015-01-13 02:27:58', '825 Market Street, Building M, Suite 250', '343', '34', '34', NULL, NULL, 1, 'test', 'test', 'pool', 33.0884, -96.6833),
(18, 11118, 77, NULL, 21, 5, 75013, '3', 'single-family', '3', 'active', 1, 1, 0, 'test', 1, '2015-01-13 02:29:16', '2015-01-14 14:58:26', '825 Market Street, Building M, Suite 250', '343', '34', '34', '', NULL, 1, 'test', 'test', 'pool', 33.0884, -96.6833),
(19, 32219, 76, NULL, 21, 5, 75013, '2fdfads', 'condo', 'adfadsf', 'active', 0, 0, 1, 'fadsfdsfsf', 1, '2015-01-13 21:55:59', '2015-01-13 21:55:59', '825 Market Street, Building M, Suite 250, Allen, TX 75013', ' 34324', '2324', '34232', NULL, NULL, 1, '23432dfsadsfadsfads', 'dfdasfadsfadsfa', 'pool', 33.0884, -96.6833),
(20, 53320, 83, NULL, 21, 5, 75032, '12321', 'condo', 'Chassidy', 'active', 1, 0, 1, 'dfadsfdstest', 1, '2015-01-14 05:42:38', '2015-01-14 13:48:24', '118 National Drive Rockwall Texas 75032', '12345', 'ddasfa', 'dfdafs', '', NULL, 1, 'dfafadsf', 'dfsaf', 'pool', 32.8913, -96.4416);

-- --------------------------------------------------------

--
-- Table structure for table `assign_requests`
--

CREATE TABLE IF NOT EXISTS `assign_requests` (
`id` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `requested_service_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assign_requests`
--

INSERT INTO `assign_requests` (`id`, `request_id`, `requested_service_id`, `vendor_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 78, 3, '2015-01-08 14:44:20', '2015-01-08 14:45:20'),
(2, 2, 2, 78, 3, '2015-01-08 18:14:32', '2015-01-08 18:16:07'),
(3, 2, 3, 78, 3, '2015-01-08 18:14:32', '2015-01-08 18:16:07'),
(4, 3, 4, 78, 2, '2015-01-09 08:11:29', '2015-01-09 08:15:42'),
(5, 3, 5, 78, 2, '2015-01-09 08:11:29', '2015-01-09 08:15:42'),
(6, 3, 6, 79, 3, '2015-01-09 08:14:01', '2015-01-09 08:18:08'),
(7, 3, 7, 79, 3, '2015-01-09 08:14:01', '2015-01-09 08:18:08'),
(8, 3, 5, 79, 3, '2015-01-09 08:17:21', '2015-01-09 08:18:08'),
(9, 3, 4, 79, 3, '2015-01-09 08:17:30', '2015-01-09 08:18:08'),
(10, 4, 8, 80, 3, '2015-01-12 04:05:51', '2015-01-12 04:06:51'),
(11, 4, 9, 80, 3, '2015-01-12 04:05:51', '2015-01-12 04:06:51'),
(12, 4, 10, 80, 3, '2015-01-12 04:05:51', '2015-01-12 04:06:51'),
(13, 6, 11, 80, 3, '2015-01-12 09:11:07', '2015-01-12 09:11:23'),
(14, 6, 12, 80, 3, '2015-01-12 09:11:07', '2015-01-12 09:11:23'),
(16, 8, 36, 80, 3, '2015-01-13 00:48:01', '2015-01-13 00:48:18'),
(27, 8, 49, 80, 3, '2015-01-13 01:17:48', '2015-01-13 01:17:48'),
(28, 8, 52, 80, 3, '2015-01-13 01:35:04', '2015-01-13 01:35:04'),
(29, 8, 53, 80, 3, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(30, 9, 54, 82, 3, '2015-01-13 22:07:32', '2015-01-13 22:07:52'),
(31, 9, 55, 82, 3, '2015-01-13 22:07:32', '2015-01-13 22:07:52'),
(32, 9, 56, 82, 3, '2015-01-13 22:07:32', '2015-01-13 22:07:52'),
(33, 9, 57, 82, 3, '2015-01-13 22:09:31', '2015-01-13 22:09:31'),
(34, 9, 58, 82, 3, '2015-01-13 22:09:40', '2015-01-13 22:09:40'),
(35, 9, 59, 82, 3, '2015-01-13 22:09:52', '2015-01-13 22:09:52'),
(36, 9, 60, 82, 3, '2015-01-13 22:13:22', '2015-01-13 22:13:22'),
(37, 9, 61, 82, 3, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(38, 9, 62, 82, 3, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(39, 9, 63, 82, 3, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(40, 9, 64, 82, 3, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(41, 9, 65, 82, 3, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(42, 9, 66, 82, 3, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(43, 9, 67, 82, 3, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(44, 9, 68, 82, 3, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(45, 9, 69, 82, 3, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(46, 9, 70, 82, 3, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(47, 9, 71, 82, 3, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(48, 9, 72, 82, 3, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(49, 9, 73, 82, 3, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(50, 9, 74, 82, 3, '2015-01-13 22:13:27', '2015-01-13 22:13:27'),
(51, 9, 75, 82, 3, '2015-01-13 22:13:27', '2015-01-13 22:13:27'),
(52, 9, 76, 82, 3, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(53, 9, 77, 82, 3, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(54, 9, 78, 82, 3, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(55, 9, 79, 82, 3, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(56, 9, 80, 82, 3, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(57, 9, 81, 82, 3, '2015-01-13 22:13:29', '2015-01-13 22:13:29'),
(58, 9, 82, 82, 3, '2015-01-13 22:13:29', '2015-01-13 22:13:29'),
(59, 9, 83, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(60, 9, 84, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(61, 9, 85, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(62, 9, 86, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(63, 9, 87, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(64, 9, 88, 82, 3, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(65, 9, 89, 82, 3, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(66, 9, 91, 82, 3, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(67, 9, 90, 82, 3, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(68, 9, 92, 82, 3, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(69, 10, 93, 80, 2, '2015-01-14 05:48:09', '2015-01-14 05:51:11'),
(70, 10, 94, 80, 2, '2015-01-14 05:48:09', '2015-01-14 05:51:11'),
(71, 10, 95, 82, 3, '2015-01-14 05:48:23', '2015-01-14 05:53:31'),
(72, 10, 96, 82, 3, '2015-01-14 05:55:51', '2015-01-14 05:55:51'),
(73, 10, 97, 82, 3, '2015-01-14 05:55:56', '2015-01-14 05:55:56'),
(74, 11, 98, 80, 3, '2015-01-14 18:44:51', '2015-01-14 18:49:52'),
(77, 27, 103, 80, 3, '2015-01-15 10:49:40', '2015-01-16 06:14:00'),
(78, 28, 104, 80, 2, '2015-01-15 10:59:09', '2015-01-16 03:02:30'),
(79, 29, 105, 80, 2, '2015-01-15 11:03:35', '2015-01-16 03:02:08'),
(80, 30, 106, 80, 2, '2015-01-15 11:06:53', '2015-01-16 03:01:53'),
(81, 31, 107, 80, 2, '2015-01-16 01:49:59', '2015-01-16 02:59:44'),
(82, 31, 108, 80, 2, '2015-01-16 01:49:59', '2015-01-16 02:59:44'),
(83, 32, 109, 80, 2, '2015-01-16 02:01:07', '2015-01-16 02:58:37'),
(84, 33, 110, 80, 2, '2015-01-16 02:01:31', '2015-01-16 02:57:51'),
(85, 34, 111, 80, 2, '2015-01-16 02:04:09', '2015-01-16 02:56:32'),
(86, 35, 112, 80, 2, '2015-01-16 02:05:23', '2015-01-16 02:52:54'),
(87, 36, 113, 80, 2, '2015-01-16 02:06:28', '2015-01-16 02:52:04'),
(88, 37, 114, 82, 3, '2015-01-16 02:07:37', '2015-01-19 04:54:57'),
(89, 41, 118, 80, 2, '2015-01-16 02:23:41', '2015-01-16 02:51:39'),
(90, 42, 119, 80, 2, '2015-01-16 03:03:00', '2015-01-16 03:03:18'),
(91, 43, 120, 80, 2, '2015-01-16 03:04:57', '2015-01-16 03:05:58'),
(92, 44, 121, 80, 2, '2015-01-16 03:11:28', '2015-01-16 03:11:56'),
(94, 45, 122, 80, 2, '2015-01-16 03:12:41', '2015-01-16 03:13:11'),
(96, 46, 123, 80, 2, '2015-01-16 03:17:24', '2015-01-16 03:17:45'),
(97, 46, 123, 82, 2, '2015-01-16 03:17:45', '2015-01-16 03:18:17'),
(98, 47, 124, 80, 2, '2015-01-16 03:24:48', '2015-01-16 03:25:23'),
(99, 47, 124, 82, 3, '2015-01-16 03:25:23', '2015-01-16 03:27:19'),
(100, 49, 126, 80, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:52'),
(101, 49, 127, 80, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:52'),
(102, 49, 128, 80, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:52'),
(103, 49, 129, 80, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:52'),
(104, 49, 130, 80, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:52'),
(105, 49, 126, 82, 2, '2015-01-19 03:30:52', '2015-01-19 03:31:44'),
(106, 49, 127, 82, 2, '2015-01-19 03:30:53', '2015-01-19 03:31:44'),
(107, 49, 128, 82, 2, '2015-01-19 03:30:53', '2015-01-19 03:31:44'),
(108, 49, 129, 82, 2, '2015-01-19 03:30:53', '2015-01-19 03:31:44'),
(109, 49, 130, 82, 2, '2015-01-19 03:30:53', '2015-01-19 03:31:44'),
(110, 50, 131, 80, 2, '2015-01-19 04:36:10', '2015-01-19 04:36:24'),
(111, 50, 132, 80, 2, '2015-01-19 04:36:10', '2015-01-19 04:36:24'),
(112, 50, 131, 82, 2, '2015-01-19 04:36:24', '2015-01-19 04:52:20'),
(113, 50, 132, 82, 2, '2015-01-19 04:36:24', '2015-01-19 04:52:20'),
(114, 37, 133, 82, 3, '2015-01-19 05:06:56', '2015-01-19 05:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `bid_requested_services`
--

CREATE TABLE IF NOT EXISTS `bid_requested_services` (
`id` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `maintenance_request_id` int(11) NOT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `required_date` date DEFAULT NULL,
  `required_time` time DEFAULT NULL,
  `service_men` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_note` text COLLATE utf8_unicode_ci,
  `customer_note` text COLLATE utf8_unicode_ci,
  `vendor_note` text COLLATE utf8_unicode_ci,
  `verified_vacancy` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys_trash_out` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trash_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storage_shed` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bid_requested_services`
--

INSERT INTO `bid_requested_services` (`id`, `request_id`, `maintenance_request_id`, `service_id`, `status`, `created_at`, `updated_at`, `required_date`, `required_time`, `service_men`, `service_note`, `customer_note`, `vendor_note`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`) VALUES
(1, 1, 8, 28, 1, '2015-01-13 00:48:51', '2015-01-13 00:48:51', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 8, 29, 1, '2015-01-13 01:22:24', '2015-01-13 01:22:24', NULL, NULL, NULL, 'test', NULL, NULL, '3', '32', '32', '32', '3232', '32'),
(3, 2, 8, 36, 1, '2015-01-13 01:22:25', '2015-01-13 01:22:25', '2015-01-01', '01:00:00', '23', 'st', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 3, 0, 39, 1, '2015-01-13 01:45:43', '2015-01-13 01:45:43', '2015-01-01', '01:00:00', '3', '', NULL, NULL, '3', '2', '2', '2', '2', NULL),
(5, 4, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(6, 5, 10, 31, 1, '2015-01-14 05:55:05', '2015-01-14 05:55:05', NULL, NULL, '12', 'Test Drive', NULL, NULL, '231', NULL, '12321', '232', NULL, '12321'),
(7, 6, 10, 26, 1, '2015-01-14 14:47:16', '2015-01-14 14:47:16', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 7, 37, 25, 1, '2015-01-19 05:06:24', '2015-01-19 05:06:24', '2015-01-01', '01:00:00', '3', 'tst', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL),
(9, 8, 11, 26, 1, '2015-01-19 05:08:54', '2015-01-19 05:08:54', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bid_requests`
--

CREATE TABLE IF NOT EXISTS `bid_requests` (
`id` int(10) unsigned NOT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `asset_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bid_requests`
--

INSERT INTO `bid_requests` (`id`, `vendor_id`, `customer_id`, `asset_id`, `status`, `created_at`, `updated_at`, `order_id`) VALUES
(1, 80, 77, 16, 2, '2015-01-13 00:48:51', '2015-01-13 01:17:48', 8),
(2, 80, 77, 16, 2, '2015-01-13 01:22:24', '2015-01-13 01:35:05', 8),
(3, 80, 77, 16, 3, '2015-01-13 01:45:43', '2015-01-13 01:46:52', 8),
(4, 82, 76, 19, 2, '2015-01-13 22:09:10', '2015-01-13 22:09:31', 9),
(5, 82, 83, 20, 2, '2015-01-14 05:55:05', '2015-01-14 05:55:51', 10),
(6, 82, 83, 20, 3, '2015-01-14 14:47:16', '2015-01-14 14:47:35', 10),
(7, 82, 77, 12, 2, '2015-01-19 05:06:24', '2015-01-19 05:06:56', 22),
(8, 80, 77, 12, 3, '2015-01-19 05:08:54', '2015-01-19 05:09:09', 19);

-- --------------------------------------------------------

--
-- Table structure for table `bid_service_images`
--

CREATE TABLE IF NOT EXISTS `bid_service_images` (
`id` int(10) unsigned NOT NULL,
  `requested_id` int(10) unsigned DEFAULT NULL,
  `image_name` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `image_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bid_service_images`
--

INSERT INTO `bid_service_images` (`id`, `requested_id`, `image_name`, `status`, `image_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'ni3phc35103254c5.jpg', 1, 0, '2015-01-13 00:48:51', '2015-01-13 00:48:51'),
(2, 2, 'ni3r0s62d06095c4.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(3, 3, 'ni3r1a3b4a9c1c00.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(4, 3, 'ni3r1a2e72f3fbdb.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(5, 3, 'ni3r1a2e72f3fbdb.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(6, 3, 'ni3r1aa0a4660b4d.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(7, 3, 'ni3r1a3d40cb6e16.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(8, 3, 'ni3r1a8e1f3ac302.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(9, 3, 'ni3r1a8e1f3ac302.jpg', 1, 0, '2015-01-13 01:22:25', '2015-01-13 01:22:25'),
(10, 4, 'ni3s45ef6ca0aa07.jpg', 1, 0, '2015-01-13 01:45:43', '2015-01-13 01:45:43'),
(11, 5, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:09:10', '2015-01-13 22:09:10'),
(12, 7, 'ni5pmff8cda14561.jpg', 1, 0, '2015-01-14 14:47:16', '2015-01-14 14:47:16'),
(13, 8, 'nif5em441c3a06a8.jpg', 1, 0, '2015-01-19 05:06:24', '2015-01-19 05:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Abbeville', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Abernant', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Adamsville', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'Addison', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(5, 'Adger', 1, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(6, 'Adak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(7, 'Akiachak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(8, 'Chefornak', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(9, 'Buckland', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(10, 'Douglas', 2, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(11, 'Aguila', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(12, 'Arlington', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(13, 'Catalina', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(14, 'Central', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(15, 'Concho', 3, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(16, 'Acampo', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(17, 'Adelanto', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(18, 'Angwin', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(19, 'Baker', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(20, 'Big Bar', 4, 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(21, 'Allen', 5, 1, '2015-01-12 07:47:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_requests`
--

CREATE TABLE IF NOT EXISTS `emergency_requests` (
`id` int(12) NOT NULL,
  `request_id` int(12) NOT NULL,
  `customer_id` int(12) NOT NULL,
  `status` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emergency_requests`
--

INSERT INTO `emergency_requests` (`id`, `request_id`, `customer_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 42, 77, 1, '2015-01-16 03:03:00', '2015-01-16 03:03:00'),
(2, 43, 77, 1, '2015-01-16 03:04:57', '2015-01-16 03:04:57'),
(3, 44, 77, 1, '2015-01-16 03:11:28', '2015-01-16 03:11:28'),
(4, 45, 77, 1, '2015-01-16 03:12:40', '2015-01-16 03:12:40'),
(5, 46, 77, 1, '2015-01-16 03:17:24', '2015-01-16 03:17:24'),
(6, 47, 77, 1, '2015-01-16 03:24:48', '2015-01-16 03:24:48'),
(7, 49, 77, 1, '2015-01-19 03:30:26', '2015-01-19 03:30:26'),
(8, 50, 77, 1, '2015-01-19 04:36:09', '2015-01-19 04:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_request_details`
--

CREATE TABLE IF NOT EXISTS `emergency_request_details` (
`id` int(10) unsigned NOT NULL,
  `emergency_request_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `request_id` int(12) DEFAULT NULL,
  `distance` float unsigned DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emergency_request_details`
--

INSERT INTO `emergency_request_details` (`id`, `emergency_request_id`, `vendor_id`, `request_id`, `distance`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 80, 42, 0.000402776, 1, '2015-01-16 03:03:00', '2015-01-16 03:03:00'),
(2, 1, 82, 42, 0.000402776, NULL, '2015-01-16 03:03:00', '2015-01-16 03:03:00'),
(3, 2, 80, 43, 0.000402776, 1, '2015-01-16 03:04:57', '2015-01-16 03:04:57'),
(4, 2, 82, 43, 0.000402776, NULL, '2015-01-16 03:04:57', '2015-01-16 03:04:57'),
(5, 3, 80, 44, 0.000402776, 1, '2015-01-16 03:11:28', '2015-01-16 03:11:28'),
(6, 3, 82, 44, 0.000402776, NULL, '2015-01-16 03:11:28', '2015-01-16 03:11:28'),
(7, 4, 80, 45, 0.000402776, 1, '2015-01-16 03:12:40', '2015-01-16 03:12:41'),
(8, 4, 82, 45, 0.000402776, NULL, '2015-01-16 03:12:41', '2015-01-16 03:12:41'),
(9, 5, 80, 46, 0.000402776, 1, '2015-01-16 03:17:24', '2015-01-16 03:17:24'),
(10, 5, 82, 46, 0.000402776, NULL, '2015-01-16 03:17:24', '2015-01-16 03:17:24'),
(11, 6, 80, 47, 0.000402776, 2, '2015-01-16 03:24:48', '2015-01-16 03:25:23'),
(12, 6, 82, 47, 0.000402776, 3, '2015-01-16 03:24:48', '2015-01-16 03:27:19'),
(13, 7, 80, 49, 0.000402776, 2, '2015-01-19 03:30:26', '2015-01-19 03:30:53'),
(14, 7, 82, 49, 0.000402776, 2, '2015-01-19 03:30:26', '2015-01-19 03:31:44'),
(15, 8, 80, 50, 0.000402776, 2, '2015-01-19 04:36:09', '2015-01-19 04:36:24'),
(16, 8, 82, 50, 0.000402776, 1, '2015-01-19 04:36:10', '2015-01-19 04:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(10) unsigned NOT NULL,
  `image_type_id` int(10) unsigned DEFAULT NULL,
  `image_path` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_types`
--

CREATE TABLE IF NOT EXISTS `image_types` (
`id` int(10) unsigned NOT NULL,
  `image_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
`id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_requests`
--

CREATE TABLE IF NOT EXISTS `maintenance_requests` (
`id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `asset_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `emergency_request` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `maintenance_requests`
--

INSERT INTO `maintenance_requests` (`id`, `customer_id`, `asset_id`, `status`, `emergency_request`, `created_at`, `updated_at`) VALUES
(1, 77, 12, 1, 0, '2015-01-08 13:46:25', '2015-01-08 13:46:25'),
(2, 77, 12, 1, 0, '2015-01-08 18:13:10', '2015-01-08 18:13:10'),
(3, 77, 12, 1, 0, '2015-01-09 08:10:51', '2015-01-09 08:10:51'),
(4, 77, 16, 1, 0, '2015-01-12 03:51:08', '2015-01-12 03:51:08'),
(6, 76, 11, 2, 0, '2015-01-12 09:08:09', '2015-01-19 03:28:59'),
(7, 77, 16, 1, 0, '2015-01-12 11:36:12', '2015-01-12 11:36:12'),
(8, 77, 16, 1, 0, '2015-01-13 00:47:36', '2015-01-13 00:47:36'),
(9, 76, 19, 1, 0, '2015-01-13 22:00:53', '2015-01-13 22:00:53'),
(10, 83, 20, 2, 0, '2015-01-14 05:46:09', '2015-01-16 06:11:50'),
(11, 77, 12, 1, 0, '2015-01-14 18:43:35', '2015-01-14 18:43:35'),
(12, 76, 19, 2, 0, '2015-01-15 07:45:43', '2015-01-19 02:58:30'),
(13, 76, 19, 1, 0, '2015-01-15 07:48:54', '2015-01-15 07:48:54'),
(14, 76, 19, 1, 0, '2015-01-15 07:49:51', '2015-01-15 07:49:51'),
(24, 76, 19, 1, 1, '2015-01-15 10:10:09', '2015-01-15 10:10:09'),
(25, 76, 19, 1, 1, '2015-01-15 10:10:38', '2015-01-15 10:10:38'),
(26, 77, 18, 1, 1, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(27, 77, 18, 1, 1, '2015-01-15 10:49:40', '2015-01-15 10:49:40'),
(28, 77, 18, 1, 1, '2015-01-15 10:59:08', '2015-01-15 10:59:08'),
(29, 77, 18, 1, 1, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(30, 77, 18, 1, 1, '2015-01-15 11:06:52', '2015-01-15 11:06:52'),
(31, 77, 18, 1, 1, '2015-01-16 01:49:58', '2015-01-16 01:49:58'),
(32, 77, 18, 1, 1, '2015-01-16 02:01:07', '2015-01-16 02:01:07'),
(33, 77, 18, 1, 1, '2015-01-16 02:01:31', '2015-01-16 02:01:31'),
(34, 77, 18, 1, 1, '2015-01-16 02:04:08', '2015-01-16 02:04:08'),
(35, 77, 18, 1, 1, '2015-01-16 02:05:23', '2015-01-16 02:05:23'),
(36, 77, 12, 1, 1, '2015-01-16 02:06:28', '2015-01-16 02:06:28'),
(37, 77, 12, 1, 1, '2015-01-16 02:07:37', '2015-01-16 02:07:37'),
(38, 77, 12, 1, 1, '2015-01-16 02:20:02', '2015-01-16 02:20:02'),
(39, 77, 12, 1, 1, '2015-01-16 02:20:57', '2015-01-16 02:20:57'),
(40, 77, 12, 1, 1, '2015-01-16 02:21:09', '2015-01-16 02:21:09'),
(41, 77, 12, 1, 1, '2015-01-16 02:23:41', '2015-01-16 02:23:41'),
(42, 77, 12, 1, 1, '2015-01-16 03:03:00', '2015-01-16 03:03:00'),
(43, 77, 12, 1, 1, '2015-01-16 03:04:57', '2015-01-16 03:04:57'),
(44, 77, 12, 1, 1, '2015-01-16 03:11:28', '2015-01-16 03:11:28'),
(45, 77, 12, 1, 1, '2015-01-16 03:12:40', '2015-01-16 03:12:40'),
(46, 77, 12, 1, 1, '2015-01-16 03:17:24', '2015-01-16 03:17:24'),
(47, 77, 12, 1, 1, '2015-01-16 03:24:47', '2015-01-16 03:24:47'),
(48, 77, 12, 2, 0, '2015-01-16 03:25:09', '2015-01-19 02:34:01'),
(49, 77, 18, 2, 1, '2015-01-19 03:30:25', '2015-01-19 04:35:17'),
(50, 77, 12, 2, 1, '2015-01-19 04:36:09', '2015-01-19 08:48:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_09_08_154521_user_types', 1),
('2014_09_08_155251_user_roles', 1),
('2014_09_08_155516_services', 1),
('2014_09_08_155709_states', 1),
('2014_09_08_155835_image_types', 1),
('2014_09_08_155947_notification_types', 1),
('2014_09_08_160153_role_functions', 1),
('2014_09_08_160725_users', 1),
('2014_09_08_161328_user_role_details', 1),
('2014_09_08_162630_cities', 1),
('2014_09_08_162953_assets', 1),
('2014_09_08_163438_maintenance_requests', 1),
('2014_09_08_163714_requested_services', 1),
('2014_09_08_164310_recurrings', 1),
('2014_09_08_164501_orders', 1),
('2014_09_08_165243_order_details', 1),
('2014_09_08_165602_vendor_services', 1),
('2014_09_08_165821_invoices', 1),
('2014_09_08_170246_order_images', 1),
('2014_09_08_170800_activities', 1),
('2014_09_08_171156_notifications', 1),
('2014_09_08_171607_images', 1),
('2014_09_08_171814_assign_requests', 1),
('2014_09_08_173019_special_prices', 1),
('2014_09_05_133221_update_asset', 2),
('2014_09_09_104018_update_asset1', 3),
('2014_09_09_122802_update_requested_service', 4),
('2014_09_09_150452_update_services', 5),
('2014_09_09_160756_update_orders', 6),
('2014_09_10_161556_update_users', 7),
('2014_09_10_162302_update_users1', 8),
('2014_09_11_140422_update_users2', 9),
('2014_09_15_120131_service_images', 10),
('2014_09_12_134717_update_requested_services', 11),
('2014_09_11_135947_update_role_functions', 12),
('2014_09_12_150713_create_access_functions', 13),
('2014_09_12_152122_update_role_function', 13),
('2014_09_17_160522_services_new_columns', 13),
('2014_09_19_093230_create_password_reminders_table', 14),
('2014_09_22_170135_userupdate22914', 15),
('2014_09_22_170511_updateassets22914', 15),
('2014_09_23_131509_UpdateUser', 16),
('2014_09_23_204319_update_service', 16),
('2014_09_24_144341_UpdateUserRoleDetail', 17),
('2014_09_24_110638_updateRequestedServices24914', 18),
('2014_09_24_111731_updateOrderDetails24914', 18),
('2014_09_24_112801_updateOrderDetails249142', 18),
('2014_09_25_154718_updateOrderImage', 18),
('2014_09_25_160502_updateOrderImage25914', 18),
('2014_09_26_132118_UpdateUserTable', 19),
('2014_09_26_140935_UpdateUserUsernameTable', 19),
('2014_09_30_073153_NotificationForeignKeyDrop', 19),
('2014_09_30_073721_UpdateRequestedFields', 19),
('2014_10_01_055955_update_service', 20),
('2014_10_01_073100_updateuser11014', 21),
('2014_10_01_081007_updaterolefunction11014', 21),
('2014_10_02_064130_AddUpdatedAddInNotification', 22),
('2014_10_02_135402_ChangeRecepientDataType', 22),
('2014_10_02_162224_EmailTemplateFieldInNotificationType', 22),
('2014_10_02_174049_NotificationTypeActivityTypeLength', 22),
('2014_10_02_182850_ChangeActivityIdToNotificationTypeId', 23),
('2014_10_01_130530_AddNotificationForeignKey', 24),
('2014_10_02_132113_updateuser21014', 25),
('2014_10_02_132651_updateasset21014', 25),
('2014_10_02_122708_updatespecialprice21014', 26),
('2014_09_30_060553_service_desc', 27),
('2014_12_05_190722_UpdateServiceColumn', 28),
('2015_01_15_140845_emergency_requests_table', 29),
('2015_01_15_143015_emergency_requests_details_table', 30);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
`id` int(10) unsigned NOT NULL,
  `sender_id` int(10) unsigned DEFAULT NULL,
  `recepient_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `notification_type_id` int(10) unsigned NOT NULL,
  `notification_url` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_id`, `recepient_id`, `message`, `notification_type_id`, `notification_url`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-19 09:11:48', '2015-01-19 09:11:48'),
(2, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-19 09:11:49', '2015-01-19 09:11:49'),
(3, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-19 09:57:55', '2015-01-19 09:57:55'),
(4, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-19 09:57:55', '2015-01-19 09:57:55'),
(5, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 1, '2015-01-19 09:57:57', '2015-01-19 09:57:57'),
(6, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 1, '2015-01-19 09:57:57', '2015-01-19 09:57:57'),
(7, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 0, '2015-01-20 02:43:26', '2015-01-20 06:09:35'),
(8, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-20 02:43:26', '2015-01-20 02:43:26'),
(9, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 0, '2015-01-20 02:49:40', '2015-01-20 06:08:27'),
(10, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 1, '2015-01-20 02:49:40', '2015-01-20 02:49:40'),
(11, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 0, '2015-01-20 02:50:00', '2015-01-20 06:13:30'),
(12, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to In-Progress of order number 21', 2, '', 1, '2015-01-20 02:50:00', '2015-01-20 02:50:00'),
(13, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 0, '2015-01-20 04:09:49', '2015-01-20 06:09:40'),
(14, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 1, '2015-01-20 04:09:49', '2015-01-20 04:09:49'),
(15, 80, '77', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 21', 2, '', 0, '2015-01-20 04:09:49', '2015-01-20 06:03:15'),
(16, 77, '80', 'testin testing has changed the status of order to Approved of order number 21', 2, '', 0, '2015-01-20 04:13:27', '2015-01-20 05:59:29'),
(17, 1, '80', 'Admin admin has changed the status of order to Completed of order number 21', 2, '', 0, '2015-01-20 04:25:17', '2015-01-20 05:59:26'),
(18, 1, '80', 'Admin admin has changed the status of order to In-Progress of order number 21', 2, '', 0, '2015-01-20 04:25:26', '2015-01-20 05:59:23'),
(19, 1, '80', 'Admin admin has changed the status of order to Completed of order number 21', 2, '', 0, '2015-01-20 04:26:58', '2015-01-20 05:59:16'),
(20, 1, '1', 'New Customer has been registered.', 1, '', 0, '2015-01-20 04:41:39', '2015-01-20 06:08:20'),
(21, 84, '84', 'New Customer has been registered.', 1, '', 1, '2015-01-20 04:41:39', '2015-01-20 04:41:39'),
(22, 77, '82', 'testin testing has changed the status of order to Completed of order number 22', 2, NULL, 1, '2015-01-20 05:30:39', '2015-01-20 05:30:39'),
(23, 77, '82', 'testin testing has changed the status of order to Completed of order number 22', 2, 'vendor-list-orders', 1, '2015-01-20 05:31:51', '2015-01-20 05:31:51'),
(24, 1, '80', 'Admin admin has changed the status of order to Completed of order number 19', 2, 'vendor-list-orders', 0, '2015-01-20 05:34:28', '2015-01-20 05:55:46'),
(25, 77, '80', 'testin testing has changed the status of order to Completed of order number 18', 2, 'vendor-list-orders', 0, '2015-01-20 06:03:25', '2015-01-20 06:03:35'),
(26, 1, '80', 'Admin admin has changed the status of order to Completed of order number 17', 2, 'vendor-list-orders', 0, '2015-01-20 06:10:53', '2015-01-20 06:11:07'),
(27, 80, '1', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 16', 2, 'list-work-order-admin', 0, '2015-01-20 06:12:20', '2015-01-20 06:13:22'),
(28, 80, '84', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 16', 2, 'list-work-order-admin', 1, '2015-01-20 06:12:20', '2015-01-20 06:12:20'),
(29, 80, '77', 'Vendor Azeem1231 testing has changed the status of order to Completed of order number 16', 2, 'customer-list-work-orders', 1, '2015-01-20 06:12:20', '2015-01-20 06:12:20'),
(30, 1, '80', 'Admin admin has changed the status of order to In-Progress of order number 16', 2, 'vendor-list-orders', 0, '2015-01-20 06:14:12', '2015-01-20 06:14:30'),
(31, 1, '80', 'Admin admin has changed the status of order to Approved of order number 16', 2, 'vendor-list-orders', 0, '2015-01-20 06:14:17', '2015-01-20 06:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `notification_types`
--

CREATE TABLE IF NOT EXISTS `notification_types` (
`id` int(10) unsigned NOT NULL,
  `activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_template` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_types`
--

INSERT INTO `notification_types` (`id`, `activity_type`, `status`, `created_at`, `updated_at`, `email_template`) VALUES
(1, 'New Customer registered', 1, '2014-10-02 13:23:19', '0000-00-00 00:00:00', 'emails.notifications.new_registration'),
(2, 'Order Completed', 1, '2015-01-19 12:39:55', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `status_class` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_text` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `request_id`, `vendor_id`, `total_amount`, `status`, `status_class`, `status_text`, `created_at`, `updated_at`, `customer_id`) VALUES
(1, 1, 78, NULL, 1, NULL, NULL, '2015-01-08 14:44:58', '2015-01-08 14:44:58', 77),
(2, 1, 78, NULL, 1, NULL, NULL, '2015-01-08 14:45:20', '2015-01-08 14:45:20', 77),
(3, 2, 78, NULL, 1, NULL, NULL, '2015-01-08 18:16:07', '2015-01-08 18:16:07', 77),
(4, 3, 79, NULL, 1, 'warning', 'In-Progress', '2015-01-09 08:18:08', '2015-01-19 08:33:30', 77),
(5, 4, 80, NULL, 1, NULL, NULL, '2015-01-12 04:06:51', '2015-01-12 06:52:25', 77),
(6, 6, 80, NULL, 1, NULL, NULL, '2015-01-12 09:11:23', '2015-01-12 09:11:23', 76),
(7, 7, 80, NULL, 1, NULL, NULL, '2015-01-12 11:36:51', '2015-01-12 11:36:51', 77),
(8, 8, 80, NULL, 1, NULL, NULL, '2015-01-13 00:48:18', '2015-01-13 00:48:18', 77),
(9, 9, 82, NULL, 3, 'success', '0', '2015-01-13 22:07:52', '2015-01-15 18:03:00', 76),
(10, 10, 82, NULL, 2, 'blue', '0', '2015-01-14 05:53:31', '2015-01-15 19:13:52', 83),
(11, 11, 80, NULL, 1, NULL, NULL, '2015-01-14 18:49:17', '2015-01-14 18:49:17', 77),
(12, 11, 80, NULL, 1, NULL, NULL, '2015-01-14 18:49:45', '2015-01-14 18:49:45', 77),
(13, 11, 80, NULL, 1, NULL, NULL, '2015-01-14 18:49:46', '2015-01-14 18:49:46', 77),
(14, 11, 80, NULL, 1, NULL, NULL, '2015-01-14 18:49:47', '2015-01-14 18:49:47', 77),
(15, 11, 80, NULL, 1, NULL, NULL, '2015-01-14 18:49:48', '2015-01-14 18:49:48', 77),
(16, 11, 80, NULL, 4, 'gray', 'Approved', '2015-01-14 18:49:49', '2015-01-20 06:14:17', 77),
(17, 11, 80, NULL, 2, 'blue', 'Completed', '2015-01-14 18:49:50', '2015-01-20 06:10:53', 77),
(18, 11, 80, NULL, 2, 'blue', 'Completed', '2015-01-14 18:49:51', '2015-01-20 06:03:25', 77),
(19, 11, 80, NULL, 2, 'blue', 'Completed', '2015-01-14 18:49:52', '2015-01-20 05:34:28', 77),
(20, 47, 82, NULL, 2, 'blue', 'Completed', '2015-01-16 03:27:19', '2015-01-19 05:36:20', 77),
(21, 27, 80, NULL, 2, 'blue', 'Completed', '2015-01-16 06:14:00', '2015-01-20 04:26:58', 77),
(22, 37, 82, NULL, 2, 'blue', 'Completed', '2015-01-19 04:54:58', '2015-01-20 05:31:51', 77);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
`id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `requested_service_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `order_date`, `status`, `created_at`, `updated_at`, `requested_service_id`) VALUES
(5, 4, '2015-01-09 13:18:09', 1, '2015-01-09 08:18:09', '2015-01-09 08:18:09', 6),
(6, 4, '2015-01-09 13:18:09', 1, '2015-01-09 08:18:09', '2015-01-09 08:18:09', 7),
(7, 4, '2015-01-09 13:18:09', 1, '2015-01-09 08:18:09', '2015-01-09 08:18:09', 5),
(8, 4, '2015-01-09 13:18:09', 1, '2015-01-09 08:18:09', '2015-01-09 08:18:09', 4),
(9, 5, '2015-01-12 09:06:51', 1, '2015-01-12 04:06:51', '2015-01-12 04:06:51', 8),
(10, 5, '2015-01-12 09:06:51', 1, '2015-01-12 04:06:51', '2015-01-12 04:06:51', 9),
(11, 5, '2015-01-12 09:06:51', 1, '2015-01-12 04:06:51', '2015-01-12 04:06:51', 10),
(12, 6, '2015-01-12 14:11:23', 1, '2015-01-12 09:11:23', '2015-01-12 09:11:23', 11),
(13, 6, '2015-01-12 14:11:23', 1, '2015-01-12 09:11:23', '2015-01-12 09:11:23', 12),
(16, 8, '2015-01-13 05:48:18', 1, '2015-01-13 00:48:18', '2015-01-13 00:48:18', 36),
(24, 8, '2015-01-13 06:17:48', 1, '2015-01-13 01:17:48', '2015-01-13 01:17:48', 49),
(25, 8, '2015-01-13 06:35:04', 1, '2015-01-13 01:35:04', '2015-01-13 01:35:04', 52),
(26, 8, '2015-01-13 06:35:05', 1, '2015-01-13 01:35:05', '2015-01-13 01:35:05', 53),
(27, 9, '2015-01-13 15:07:52', 1, '2015-01-13 22:07:52', '2015-01-13 22:07:52', 54),
(28, 9, '2015-01-13 15:07:52', 1, '2015-01-13 22:07:52', '2015-01-13 22:07:52', 55),
(29, 9, '2015-01-13 15:07:52', 1, '2015-01-13 22:07:52', '2015-01-13 22:07:52', 56),
(30, 9, '2015-01-13 15:09:31', 1, '2015-01-13 22:09:31', '2015-01-13 22:09:31', 57),
(31, 9, '2015-01-13 15:09:40', 1, '2015-01-13 22:09:40', '2015-01-13 22:09:40', 58),
(32, 9, '2015-01-13 15:09:52', 1, '2015-01-13 22:09:52', '2015-01-13 22:09:52', 59),
(33, 9, '2015-01-13 15:13:22', 1, '2015-01-13 22:13:22', '2015-01-13 22:13:22', 60),
(34, 9, '2015-01-13 15:13:23', 1, '2015-01-13 22:13:23', '2015-01-13 22:13:23', 61),
(35, 9, '2015-01-13 15:13:23', 1, '2015-01-13 22:13:23', '2015-01-13 22:13:23', 62),
(36, 9, '2015-01-13 15:13:23', 1, '2015-01-13 22:13:23', '2015-01-13 22:13:23', 63),
(37, 9, '2015-01-13 15:13:23', 1, '2015-01-13 22:13:23', '2015-01-13 22:13:23', 64),
(38, 9, '2015-01-13 15:13:23', 1, '2015-01-13 22:13:23', '2015-01-13 22:13:23', 65),
(39, 9, '2015-01-13 15:13:24', 1, '2015-01-13 22:13:24', '2015-01-13 22:13:24', 66),
(40, 9, '2015-01-13 15:13:24', 1, '2015-01-13 22:13:24', '2015-01-13 22:13:24', 67),
(41, 9, '2015-01-13 15:13:24', 1, '2015-01-13 22:13:24', '2015-01-13 22:13:24', 68),
(42, 9, '2015-01-13 15:13:24', 1, '2015-01-13 22:13:24', '2015-01-13 22:13:24', 69),
(43, 9, '2015-01-13 15:13:26', 1, '2015-01-13 22:13:26', '2015-01-13 22:13:26', 70),
(44, 9, '2015-01-13 15:13:26', 1, '2015-01-13 22:13:26', '2015-01-13 22:13:26', 71),
(45, 9, '2015-01-13 15:13:26', 1, '2015-01-13 22:13:26', '2015-01-13 22:13:26', 72),
(46, 9, '2015-01-13 15:13:26', 1, '2015-01-13 22:13:26', '2015-01-13 22:13:26', 73),
(47, 9, '2015-01-13 15:13:27', 1, '2015-01-13 22:13:27', '2015-01-13 22:13:27', 74),
(48, 9, '2015-01-13 15:13:27', 1, '2015-01-13 22:13:27', '2015-01-13 22:13:27', 75),
(49, 9, '2015-01-13 15:13:28', 1, '2015-01-13 22:13:28', '2015-01-13 22:13:28', 76),
(50, 9, '2015-01-13 15:13:28', 1, '2015-01-13 22:13:28', '2015-01-13 22:13:28', 77),
(51, 9, '2015-01-13 15:13:28', 1, '2015-01-13 22:13:28', '2015-01-13 22:13:28', 78),
(52, 9, '2015-01-13 15:13:28', 1, '2015-01-13 22:13:28', '2015-01-13 22:13:28', 79),
(53, 9, '2015-01-13 15:13:28', 1, '2015-01-13 22:13:28', '2015-01-13 22:13:28', 80),
(54, 9, '2015-01-13 15:13:29', 1, '2015-01-13 22:13:29', '2015-01-13 22:13:29', 81),
(55, 9, '2015-01-13 15:13:29', 1, '2015-01-13 22:13:29', '2015-01-13 22:13:29', 82),
(56, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 83),
(57, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 84),
(58, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 85),
(59, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 86),
(60, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 87),
(61, 9, '2015-01-13 15:13:30', 1, '2015-01-13 22:13:30', '2015-01-13 22:13:30', 88),
(62, 9, '2015-01-13 15:13:32', 1, '2015-01-13 22:13:32', '2015-01-13 22:13:32', 89),
(63, 9, '2015-01-13 15:13:32', 1, '2015-01-13 22:13:32', '2015-01-13 22:13:32', 91),
(64, 9, '2015-01-13 15:13:32', 1, '2015-01-13 22:13:32', '2015-01-13 22:13:32', 90),
(65, 9, '2015-01-13 15:13:32', 1, '2015-01-13 22:13:32', '2015-01-13 22:13:32', 92),
(66, 10, '2015-01-13 22:53:31', 1, '2015-01-14 05:53:31', '2015-01-14 05:53:31', 95),
(67, 10, '2015-01-13 22:55:51', 1, '2015-01-14 05:55:51', '2015-01-14 05:55:51', 96),
(68, 10, '2015-01-13 22:55:56', 1, '2015-01-14 05:55:56', '2015-01-14 05:55:56', 97),
(69, 11, '2015-01-14 11:49:17', 1, '2015-01-14 18:49:17', '2015-01-14 18:49:17', 98),
(70, 12, '2015-01-14 11:49:45', 1, '2015-01-14 18:49:45', '2015-01-14 18:49:45', 98),
(71, 13, '2015-01-14 11:49:46', 1, '2015-01-14 18:49:46', '2015-01-14 18:49:46', 98),
(72, 14, '2015-01-14 11:49:47', 1, '2015-01-14 18:49:47', '2015-01-14 18:49:47', 98),
(73, 15, '2015-01-14 11:49:48', 1, '2015-01-14 18:49:48', '2015-01-14 18:49:48', 98),
(74, 16, '2015-01-14 11:49:49', 1, '2015-01-14 18:49:49', '2015-01-14 18:49:49', 98),
(75, 17, '2015-01-14 11:49:50', 1, '2015-01-14 18:49:50', '2015-01-14 18:49:50', 98),
(76, 18, '2015-01-14 11:49:51', 1, '2015-01-14 18:49:51', '2015-01-14 18:49:51', 98),
(77, 19, '2015-01-14 11:49:52', 1, '2015-01-14 18:49:52', '2015-01-14 18:49:52', 98),
(78, 20, '2015-01-16 08:27:19', 1, '2015-01-16 03:27:19', '2015-01-16 03:27:19', 124),
(79, 21, '2015-01-16 11:14:00', 1, '2015-01-16 06:14:00', '2015-01-16 06:14:00', 103),
(80, 22, '2015-01-19 09:54:58', 1, '2015-01-19 04:54:58', '2015-01-19 04:54:58', 114),
(81, 22, '2015-01-19 10:06:56', 1, '2015-01-19 05:06:56', '2015-01-19 05:06:56', 133);

-- --------------------------------------------------------

--
-- Table structure for table `order_images`
--

CREATE TABLE IF NOT EXISTS `order_images` (
`id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `type` enum('before','after') COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_details_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_images`
--

INSERT INTO `order_images` (`id`, `order_id`, `type`, `address`, `created_at`, `updated_at`, `order_details_id`) VALUES
(1, 5, 'after', '5-9-Tulips.jpg', '2015-01-12 04:35:38', '2015-01-12 04:35:38', 9),
(2, 8, 'before', '8-16-Desert.jpg', '2015-01-13 01:51:40', '2015-01-13 01:51:40', 16),
(3, 8, 'after', '8-16-Tulips.jpg', '2015-01-13 01:51:48', '2015-01-13 01:51:48', 16),
(4, 10, 'before', '10-67-dynammiKunstMap.png', '2015-01-14 05:58:19', '2015-01-14 05:58:19', 67),
(5, 10, 'before', '10-67-emp.jpg', '2015-01-14 05:58:20', '2015-01-14 05:58:20', 67),
(6, 10, 'before', '10-67-GSS-Logo.png', '2015-01-14 05:58:31', '2015-01-14 05:58:31', 67);

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_config`
--

CREATE TABLE IF NOT EXISTS `quickbooks_config` (
`quickbooks_config_id` int(10) unsigned NOT NULL,
  `qb_username` varchar(40) NOT NULL,
  `module` varchar(40) NOT NULL,
  `cfgkey` varchar(40) NOT NULL,
  `cfgval` varchar(40) NOT NULL,
  `cfgtype` varchar(40) NOT NULL,
  `cfgopts` text NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
`quickbooks_log_id` int(10) unsigned NOT NULL,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quickbooks_log`
--

INSERT INTO `quickbooks_log` (`quickbooks_log_id`, `quickbooks_ticket_id`, `batch`, `msg`, `log_datetime`) VALUES
(1, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/vendor/consolibyte/quickbooks/docs/web_connector/example_web_connector.php'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 11:15:22'),
(2, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/qb'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 15:31:03'),
(3, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/qb'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-24 14:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_oauth`
--

CREATE TABLE IF NOT EXISTS `quickbooks_oauth` (
`quickbooks_oauth_id` int(10) unsigned NOT NULL,
  `app_username` varchar(255) NOT NULL,
  `app_tenant` varchar(255) NOT NULL,
  `oauth_request_token` varchar(255) DEFAULT NULL,
  `oauth_request_token_secret` varchar(255) DEFAULT NULL,
  `oauth_access_token` varchar(255) DEFAULT NULL,
  `oauth_access_token_secret` varchar(255) DEFAULT NULL,
  `qb_realm` varchar(32) DEFAULT NULL,
  `qb_flavor` varchar(12) DEFAULT NULL,
  `qb_user` varchar(64) DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
`quickbooks_queue_id` int(10) unsigned NOT NULL,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) NOT NULL,
  `msg` text,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quickbooks_queue`
--

INSERT INTO `quickbooks_queue` (`quickbooks_queue_id`, `quickbooks_ticket_id`, `qb_username`, `qb_action`, `ident`, `extra`, `qbxml`, `priority`, `qb_status`, `msg`, `enqueue_datetime`, `dequeue_datetime`) VALUES
(1, NULL, 'quickbooks', 'CustomerAdd', '1', NULL, NULL, 0, '1', NULL, '2014-09-24 11:43:15', '2014-09-24 11:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_recur`
--

CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
`quickbooks_recur_id` int(10) unsigned NOT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
`quickbooks_ticket_id` int(10) unsigned NOT NULL,
  `qb_username` varchar(40) NOT NULL,
  `ticket` char(36) NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) DEFAULT NULL,
  `lasterror_msg` varchar(255) DEFAULT NULL,
  `ipaddr` char(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(255) NOT NULL,
  `qb_company_file` varchar(255) DEFAULT NULL,
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quickbooks_user`
--

INSERT INTO `quickbooks_user` (`qb_username`, `qb_password`, `qb_company_file`, `qbwc_wait_before_next_update`, `qbwc_min_run_every_n_seconds`, `status`, `write_datetime`, `touch_datetime`) VALUES
('quickbooks', '725cbb4eb5719aabdf405267531b8e8ac9ac454c', '', 0, 0, 'e', '2014-09-22 11:15:21', '2014-09-22 11:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `recurrings`
--

CREATE TABLE IF NOT EXISTS `recurrings` (
`id` int(10) unsigned NOT NULL,
  `request_service_id` int(10) unsigned DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `assignment_type` enum('single','multiple') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requested_services`
--

CREATE TABLE IF NOT EXISTS `requested_services` (
`id` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `required_date` date DEFAULT NULL,
  `required_time` time DEFAULT NULL,
  `service_men` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_note` text COLLATE utf8_unicode_ci,
  `customer_note` text COLLATE utf8_unicode_ci,
  `vendor_note` text COLLATE utf8_unicode_ci,
  `verified_vacancy` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys_trash_out` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trash_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storage_shed` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `requested_services`
--

INSERT INTO `requested_services` (`id`, `request_id`, `service_id`, `status`, `created_at`, `updated_at`, `required_date`, `required_time`, `service_men`, `service_note`, `customer_note`, `vendor_note`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`) VALUES
(1, 1, 26, 1, '2015-01-08 13:46:25', '2015-01-08 13:46:25', '2015-01-01', '01:00:00', '5', 'I need this work done', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 26, 1, '2015-01-08 18:13:10', '2015-01-08 18:13:10', '2015-01-08', '01:00:00', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 27, 1, '2015-01-08 18:13:10', '2015-01-08 18:13:10', NULL, NULL, NULL, '', NULL, NULL, '12', NULL, 'adfasd', NULL, NULL, NULL),
(4, 3, 26, 1, '2015-01-09 08:10:51', '2015-01-09 08:10:51', '2015-01-01', '01:00:00', '', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 3, 28, 1, '2015-01-09 08:10:51', '2015-01-09 08:10:51', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, 31, 1, '2015-01-09 08:10:51', '2015-01-09 08:10:51', NULL, NULL, '32', 'test', NULL, NULL, '3', NULL, '23', '23', NULL, '32'),
(7, 3, 35, 1, '2015-01-09 08:10:51', '2015-01-09 08:10:51', '2015-01-05', '01:00:00', '', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 4, 26, 1, '2015-01-12 03:51:08', '2015-01-12 03:51:08', '2015-01-01', '01:02:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 4, 28, 1, '2015-01-12 03:51:08', '2015-01-12 03:51:08', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 4, 33, 1, '2015-01-12 03:51:08', '2015-01-12 03:51:08', NULL, NULL, NULL, 'st', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 6, 27, 1, '2015-01-12 09:08:09', '2015-01-12 09:08:09', NULL, NULL, NULL, 'test', NULL, NULL, '12', NULL, 'adfasd', NULL, NULL, NULL),
(12, 6, 31, 1, '2015-01-12 09:08:09', '2015-01-12 09:08:09', NULL, NULL, '32', 'test', NULL, NULL, '3', NULL, '23', '23', NULL, '32'),
(31, 6, 26, 1, '2015-01-12 09:58:36', '2015-01-12 09:58:36', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 8, 27, 1, '2015-01-13 00:47:36', '2015-01-13 01:52:00', NULL, NULL, NULL, 'test', NULL, 'testestew', '12', NULL, 'adfasd', NULL, NULL, NULL),
(49, 8, 28, 1, '2015-01-13 00:48:51', '2015-01-13 00:48:51', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 8, 29, 1, '2015-01-13 01:22:24', '2015-01-13 01:22:24', NULL, NULL, NULL, 'test', NULL, NULL, '3', '32', '32', '32', '3232', '32'),
(53, 8, 36, 1, '2015-01-13 01:22:25', '2015-01-13 01:22:25', '2015-01-01', '01:00:00', '23', 'st', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 9, 26, 1, '2015-01-13 22:00:53', '2015-01-13 22:00:53', '2015-01-14', '01:00:00', '343242', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 9, 28, 1, '2015-01-13 22:00:53', '2015-01-13 22:00:53', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 9, 33, 1, '2015-01-13 22:00:53', '2015-01-13 22:00:53', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(58, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(59, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(60, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(61, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(62, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(63, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(64, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(65, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(66, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(67, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(68, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(69, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(70, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(71, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(72, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(73, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(74, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(75, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(76, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(77, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(78, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(79, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(80, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(81, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(82, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(83, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(84, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(85, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(86, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(87, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(88, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(89, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(90, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(91, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(92, 9, 27, 1, '2015-01-13 22:09:10', '2015-01-13 22:09:10', NULL, NULL, NULL, '', NULL, NULL, '3242', NULL, '34324', NULL, NULL, NULL),
(93, 10, 27, 1, '2015-01-14 05:46:09', '2015-01-14 05:46:09', NULL, NULL, NULL, 'test service', NULL, NULL, '12', NULL, '2313', NULL, NULL, NULL),
(94, 10, 33, 1, '2015-01-14 05:46:09', '2015-01-14 05:46:09', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 10, 36, 1, '2015-01-14 05:46:09', '2015-01-14 05:46:09', '2015-01-16', '05:00:00', '12', 'This is a Test Note', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 10, 31, 1, '2015-01-14 05:55:05', '2015-01-14 05:59:00', NULL, NULL, '12', 'Test Drive', NULL, 'Test vendor notes', '231', NULL, '12321', '232', NULL, '12321'),
(97, 10, 31, 1, '2015-01-14 05:55:05', '2015-01-14 05:55:05', NULL, NULL, '12', 'Test Drive', NULL, NULL, '231', NULL, '12321', '232', NULL, '12321'),
(98, 11, 26, 1, '2015-01-14 18:43:35', '2015-01-14 18:43:35', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 12, 26, 1, '2015-01-15 07:45:43', '2015-01-15 07:45:43', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 13, 26, 1, '2015-01-15 07:48:54', '2015-01-15 07:48:54', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 14, 26, 1, '2015-01-15 07:49:51', '2015-01-15 07:49:51', '2015-01-01', '01:00:00', '3', 'test ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 26, 25, 1, '2015-01-15 10:43:09', '2015-01-15 10:43:09', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL),
(103, 27, 30, 1, '2015-01-15 10:49:40', '2015-01-15 10:49:40', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 28, 26, 1, '2015-01-15 10:59:08', '2015-01-15 10:59:08', '2015-01-01', '01:00:00', '2', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 29, 26, 1, '2015-01-15 11:03:35', '2015-01-15 11:03:35', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 30, 29, 1, '2015-01-15 11:06:53', '2015-01-15 11:06:53', NULL, NULL, NULL, 'test', NULL, NULL, '3', '32', '32', '32', '3232', '32'),
(107, 31, 26, 1, '2015-01-16 01:49:58', '2015-01-16 01:49:58', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 31, 26, 1, '2015-01-16 01:49:59', '2015-01-16 01:49:59', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 32, 26, 1, '2015-01-16 02:01:07', '2015-01-16 02:01:07', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 33, 26, 1, '2015-01-16 02:01:31', '2015-01-16 02:01:31', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 34, 26, 1, '2015-01-16 02:04:08', '2015-01-16 02:04:08', '2015-01-01', '01:00:00', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 35, 26, 1, '2015-01-16 02:05:23', '2015-01-16 02:05:23', '2015-01-01', '01:00:00', '3', 'trest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 36, 26, 1, '2015-01-16 02:06:28', '2015-01-16 02:06:28', '2015-01-01', '01:00:00', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 37, 26, 1, '2015-01-16 02:07:37', '2015-01-16 02:07:37', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 38, 26, 1, '2015-01-16 02:20:02', '2015-01-16 02:20:02', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 39, 26, 1, '2015-01-16 02:20:57', '2015-01-16 02:20:57', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 40, 26, 1, '2015-01-16 02:21:10', '2015-01-16 02:21:10', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 41, 26, 1, '2015-01-16 02:23:41', '2015-01-16 02:23:41', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 42, 26, 1, '2015-01-16 03:03:00', '2015-01-16 03:03:00', '2015-01-01', '01:00:00', '3', 'rest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 43, 26, 1, '2015-01-16 03:04:57', '2015-01-16 03:04:57', '2015-01-09', '01:00:00', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 44, 26, 1, '2015-01-16 03:11:28', '2015-01-16 03:11:28', '2015-01-01', '01:00:00', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 45, 26, 1, '2015-01-16 03:12:40', '2015-01-16 03:12:40', '2015-01-09', '01:00:00', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 46, 26, 1, '2015-01-16 03:17:24', '2015-01-16 03:17:24', '2015-01-09', '01:00:00', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 47, 26, 1, '2015-01-16 03:24:47', '2015-01-16 03:24:47', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 48, 26, 1, '2015-01-16 03:25:09', '2015-01-16 03:25:09', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 49, 25, 1, '2015-01-19 03:30:25', '2015-01-19 03:30:25', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL),
(127, 49, 28, 1, '2015-01-19 03:30:25', '2015-01-19 03:30:25', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 49, 31, 1, '2015-01-19 03:30:25', '2015-01-19 03:30:25', NULL, NULL, '32', 'test', NULL, NULL, '3', NULL, '23', '23', NULL, '32'),
(129, 49, 33, 1, '2015-01-19 03:30:25', '2015-01-19 03:30:25', NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 49, 39, 1, '2015-01-19 03:30:26', '2015-01-19 03:30:26', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, '3', '2', '2', '2', '2', NULL),
(131, 50, 25, 1, '2015-01-19 04:36:09', '2015-01-19 04:36:09', '2015-01-01', '01:00:00', '3', 'test', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL),
(132, 50, 27, 1, '2015-01-19 04:36:09', '2015-01-19 04:36:09', NULL, NULL, NULL, 'tst', NULL, NULL, '12', NULL, 'adfasd', NULL, NULL, NULL),
(133, 37, 25, 1, '2015-01-19 05:06:24', '2015-01-19 05:06:24', '2015-01-01', '01:00:00', '3', 'tst', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_functions`
--

CREATE TABLE IF NOT EXISTS `role_functions` (
`id` int(10) unsigned NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_function` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_function_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_functions`
--

INSERT INTO `role_functions` (`id`, `status`, `created_at`, `updated_at`, `role_function`, `access_function_id`, `deleted_at`) VALUES
(1, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'User', 1, NULL),
(2, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Access Level', 1, NULL),
(3, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Access Rights', 1, NULL),
(4, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Customer', 2, NULL),
(5, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Asset', 3, NULL),
(6, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Vendor', 4, NULL),
(7, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Maintenance Request', 5, NULL),
(8, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Service', 6, NULL),
(9, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Order', 7, NULL),
(10, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Completed Request', 8, NULL),
(11, 1, '2014-10-01 10:14:53', '0000-00-00 00:00:00', 'Invoice', 9, NULL),
(12, 1, '2014-10-03 14:43:07', '0000-00-00 00:00:00', 'Special Price', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_price` int(10) unsigned DEFAULT NULL,
  `vendor_price` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `req_date` tinyint(1) DEFAULT NULL,
  `number_of_men` tinyint(1) DEFAULT NULL,
  `verified_vacancy` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys_trash_out` tinyint(4) NOT NULL DEFAULT '0',
  `trash_size` tinyint(4) NOT NULL DEFAULT '0',
  `storage_shed` tinyint(4) NOT NULL DEFAULT '0',
  `lot_size` tinyint(4) NOT NULL DEFAULT '0',
  `emergency` tinyint(4) NOT NULL DEFAULT '0',
  `desc` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `customer_price`, `vendor_price`, `status`, `created_at`, `updated_at`, `service_code`, `req_date`, `number_of_men`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`, `emergency`, `desc`) VALUES
(25, 'interior / Exterior Painting', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:44:44', 'INT123', 1, 1, 1, 1, 0, 0, 0, 0, 0, ''),
(26, 'Flooring (Carpet/ Wood/ Tile)', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:45:14', 'FLO123', 1, 1, 0, 0, 0, 0, 0, 0, 0, ''),
(27, 'Drywall - New/ Repairs', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:45:43', 'DRY123', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, ''),
(28, 'Countertops/ Cabinets', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:13', 'COU123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(29, ' Carpentry', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:32', 'CAR123', NULL, NULL, 1, 1, 1, 1, 1, 1, 0, ''),
(30, 'Cleaning/ Trush Removal', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:46:46', 'CLE123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(31, 'Electrical', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'ELE123', NULL, 1, 1, 0, 1, 1, 0, 1, 0, '\r\n'),
(32, 'Garage Doors', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'GAR123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(33, 'Plumbing', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'PLU123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(34, 'Fences', 100, 80, 1, '2014-10-01 06:12:37', '2014-12-06 02:47:42', 'FEN123', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ''),
(35, 'Ceramic Tile', 0, NULL, 1, '2014-10-01 06:12:37', '0000-00-00 00:00:00', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, NULL),
(36, 'Roofing', 0, NULL, 1, '2014-10-01 06:12:37', '0000-00-00 00:00:00', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, NULL),
(38, 'Test Service', 100, 80, 1, '2014-12-06 02:33:11', '2014-12-06 02:33:11', 'test123', 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Test'),
(39, 'Rekey', 20, 5, 1, '2015-01-01 03:21:31', '2015-01-01 03:21:31', 'Rek123', 1, 1, 1, 1, 1, 1, 1, 0, 1, 'This is a test description'),
(40, 'test Service', 100, 60, 1, '2015-01-03 03:21:41', '2015-01-03 03:21:41', 'Code123', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 'test description');

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE IF NOT EXISTS `service_images` (
`id` int(10) unsigned NOT NULL,
  `requested_id` int(10) unsigned DEFAULT NULL,
  `image_name` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `image_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `requested_id`, `image_name`, `status`, `image_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'nhuit56bed98fd36.jpg', 1, 0, '2015-01-08 13:46:25', '2015-01-08 13:46:25'),
(2, 2, 'nhuv4s5440049c2f.jpg', 1, 0, '2015-01-08 18:13:10', '2015-01-08 18:13:10'),
(3, 3, 'nhuv5p97c00bb012.jpg', 1, 0, '2015-01-08 18:13:10', '2015-01-08 18:13:10'),
(4, 4, 'nhwv8pfbf743c778.jpg', 1, 0, '2015-01-09 08:10:51', '2015-01-09 08:10:51'),
(5, 5, 'nhwv8w4325868524.jpg', 1, 0, '2015-01-09 08:10:51', '2015-01-09 08:10:51'),
(6, 6, 'nhwv9q9b72c9cd6e.jpg', 1, 0, '2015-01-09 08:10:51', '2015-01-09 08:10:51'),
(7, 7, 'nhwv9y8ea74fdf44.jpg', 1, 0, '2015-01-09 08:10:51', '2015-01-09 08:10:51'),
(8, 8, 'ni238sbba64de433.jpg', 1, 0, '2015-01-12 03:51:08', '2015-01-12 03:51:08'),
(9, 9, 'ni238yfc7a646947.jpg', 1, 0, '2015-01-12 03:51:08', '2015-01-12 03:51:08'),
(10, 10, 'ni239478a7e61f31.jpg', 1, 0, '2015-01-12 03:51:08', '2015-01-12 03:51:08'),
(11, 11, 'ni2hx9d95622fd72.jpg', 1, 0, '2015-01-12 09:08:09', '2015-01-12 09:08:09'),
(36, 49, 'ni3phc35103254c5.jpg', 1, 0, '2015-01-13 01:17:48', '2015-01-13 01:17:48'),
(37, 52, 'ni3r0s62d06095c4.jpg', 1, 0, '2015-01-13 01:35:04', '2015-01-13 01:35:04'),
(38, 53, 'ni3r1a3b4a9c1c00.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(39, 53, 'ni3r1a2e72f3fbdb.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(40, 53, 'ni3r1a2e72f3fbdb.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(41, 53, 'ni3r1aa0a4660b4d.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(42, 53, 'ni3r1a3d40cb6e16.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(43, 53, 'ni3r1a8e1f3ac302.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(44, 53, 'ni3r1a8e1f3ac302.jpg', 1, 0, '2015-01-13 01:35:05', '2015-01-13 01:35:05'),
(45, 54, 'ni4eyd1bee40a8ab.jpg', 1, 0, '2015-01-13 22:00:53', '2015-01-13 22:00:53'),
(46, 55, 'ni4f0ecfb4ac48d8.jpg', 1, 0, '2015-01-13 22:00:53', '2015-01-13 22:00:53'),
(47, 56, 'ni4f1cbfaaa5cfae.jpg', 1, 0, '2015-01-13 22:00:53', '2015-01-13 22:00:53'),
(48, 57, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:09:31', '2015-01-13 22:09:31'),
(49, 58, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:09:40', '2015-01-13 22:09:40'),
(50, 59, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:09:52', '2015-01-13 22:09:52'),
(51, 60, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:22', '2015-01-13 22:13:22'),
(52, 61, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(53, 62, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(54, 63, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(55, 64, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(56, 65, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:23', '2015-01-13 22:13:23'),
(57, 66, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(58, 67, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(59, 68, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(60, 69, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:24', '2015-01-13 22:13:24'),
(61, 70, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(62, 71, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(63, 72, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(64, 73, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:26', '2015-01-13 22:13:26'),
(65, 74, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:27', '2015-01-13 22:13:27'),
(66, 75, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:27', '2015-01-13 22:13:27'),
(67, 76, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(68, 77, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(69, 78, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(70, 79, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(71, 80, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:28', '2015-01-13 22:13:28'),
(72, 81, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:29', '2015-01-13 22:13:29'),
(73, 82, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:29', '2015-01-13 22:13:29'),
(74, 83, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(75, 84, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(76, 85, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(77, 86, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(78, 87, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(79, 88, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:30', '2015-01-13 22:13:30'),
(80, 89, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(81, 90, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(82, 91, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(83, 92, 'ni4ff78130fd8602.jpg', 1, 0, '2015-01-13 22:13:32', '2015-01-13 22:13:32'),
(84, 93, 'ni50i15fbb7dad80.png', 1, 0, '2015-01-14 05:46:09', '2015-01-14 05:46:09'),
(85, 94, 'ni50it2f296eb3c0.png', 1, 0, '2015-01-14 05:46:09', '2015-01-14 05:46:09'),
(86, 95, 'ni50ki13a0185bfe.png', 1, 0, '2015-01-14 05:46:09', '2015-01-14 05:46:09'),
(87, 98, 'ni60ki73e11e87ea.jpg', 1, 0, '2015-01-14 18:43:35', '2015-01-14 18:43:35'),
(88, 99, 'ni7y431be5e6ae51.jpg', 1, 0, '2015-01-15 07:45:43', '2015-01-15 07:45:43'),
(89, 100, 'ni7y9gf24aac9118.jpg', 1, 0, '2015-01-15 07:48:55', '2015-01-15 07:48:55'),
(90, 101, 'ni7yax671483ed35.jpg', 1, 0, '2015-01-15 07:49:51', '2015-01-15 07:49:51'),
(91, 102, 'ni86brc89717e412.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(92, 102, 'ni86brc89717e412.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(93, 102, 'ni86brc89717e412.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(94, 102, 'ni86brc89717e412.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(95, 102, 'ni86brfad8dbb8e4.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(96, 102, 'ni86brfad8dbb8e4.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(97, 102, 'ni86brfad8dbb8e4.jpg', 1, 0, '2015-01-15 10:43:09', '2015-01-15 10:43:09'),
(98, 103, 'ni86mp38de4a3fdd.jpg', 1, 0, '2015-01-15 10:49:40', '2015-01-15 10:49:40'),
(99, 104, 'ni872g8af7df84a5.jpg', 1, 0, '2015-01-15 10:59:09', '2015-01-15 10:59:09'),
(100, 105, 'ni879m33906c1d93.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(101, 105, 'ni879m33906c1d93.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(102, 105, 'ni879m33906c1d93.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(103, 105, 'ni879m7b2aa1b186.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(104, 105, 'ni879m7b2aa1b186.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(105, 105, 'ni879m7b2aa1b186.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(106, 105, 'ni879m7b2aa1b186.jpg', 1, 0, '2015-01-15 11:03:35', '2015-01-15 11:03:35'),
(107, 106, 'ni87fb54c30df8cb.jpg', 1, 0, '2015-01-15 11:06:53', '2015-01-15 11:06:53'),
(108, 107, 'ni9cb770bd5af99e.jpg', 1, 0, '2015-01-16 01:49:58', '2015-01-16 01:49:58'),
(109, 107, 'ni9cb770bd5af99e.jpg', 1, 0, '2015-01-16 01:49:58', '2015-01-16 01:49:58'),
(110, 107, 'ni9cb7832c54696e.jpg', 1, 0, '2015-01-16 01:49:58', '2015-01-16 01:49:58'),
(111, 107, 'ni9cb784318baa5c.jpg', 1, 0, '2015-01-16 01:49:58', '2015-01-16 01:49:58'),
(112, 108, 'ni9cb770bd5af99e.jpg', 1, 0, '2015-01-16 01:49:59', '2015-01-16 01:49:59'),
(113, 108, 'ni9cb770bd5af99e.jpg', 1, 0, '2015-01-16 01:49:59', '2015-01-16 01:49:59'),
(114, 108, 'ni9cb7832c54696e.jpg', 1, 0, '2015-01-16 01:49:59', '2015-01-16 01:49:59'),
(115, 108, 'ni9cb784318baa5c.jpg', 1, 0, '2015-01-16 01:49:59', '2015-01-16 01:49:59'),
(116, 112, 'ni9d0w462db78cea.jpg', 1, 0, '2015-01-16 02:05:23', '2015-01-16 02:05:23'),
(117, 118, 'ni9dvefc49db65fb.jpg', 1, 0, '2015-01-16 02:23:41', '2015-01-16 02:23:41'),
(118, 118, 'ni9dve056c0691da.jpg', 1, 0, '2015-01-16 02:23:41', '2015-01-16 02:23:41'),
(119, 125, 'ni9gpsf66e6e5bdd.jpg', 1, 0, '2015-01-16 03:25:09', '2015-01-16 03:25:09'),
(120, 125, 'ni9gpsf66e6e5bdd.jpg', 1, 0, '2015-01-16 03:25:09', '2015-01-16 03:25:09'),
(121, 131, 'nif3znc4575d0b96.jpg', 1, 0, '2015-01-19 04:36:09', '2015-01-19 04:36:09'),
(122, 131, 'nif3znc4575d0b96.jpg', 1, 0, '2015-01-19 04:36:09', '2015-01-19 04:36:09'),
(123, 132, 'nif40665e8c41103.jpg', 1, 0, '2015-01-19 04:36:09', '2015-01-19 04:36:09'),
(124, 132, 'nif40665e8c41103.jpg', 1, 0, '2015-01-19 04:36:09', '2015-01-19 04:36:09'),
(125, 133, 'nif5em441c3a06a8.jpg', 1, 0, '2015-01-19 05:06:56', '2015-01-19 05:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `special_prices`
--

CREATE TABLE IF NOT EXISTS `special_prices` (
`id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `special_price` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `special_prices`
--

INSERT INTO `special_prices` (`id`, `customer_id`, `service_id`, `special_price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, 31, 60, 1, '2015-01-01 03:24:22', '2015-01-01 03:24:22', NULL),
(2, 48, 33, 12, 1, '2015-01-06 17:25:28', '2015-01-06 17:25:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alabama', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Alaska', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Arizona', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'California', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(5, 'Texas', 1, '2015-01-12 07:46:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `first_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_1` text COLLATE utf8_unicode_ci,
  `address_2` text COLLATE utf8_unicode_ci,
  `zipcode` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `user_role_id` int(10) unsigned DEFAULT NULL,
  `profile_status` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `profile_picture` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `email`, `username`, `password`, `phone`, `address_1`, `address_2`, `zipcode`, `profile_image`, `type_id`, `user_role_id`, `profile_status`, `status`, `remember_token`, `created_at`, `updated_at`, `city_id`, `state_id`, `profile_picture`, `latitude`, `longitude`, `deleted_at`) VALUES
(1, 'Admin', 'admin', NULL, 'sheheryar.alam.khan@gmail.com', 'admin', '$2y$10$C5CALRtIlX4zuk5iusvrpOpJ377VDmzljpmQZf0Y5cb3TdFM/r.Lq', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, '1culTT1wpFRAdiLm5f4XQmkLtzSIh3UwRBYmq7PIrOHIxJBYdHJDCgUBWEFS', '2014-10-01 06:12:36', '2015-01-20 06:14:20', NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Chassidy', 'Goolsby', 'GSS ', 'officemgr@gssreo.com', 'officemgr@gssreo.com', '$2y$10$1XyZm9qQfRbtpDquhWhWRuqj0i8x.4mVrae2H8tYUdFVn6NZGKT1a', '4694532032', '118 National Dr', '', '75448', NULL, 2, 2, 1, 1, NULL, '2014-10-22 07:20:42', '2014-10-22 07:41:07', 2, 1, NULL, NULL, NULL, NULL),
(48, 'Shery', 'khan', 'testing', 'abc123456yudkasj@gmail.com', 'shery', '$2y$10$vnfAIYTA53ksHCZrLWd7CuVMeS5OWvRJaDV4TsRIL4S7YFH8roYcq', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, NULL, '2014-10-22 07:38:45', '2014-10-22 07:38:45', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Shery', 'Test', 'Test', 'sheheryardfdas@devronix.com', 'shery123', '$2y$10$EIW5eMxWy7r7Edcx.fDST.u8oXEib2jcW/.DXl/BV6dT5spAWbLBu', '23123142343', 'dfsafads', 'fasdfa', '75300', NULL, 2, 2, 1, 1, NULL, '2014-12-09 00:15:58', '2014-12-09 00:17:23', 3, 1, NULL, NULL, NULL, NULL),
(71, 'Muhammad', 'Bilal', 'abc', 'bilal.virtual+1@gmail.com', 'bilal.virtual+1', '$2y$10$3mR6VPLN/BYRzZjRlCxo8.dPjtZfm6D2O/eQPGJ3V5Z9xhfrLYEYi', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, 'yIlGrHitmc6FOHCmAw39OXbDZMtruCj0UlLeJzOnvcbF3bpVnBOHkWXG0E5w', '2014-12-09 03:26:54', '2014-12-09 03:39:49', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Muhammad', 'Bilal', 'abc', 'bilal.virtual+2@gmail.com', 'bilal.virtual+2', '$2y$10$DqyA4uQoRQV8BzHFncQoG.8ojtzy7F2TYEdJnUzXnfGXH/0FAitze', '23432879', 'this is testing address', '', '32893', NULL, 2, 2, 1, 1, NULL, '2014-12-09 03:32:21', '2014-12-09 03:49:11', 1, 1, NULL, NULL, NULL, NULL),
(73, 'test', 'test', 'test', 'asdfas@gmail.com', 'cust', '$2y$10$U/pXnVPoJQb6AjSctd5G9uyqIywuWG3fbmzDeol0l3bQhhtMHQ.sK', '343242', 'sfdadsfdfdsadsafdasfadsfdasfadsfads', 'adfafadsfa', '143343', NULL, 2, 2, 1, 1, 'GTVBXNmLlN1vvwMlZomV9ZWGPDIQZjRDryp7Mz6fA3mjev7ArqGFr9ixvgAv', '2014-12-25 04:18:17', '2015-01-06 17:24:04', 2, 1, 'profile-emp-cust.jpg', NULL, NULL, NULL),
(74, 'Sheheryar', 'Khan', 'test company', 'test@gmail.com', 'customer', '$2y$10$vFXPfySUwwI6AqOoROOZ7O9GD6VeqpPOOnok96zRJShqj3vn..jGS', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, NULL, '2015-01-01 03:01:29', '2015-01-01 03:01:29', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'test', 'test', 'GSS REO', 'test@gmail.gss', 'testcustomer', '$2y$10$oys37Sn6s/R8Rp1sRqIxUOJJPo3Zgwi6ulaMCIOPmKkyj6JzdRKgG', '232132131', 'Test Test street, Test State ', 'Test City  test country', '876897', NULL, 2, 2, 1, 1, 'P8oZbLySxK9xA9OQIK9tykzHoiueUPEfE5Onu0NKgL1XK9hWz4nOqepvuyUo', '2015-01-01 03:10:02', '2015-01-01 03:16:19', 19, 4, 'profile-emp-testcustomer.jpg', NULL, NULL, NULL),
(76, 'Customer', 'One', 'GSS REO', '', 'customer.dev', '$2y$10$C5CALRtIlX4zuk5iusvrpOpJ377VDmzljpmQZf0Y5cb3TdFM/r.Lq', '2545774247', 'Test Test street, Test State', 'Test City  test country', '75300', NULL, 2, 2, 1, 1, '588bgNuZFmpfAXK4s1h9CLifm7FdJaRU6XOyUbb8Ko2Hkceu6AL8H1IwYp3q', '2015-01-03 03:06:39', '2015-01-15 10:16:58', 5, 1, 'profile-dynammiKunstMap-customer.dev.png', NULL, NULL, NULL),
(77, 'testin', 'testing', 'devronix', 'azeem.devronix@gmail.com', 'azeem', '$2y$10$3tcl7NoTOyAUBp3HJwVoGe9aqcdpifRa94fph.bc7xi.WN/Xn3dYO', '2432', '324 jfhg ghf fgfgh fgf', '324 dsfadsfasdfads', '3432', NULL, 2, 2, 1, 1, 'r3Srabbyw81Zu55rsf4gtJxzkumpJnyq4EDkPHl9Bey6QxfgFq8EUZs5OW2o', '2015-01-07 13:48:23', '2015-01-20 06:03:28', 1, 1, NULL, NULL, NULL, NULL),
(78, 'testin', 'testing', 'devronix', 'farhan.devronix@gmail.com', 'farhan', '$2y$10$3tcl7NoTOyAUBp3HJwVoGe9aqcdpifRa94fph.bc7xi.WN/Xn3dYO', '2432', '324 jfhg ghf fgfgh fgf', '324 dsfadsfasdfads', '3432', NULL, 3, 3, 1, 1, 'swc3Vwx8lOsTaDcINL03IssKJrmLbiZhnw19jLppGuMgSxitaMMtkBIi0nLZ', '2015-01-07 14:52:49', '2015-01-09 08:17:52', 6, 2, NULL, 51.88, -176.658, NULL),
(79, 'Azeem', 'testing', 'devronix', 'azeem2_hansom@yahoo.com', 'azeem88', '$2y$10$BJco49mNSiy4daRsXLNqOuierjOSepeH16lCT0DCwH.5hSJGE/XMm', NULL, NULL, NULL, NULL, NULL, 3, 3, 0, 1, 'vbjjyoyKAJ85x2GsFFeoy7BkjqNprfJhKoki0owIxnJ2RpTqVnSYaDoKwJ8T', '2015-01-09 08:12:10', '2015-01-19 08:44:17', NULL, NULL, NULL, 51.88, -176.658, NULL),
(80, 'Azeem1231', 'testing', 'devronix', 'azeem1_hansom@yahoo.com', 'azeem881', '$2y$10$BJco49mNSiy4daRsXLNqOuierjOSepeH16lCT0DCwH.5hSJGE/XMm', '2432', '825 Market Street, Building M, Suite 250', '825 Market Street, Building M, Suite 250', '75013', NULL, 3, 3, 1, 1, 'nmhWQMzqOqtgvAinBO9HZhejmmR80Rf0YhayJjYDpc85AtR21VHgvObmbMvh', '2015-01-12 03:44:06', '2015-01-20 06:12:23', 21, 5, NULL, 33.0884, -96.6833, NULL),
(81, 'shery', 'khan', 'dev', 'nav.devronix@gmail.com', 'nav', '$2y$10$T9r51vT5w7wtII5oIdlx5emd/bxKwA/5/YOMgbKnw393LFWXUcj7S', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, NULL, '2015-01-13 21:50:40', '2015-01-13 21:50:40', NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'nav', 'nav', 'nav', 'nav.d@g.com', 'nav123', '$2y$10$BJco49mNSiy4daRsXLNqOuierjOSepeH16lCT0DCwH.5hSJGE/XMm', '34324324', '825 Market Street, Building M, Suite 250, Allen, TX 75013', '', '75013', NULL, 3, 3, 1, 1, 'lrZUP2EPISq12ye51CKYoFtMPsJ4zLygXJ2Zyn18WltbjDq3nwFnMRqB6YZI', '2015-01-13 22:04:49', '2015-01-19 05:06:43', 21, 5, NULL, 33.0884, -96.6833, NULL),
(83, 'Customer', 'New', 'Gss', 'sheheryar@devronix.com', 'customer.new', '$2y$10$iY06yczfLd.N44K4fn.2se56Rs/O1AJ9RFTV3L7wu23D00hjqhOie', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 1, 'ZqBLZRfkOkdwPznQTYHNm5KPwVIHy2qtQv50TzratyphUu1njgJREuR6GkeW', '2015-01-14 05:38:03', '2015-01-14 05:46:30', NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'JESSICA', 'JESSICA', NULL, 'jessica@gssreo.com', NULL, '$2y$10$M//GC58ZVXMKHdXJ2In1M.q0NVKj38Y1BSNvEW7MSnxBHNzuYEAWm', NULL, NULL, NULL, NULL, NULL, 4, 6, 0, 1, NULL, '2015-01-14 06:26:58', '2015-01-14 06:26:58', NULL, NULL, NULL, NULL, NULL, NULL),
(85, 'Azeem', 'testing', 'devronix', 'azee323m2_hansom@yahoo.com', 'azeemku', '$2y$10$fLQzTYF8LPzlOcWAh.Q3HOcMxQSxYFCyLeYcfHDsNRv/PPx2wgw22', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 0, NULL, '2015-01-20 04:40:28', '2015-01-20 04:40:28', NULL, NULL, NULL, NULL, NULL, NULL),
(86, 'Azeem', 'testing', 'devronix', 'azee3232m2_hansom@yahoo.com', 'azeem2', '$2y$10$ZAq2aK4jjw4hG51UNogNzOLEuxG4wFAe2m1MLfhrBOJI7w677rAxq', NULL, NULL, NULL, NULL, NULL, 2, 2, 0, 0, NULL, '2015-01-20 04:41:39', '2015-01-20 04:41:39', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`id` int(10) unsigned NOT NULL,
  `role_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'This is the admin role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'Customer', 'This is the customer role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'Vendors', 'This is the vendor role', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'Manager', 'This is the manager manaigin the services', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(5, 'Admin Assitant', 'He will be assisting the Admin', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(6, 'Coordinator', 'Property Coordinator', 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_details`
--

CREATE TABLE IF NOT EXISTS `user_role_details` (
`id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `role_function_id` int(10) unsigned DEFAULT NULL,
  `add` tinyint(1) DEFAULT NULL,
  `edit` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_role_details`
--

INSERT INTO `user_role_details` (`id`, `role_id`, `role_function_id`, `add`, `edit`, `delete`, `status`, `created_at`, `updated_at`, `view`) VALUES
(1, 1, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(2, 1, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(3, 1, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(4, 1, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(5, 1, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(6, 1, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(7, 1, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(8, 1, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(9, 1, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(10, 1, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(11, 1, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(12, 2, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(13, 2, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(14, 2, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(15, 2, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(16, 2, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(17, 2, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(18, 2, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(19, 2, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(20, 2, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(21, 2, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(22, 2, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-01 17:29:06', 0),
(23, 3, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(24, 3, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(25, 3, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(26, 3, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(27, 3, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(28, 3, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(29, 3, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(30, 3, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(31, 3, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(32, 3, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(33, 3, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(34, 4, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(35, 4, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-03 13:11:02', 1),
(36, 4, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '2014-10-03 13:11:02', 1),
(37, 4, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(38, 4, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(39, 4, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(40, 4, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(41, 4, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(42, 4, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(43, 4, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(44, 4, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(45, 5, 1, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(46, 5, 2, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(47, 5, 3, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(48, 5, 4, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(49, 5, 5, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(50, 5, 6, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(51, 5, 7, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(52, 5, 8, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(53, 5, 9, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(54, 5, 10, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(55, 5, 11, 1, 1, 1, NULL, '2014-10-01 10:28:17', '0000-00-00 00:00:00', 1),
(56, 1, 12, 1, 1, 1, NULL, '2014-10-03 14:43:57', '0000-00-00 00:00:00', 1),
(57, 2, 12, 0, 0, 0, NULL, '2014-10-03 14:47:25', '0000-00-00 00:00:00', 0),
(58, 3, 12, 0, 0, 0, NULL, '2014-10-03 14:47:36', '0000-00-00 00:00:00', 0),
(59, 4, 12, 0, 0, 0, NULL, '2014-10-03 14:47:39', '0000-00-00 00:00:00', 0),
(60, 5, 12, 0, 0, 0, NULL, '2014-10-03 14:47:42', '0000-00-00 00:00:00', 0),
(61, 6, 1, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(62, 6, 2, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(63, 6, 3, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(64, 6, 4, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(65, 6, 5, 1, 1, 1, 1, '2015-01-14 06:26:09', '2015-01-14 06:27:37', 1),
(66, 6, 6, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(67, 6, 7, 1, 1, 1, 1, '2015-01-14 06:26:09', '2015-01-14 06:27:37', 1),
(68, 6, 8, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(69, 6, 9, 1, 1, 1, 1, '2015-01-14 06:26:09', '2015-01-14 06:27:37', 1),
(70, 6, 10, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(71, 6, 11, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0),
(72, 6, 12, 0, 0, 0, 1, '2015-01-14 06:26:09', '2015-01-14 06:26:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(2, 'customer', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(3, 'vendors', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00'),
(4, 'user', 1, '2014-10-01 06:12:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_services`
--

CREATE TABLE IF NOT EXISTS `vendor_services` (
`id` int(10) unsigned NOT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendor_services`
--

INSERT INTO `vendor_services` (`id`, `vendor_id`, `service_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 78, 25, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(2, 78, 26, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(3, 78, 27, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(4, 78, 28, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(5, 78, 29, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31'),
(6, 78, 30, 1, '2015-01-07 15:50:31', '2015-01-07 15:50:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_functions`
--
ALTER TABLE `access_functions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
 ADD PRIMARY KEY (`id`), ADD KEY `assets_customer_id_foreign` (`customer_id`), ADD KEY `assets_city_id_foreign` (`city_id`), ADD KEY `assets_state_id_foreign` (`state_id`);

--
-- Indexes for table `assign_requests`
--
ALTER TABLE `assign_requests`
 ADD PRIMARY KEY (`id`), ADD KEY `assign_requests_request_id_foreign` (`request_id`), ADD KEY `assign_requests_requested_service_id_foreign` (`requested_service_id`), ADD KEY `assign_requests_vendor_id_foreign` (`vendor_id`);

--
-- Indexes for table `bid_requested_services`
--
ALTER TABLE `bid_requested_services`
 ADD PRIMARY KEY (`id`), ADD KEY `requested_services_requested_id_foreign` (`request_id`), ADD KEY `requested_services_service_id_foreign` (`service_id`);

--
-- Indexes for table `bid_requests`
--
ALTER TABLE `bid_requests`
 ADD PRIMARY KEY (`id`), ADD KEY `maintenance_requests_customer_id_foreign` (`vendor_id`), ADD KEY `maintenance_requests_asset_id_foreign` (`asset_id`);

--
-- Indexes for table `bid_service_images`
--
ALTER TABLE `bid_service_images`
 ADD PRIMARY KEY (`id`), ADD KEY `service_images_requested_id_foreign` (`requested_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`id`), ADD KEY `cities_state_id_foreign` (`state_id`);

--
-- Indexes for table `emergency_requests`
--
ALTER TABLE `emergency_requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emergency_request_details`
--
ALTER TABLE `emergency_request_details`
 ADD PRIMARY KEY (`id`), ADD KEY `emergency_request_details_vendor_id_foreign` (`vendor_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`), ADD KEY `images_image_type_id_foreign` (`image_type_id`);

--
-- Indexes for table `image_types`
--
ALTER TABLE `image_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
 ADD PRIMARY KEY (`id`), ADD KEY `invoices_order_id_foreign` (`order_id`), ADD KEY `invoices_request_id_foreign` (`request_id`);

--
-- Indexes for table `maintenance_requests`
--
ALTER TABLE `maintenance_requests`
 ADD PRIMARY KEY (`id`), ADD KEY `maintenance_requests_customer_id_foreign` (`customer_id`), ADD KEY `maintenance_requests_asset_id_foreign` (`asset_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`), ADD KEY `notifications_activity_id_foreign` (`notification_type_id`);

--
-- Indexes for table `notification_types`
--
ALTER TABLE `notification_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD KEY `orders_request_id_foreign` (`request_id`), ADD KEY `orders_vendor_id_foreign` (`vendor_id`), ADD KEY `orders_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
 ADD PRIMARY KEY (`id`), ADD KEY `order_details_order_id_foreign` (`order_id`), ADD KEY `order_details_requested_service_id_foreign` (`requested_service_id`);

--
-- Indexes for table `order_images`
--
ALTER TABLE `order_images`
 ADD PRIMARY KEY (`id`), ADD KEY `order_images_order_id_foreign` (`order_id`), ADD KEY `order_images_order_details_id_foreign` (`order_details_id`);

--
-- Indexes for table `password_reminders`
--
ALTER TABLE `password_reminders`
 ADD KEY `password_reminders_email_index` (`email`), ADD KEY `password_reminders_token_index` (`token`);

--
-- Indexes for table `quickbooks_config`
--
ALTER TABLE `quickbooks_config`
 ADD PRIMARY KEY (`quickbooks_config_id`);

--
-- Indexes for table `quickbooks_log`
--
ALTER TABLE `quickbooks_log`
 ADD PRIMARY KEY (`quickbooks_log_id`), ADD KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`), ADD KEY `batch` (`batch`);

--
-- Indexes for table `quickbooks_oauth`
--
ALTER TABLE `quickbooks_oauth`
 ADD PRIMARY KEY (`quickbooks_oauth_id`);

--
-- Indexes for table `quickbooks_queue`
--
ALTER TABLE `quickbooks_queue`
 ADD PRIMARY KEY (`quickbooks_queue_id`), ADD KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`), ADD KEY `priority` (`priority`), ADD KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`), ADD KEY `qb_status` (`qb_status`);

--
-- Indexes for table `quickbooks_recur`
--
ALTER TABLE `quickbooks_recur`
 ADD PRIMARY KEY (`quickbooks_recur_id`), ADD KEY `qb_username` (`qb_username`,`qb_action`,`ident`), ADD KEY `priority` (`priority`);

--
-- Indexes for table `quickbooks_ticket`
--
ALTER TABLE `quickbooks_ticket`
 ADD PRIMARY KEY (`quickbooks_ticket_id`), ADD KEY `ticket` (`ticket`);

--
-- Indexes for table `quickbooks_user`
--
ALTER TABLE `quickbooks_user`
 ADD PRIMARY KEY (`qb_username`);

--
-- Indexes for table `recurrings`
--
ALTER TABLE `recurrings`
 ADD PRIMARY KEY (`id`), ADD KEY `recurrings_request_service_id_foreign` (`request_service_id`), ADD KEY `recurrings_vendor_id_foreign` (`vendor_id`);

--
-- Indexes for table `requested_services`
--
ALTER TABLE `requested_services`
 ADD PRIMARY KEY (`id`), ADD KEY `requested_services_requested_id_foreign` (`request_id`), ADD KEY `requested_services_service_id_foreign` (`service_id`);

--
-- Indexes for table `role_functions`
--
ALTER TABLE `role_functions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
 ADD PRIMARY KEY (`id`), ADD KEY `service_images_requested_id_foreign` (`requested_id`);

--
-- Indexes for table `special_prices`
--
ALTER TABLE `special_prices`
 ADD PRIMARY KEY (`id`), ADD KEY `special_prices_customer_id_foreign` (`customer_id`), ADD KEY `special_prices_service_id_foreign` (`service_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `users_type_id_foreign` (`type_id`), ADD KEY `users_user_role_id_foreign` (`user_role_id`), ADD KEY `users_city_id_foreign` (`city_id`), ADD KEY `users_state_id_foreign` (`state_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role_details`
--
ALTER TABLE `user_role_details`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_details_role_id_foreign` (`role_id`), ADD KEY `user_role_details_role_function_id_foreign` (`role_function_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_services`
--
ALTER TABLE `vendor_services`
 ADD PRIMARY KEY (`id`), ADD KEY `vendor_services_vendor_id_foreign` (`vendor_id`), ADD KEY `vendor_services_service_id_foreign` (`service_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_functions`
--
ALTER TABLE `access_functions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `assign_requests`
--
ALTER TABLE `assign_requests`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `bid_requested_services`
--
ALTER TABLE `bid_requested_services`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `bid_requests`
--
ALTER TABLE `bid_requests`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bid_service_images`
--
ALTER TABLE `bid_service_images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `emergency_requests`
--
ALTER TABLE `emergency_requests`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `emergency_request_details`
--
ALTER TABLE `emergency_request_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image_types`
--
ALTER TABLE `image_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `maintenance_requests`
--
ALTER TABLE `maintenance_requests`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `notification_types`
--
ALTER TABLE `notification_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `order_images`
--
ALTER TABLE `order_images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `quickbooks_config`
--
ALTER TABLE `quickbooks_config`
MODIFY `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quickbooks_log`
--
ALTER TABLE `quickbooks_log`
MODIFY `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `quickbooks_oauth`
--
ALTER TABLE `quickbooks_oauth`
MODIFY `quickbooks_oauth_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quickbooks_queue`
--
ALTER TABLE `quickbooks_queue`
MODIFY `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quickbooks_recur`
--
ALTER TABLE `quickbooks_recur`
MODIFY `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quickbooks_ticket`
--
ALTER TABLE `quickbooks_ticket`
MODIFY `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurrings`
--
ALTER TABLE `recurrings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requested_services`
--
ALTER TABLE `requested_services`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `role_functions`
--
ALTER TABLE `role_functions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `special_prices`
--
ALTER TABLE `special_prices`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_role_details`
--
ALTER TABLE `user_role_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vendor_services`
--
ALTER TABLE `vendor_services`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
ADD CONSTRAINT `assets_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `assets_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `assets_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assign_requests`
--
ALTER TABLE `assign_requests`
ADD CONSTRAINT `assign_requests_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `assign_requests_requested_service_id_foreign` FOREIGN KEY (`requested_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `assign_requests_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `emergency_request_details`
--
ALTER TABLE `emergency_request_details`
ADD CONSTRAINT `emergency_request_details_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
ADD CONSTRAINT `images_image_type_id_foreign` FOREIGN KEY (`image_type_id`) REFERENCES `image_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
ADD CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `invoices_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `maintenance_requests`
--
ALTER TABLE `maintenance_requests`
ADD CONSTRAINT `maintenance_requests_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `maintenance_requests_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
ADD CONSTRAINT `notifications_notification_type_id_foreign` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `orders_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `orders_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `order_details_requested_service_id_foreign` FOREIGN KEY (`requested_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_images`
--
ALTER TABLE `order_images`
ADD CONSTRAINT `order_images_order_details_id_foreign` FOREIGN KEY (`order_details_id`) REFERENCES `order_details` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `order_images_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recurrings`
--
ALTER TABLE `recurrings`
ADD CONSTRAINT `recurrings_request_service_id_foreign` FOREIGN KEY (`request_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `recurrings_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `requested_services`
--
ALTER TABLE `requested_services`
ADD CONSTRAINT `requested_services_requested_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `requested_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
ADD CONSTRAINT `service_images_requested_id_foreign` FOREIGN KEY (`requested_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `special_prices`
--
ALTER TABLE `special_prices`
ADD CONSTRAINT `special_prices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `special_prices_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `users_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `users_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_role_details`
--
ALTER TABLE `user_role_details`
ADD CONSTRAINT `user_role_details_role_function_id_foreign` FOREIGN KEY (`role_function_id`) REFERENCES `role_functions` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `user_role_details_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vendor_services`
--
ALTER TABLE `vendor_services`
ADD CONSTRAINT `vendor_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `vendor_services_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
