


$(document).ready(function() {


 $("input[name='emergency_request']").change(function() {
        var emergency_request=  $(this).val();
       
        if(emergency_request=="1")
        {
            $("#emergency_request_additional_text").show();

        }
        else
        {
             $("#emergency_request_additional_text").hide();  

        }


    });

 $("#property_status").change(function() {
        var property_status=  $(this).val();
       
        if(property_status=="inactive")
        {
            $("#property_status_note").show();

        }
        else
        {
             $("#property_status_note").hide();  

        }


    });

    $("#Invoiceonchange").change(function() {
        var Invoiceonchange=  $(this).val();
        

        var over = '<div id="overlay">' +
        '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');

        window.location=baseurl+"admin-list-invoice/"+Invoiceonchange;

    });



    $("#bidRequestonchangeCustomer").change(function() {
        var bidRequestonchange=  $(this).val();
        

        var over = '<div id="overlay">' +
        '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');

        window.location=baseurl+"customer-bid-requests/"+bidRequestonchange;

    });


    $("#bidRequestonchangeVendor").change(function() {
        var bidRequestonchange=  $(this).val();
        

        var over = '<div id="overlay">' +
        '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');

        window.location=baseurl+"vendor-bid-requests/"+bidRequestonchange;

    });


    $("#bidRequestonchange").change(function() {
        var bidRequestonchange=  $(this).val();
        

        var over = '<div id="overlay">' +
        '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');

        window.location=baseurl+"admin-bid-requests/"+bidRequestonchange;

    });



    $('.mystatusclass li > a').click(function(e){

        var orderstatus=this.innerHTML;
        var orderstatusid="";
        var orderstatus_class="";


        if(orderstatus=="In-Progress")
        {
            orderstatusid=1;
            orderstatus_class="warning";
        }
        else if (orderstatus=="Completed")
        {
            orderstatusid=2;
            orderstatus_class="blue";
        }
        else if (orderstatus=="Requested")
        {
            orderstatusid=3;
            orderstatus_class="success";
        }
        else if (orderstatus=="Approved")
        {
            orderstatusid=4;
            orderstatus_class="gray";
        }
        else if (orderstatus=="Inactive")
        {
            orderstatusid=5;
            orderstatus_class="black";
        }

        var order_id=$("#order_id_for_change").val();

        $('.orderstatus').text(this.innerHTML);

        $.ajax({
            type: 'Post',
            url:  baseurl + 'change-status',

            data: {
                order_id: order_id,
                orderstatusid: orderstatusid,
                orderstatus_class:orderstatus_class,
                orderstatus_text:orderstatus


            },
            
            success: function(response) {
             $('#errorMessage').focus();
             $('#errorMessage').addClass("alert alert-success");
             $('#errorMessage').slideUp('slow');
             $('#errorMessage').html("Status has been changed to "+orderstatus).hide();
             $('#errorMessage').slideDown('slow');


         }
     });






    });

$("#property_address").change(function() {
    var zip=  $(this).val();
    var state=  $('#state_id option:selected').text();
    var city=  $('#city_id option:selected').text();
    var property_address=  $('#property_address').val();

    var over = '<div id="overlay">' +
    '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
    '</div>';
    $(over).appendTo('body');


    $.ajax({
        type: 'Post',
        url: baseurl + 'get-asset-map',

        data: {
            zip: zip,
            state: state,
            city: city,
            property_address: property_address,

        },
        
        success: function(response) {
         $('#overlay').remove();
         $("#maparea").html(response);

     }
 });


});


$('.request-services').on('submit', function(e) {
    var list_of_services = $('#list_of_services').html();
    var asset_number = $('#asset_number').val();
    var service_id = $('#service_ids').val();
    if (asset_number == '' || service_id == null) {
        $('#add_asset_alert').html('Asset number and at least one service is required.').slideDown('slow');
        return false;
    } else {
        if (list_of_services == "") {

            $('#add_asset_alert').html('Service detail is required.').slideDown('slow');
            $('#service_ids').val('Select some options');
            $('#service_ids').trigger('liszt:updated');
            return false;
        } else {

            return true;
        }

    }







});

$('#generate_asset_number').click(function() {


    $.ajax({
        type: 'Post',
        url: 'generate-asset-number',
        cache: false,
        success: function(response) {
            var asset_number = response;
            $('#asset_number').val(asset_number);
        }
    });
});

$('#state_id').change(function() {
        //e.preventDefault();
        var state_id = this.value;
        var options = '';
        $.ajax({
            type: 'Post',
            url: baseurl + '/get-cities-by-state-id',
            data: {
                state_id: state_id
            },
            cache: false,
            success: function(response) {
                var obj = JSON.parse(response);
                for (var i = 0; i < obj.length; i++) {
                    options += '<option value="' + obj[i].id + '">' + obj[i].name + '</option>';
                }
                $("#city_id").html(options);

                $('#city_id').trigger('liszt:updated');
            }
        });
    });


$('#asset_number').change(function(e) {
    $('#add_asset_alert').slideUp('slow');
    var asset_id = this.value;
    $.ajax({
        type: 'Post',
        url: 'ajax-get-asset-by-asset-id',
        data: {
            asset_id: asset_id
        },
        cache: false,
        success: function(response) {

            $('#showServiceid').html(response);

        }
    });
});

$('.view_asset_information').click(function() {
    var asset_id = this.id;


    $.ajax({
        type: 'Post',
        url: 'ajax-get-asset-by-asset-id',
        data: {
            asset_id: asset_id
        },
        cache: false,
        success: function(response) {
            $('#asset_information').html(response);
        }
    });

    $('#asset_information').modal('show');
});



$('#viewassets').click(function() {
    var asset_number = $('#asset_number').val();
    if (asset_number == '') {
        $('#add_asset_alert').html('Please Select asset.').slideDown('slow');

    } else {
        $('#showServiceid').modal('show');
    }


});

    // Set the dialog to open when mySelect changes

    /*
     * While Service is selected in add request page
     * Start here
     */




     {
        $('select#service_ids').on('change', function(event, params) {
            if (params.selected)

            {
                var asset_number = $('#asset_number').val();
                var service_id = $(this).val();
                var current_service = params.selected;
                //        var current_service_id=service_id[service_id.length - 1];
                $.ajax({
                    type: 'Post',
                    url: 'ajax-service-information-popup',
                    data: {
                        service_id: service_id,
                        asset_number: asset_number
                    },
                    cache: false,
                    success: function(response) {
                        if (response == 'error') {
                            $('#add_asset_alert').html('Please Select asset.');
                            $('#add_asset_alert').slideDown('slow');
                            $('#service_ids').val('Select some options');
                            $('#service_ids').trigger('liszt:updated');

                            // $('#service_ids').append('<option></option>');
                            // $( "#allservices" ).load(baseurl+'/includejs.php');

                        } else {
                            $('#allservices').html(response);
                            $('#add_asset_alert').slideUp('slow');
                            $('#' + current_service).modal({
                                backdrop: 'static',
                                keyboard: true
                            });
                            $('#' + current_service).modal('show');
                            $('.datepicker').datepicker();
                        }
                    }
                });
}

else
{
    var service_id = params.deselected;

    $('#services_list_' + service_id).remove();


}

            // $('#addcontenthere').load('view-customer-assets');

        });
}


{
    $('select#vendor_service_ids').on('change', function(event, params) {
        if (params.selected)

        {
            var asset_number = $('#asset_number').val();
            var service_id = $(this).val();
            var current_service = params.selected;
                //        var current_service_id=service_id[service_id.length - 1];
                $.ajax({
                    type: 'Post',
                    url: 'ajax-vendor-service-information-popup',
                    data: {
                        service_id: service_id,
                        asset_number: asset_number
                    },
                    cache: false,
                    success: function(response) {
                        if (response == 'error') {
                            $('#add_asset_alert').html('Please Select asset.');
                            $('#add_asset_alert').slideDown('slow');
                            $('#service_ids').val('Select some options');
                            $('#service_ids').trigger('liszt:updated');

                            // $('#service_ids').append('<option></option>');
                            // $( "#allservices" ).load(baseurl+'/includejs.php');

                        } else {
                            $('#allservices').html(response);
                            $('#add_asset_alert').slideUp('slow');
                            $('#' + current_service).modal({
                                backdrop: 'static',
                                keyboard: true
                            });
                            $('#' + current_service).modal('show');
                            $('.datepicker').datepicker();
                        }
                    }
                });
}

else
{
    var service_id = params.deselected;

    $('#services_list_' + service_id).remove();


}

            // $('#addcontenthere').load('view-customer-assets');

        });
}





//    $('select#service_ids').change(function() {
//        var asset_number = $('#asset_number').val();
//
//        var service_id = $(this).val();
//        var current_service_id = service_id[service_id.length - 1];
//        $.ajax({
//            type: 'Post',
//            url: 'ajax-service-information-popup',
//            data: {
//                service_id: service_id,
//                asset_number: asset_number
//            },
//            cache: false,
//            success: function(response) {
//                if (response == 'error') {
//                    $('#add_asset_alert').html('Please Select asset.');
//                    $('#add_asset_alert').slideDown('slow');
//                    $('#service_ids').val('Select some options');
//                    $('#service_ids').trigger('liszt:updated');
//
//                    // $('#service_ids').append('<option></option>');
//                    // $( "#allservices" ).load(baseurl+'/includejs.php');
//
//                } else {
//                    $('#allservices').html(response);
//                    $('#add_asset_alert').slideUp('slow');
//                    $('#' + current_service_id).modal('show');
//                    $('.datepicker').datepicker();
//                }
//
//            }
//        });
//        // $('#addcontenthere').load('view-customer-assets');
//
//    });

    /*
     * While Service is selected in add request page
     * Start End
     */



    ////////////////////////////////////////////// Vendor //////////////////////////////////



    {
        $('#changePassword').click(function() {
            if ($(this).is(':checked')) {
                $('#changePasswordControll').slideDown();
            }
            else {
                $('#changePasswordControll').slideUp();
            }
        });

        $('#editFirstName').click(function() {
            $('firstName').removeAttr('readonly');
        })

    }



    {
        // Set the dialog to open when mySelect changes
        $('#changePassword').click(function() {
            if ($(this).is(':checked')) {
                $('#changePasswordControll').slideDown();
            }
            else {
                $('#changePasswordControll').slideUp();
            }
        });

        $('#editFirstName').click(function() {
            $('#firstName').removeAttr('readonly');
        })
        $('#editLastName').click(function() {
            $('#lastName').removeAttr('readonly');
        })
        $('#editUsername').click(function() {
            $('#username').removeAttr('readonly');
        })
        $('#editEmail').click(function() {
            $('#email').removeAttr('readonly');
        })
        $('#editCompany').click(function() {
            $('#company').removeAttr('readonly');
        })
        $('#editPhone').click(function() {
            $('#phone').removeAttr('readonly');
        })
        $('#editFirstName').click(function() {
            $('#firstName').removeAttr('readonly');
        })
        $('#editAddress1').click(function() {
            $('#address1').removeAttr('readonly');
        })
        $('#editAddress2').click(function() {
            $('#address2').removeAttr('readonly');
        })
        $('#editZipcode').click(function() {
            $('#zipcode').removeAttr('readonly');
        })

    }



//    {
//        $('#state_id').change(function() {
//            var state_id = this.value;
//            var options = '';
//            $.ajax({
//                type: 'POST',
//                url: 'get-cities-by-state-id',
//                data: {
//                    state_id: state_id
//                },
//                cache: false,
//                success: function(response) {
//
//                    alert(response);
//                    var obj = JSON.parse(response);
//                    for (var i = 0; i < obj.length; i++) {
//                        options += '<option value="' + obj[i].id + '">' + obj[i].name + '</option>';
//                    }
//
//                    $("select#city_id").html(options);
//                }
//            });
//        });
//    }



{
    $("#profileEditForm").on('submit', (function(e) {



        e.preventDefault();
        var over = '<div id="overlay">' +
        '<img id="loading" src="public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');
        $.ajax(
        {
            url: baseurl + '/save-profile',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data)
            {

                if (data.indexOf("Update Profile Successfully") > -1)
                {
                    $('#overlay').remove();

                    $('#profileValidationErrorMessage').slideUp('slow')
                    $('#profileSuccessMessage').html(data).hide();
                    $('#profileSuccessMessage').slideDown('slow');
                }
                else
                {
                    $('#overlay').remove();
                    $('#profileSuccessMessage').slideUp('slow')
                    $('#profileValidationErrorMessage').html(data).hide();
                    $('#profileValidationErrorMessage').slideDown('slow');
                }
            },
            error: function()
            {

                $('#overlay').remove();
                $('#profileErrorMessage').slideDown();
            }
        });
}));
}





{
    $("#UserEditForm").on('submit', (function(e) {

        var id = $('#user_id').val();


        e.preventDefault();
        var over = '<div id="overlay">' +
        '<img id="loading" src="' + baseurl + '/public/assets/img/loader.gif">' +
        '</div>';
        $(over).appendTo('body');
        $.ajax(
        {
            url: baseurl + '/save-profile-admin/' + id,
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data)
            {

                if (data.indexOf("Update Profile Successfully") > -1)
                {
                    $('#overlay').remove();

                    $('#profileValidationErrorMessage').slideUp('slow')
                    $('#profileSuccessMessage').html(data).hide();
                    $('#profileSuccessMessage').slideDown('slow');
                }
                else
                {
                    $('#overlay').remove();
                    $('#profileSuccessMessage').slideUp('slow')
                    $('#profileValidationErrorMessage').html(data).hide();
                    $('#profileValidationErrorMessage').slideDown('slow');
                }
            },
            error: function()
            {

                $('#overlay').remove();
                $('#profileErrorMessage').slideDown();
            }
        });
}));
}


{
    $("#addVendorForm").on('submit', (function(e) {

     var over = '<div id="overlay">' +
     '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
     '</div>';
     $(over).appendTo('body');
     
     
     e.preventDefault();
     $.ajax(
     {
        url: 'process-add-vendor',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(data)
        {
            if (data.indexOf("Successfully ! Vendor Created!") > -1)
            {
                window.location=baseurl+"/list-vendors";
                $('#addVendorValidationErrorMessage').slideUp('slow')
                $('#addVendorSuccessMessage').html(data).hide();
                $('#addVendorSuccessMessage').slideDown('slow');

            }
            else
            {
                $('#addVendorSuccessMessage').slideUp('slow')
                $('#addVendorValidationErrorMessage').html(data).hide();
                $('#addVendorValidationErrorMessage').slideDown('slow');
            }
        },
        error: function()
        {
            $('#addVendorErrorMessage').slideDown();
        }
    });
 }));
}




{
    $("#addSpecialPriceForm").on('submit', (function(e) {
        e.preventDefault();
        $.ajax(
        {
            url: 'add-special-prices',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data)
            {
                if (data.indexOf("Successfully ! Added Special Price!") > -1)
                {
                    $('#addSpecialPriceValidationErrorMessage').slideUp('slow')
                    $('#addSpecialPriceSuccessMessage').html(data).hide();
                    $('#addSpecialPriceSuccessMessage').slideDown('slow');

                }
                else
                {
                    $('#addSpecialPriceSuccessMessage').slideUp('slow')
                    $('#addSpecialPriceValidationErrorMessage').html(data).hide();
                    $('#addSpecialPriceValidationErrorMessage').slideDown('slow');
                }
            },
            error: function()
            {
                $('#addSpecialPriceErrorMessage').slideDown();
            }
        });
}));
}



{
    $("#editSpecialPriceForm").on('submit', (function(e) {
        var special_price_id = $('.submit_form').attr('id');
        e.preventDefault();
        $.ajax(
        {
            url: '../edit-special-price/' + special_price_id,
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data)
            {
                if (data.indexOf("Successfully ! Added Special Price!") > -1)
                {

                    $('#editSpecialPriceValidationErrorMessage').slideUp('slow')
                    $('#editSpecialPriceSuccessMessage').html(data).hide();
                    $('#editSpecialPriceSuccessMessage').slideDown('slow');

                }
                else
                {
                    $('#editSpecialPriceSuccessMessage').slideUp('slow')
                    $('#editSpecialPriceValidationErrorMessage').html(data).hide();
                    $('#editSpecialPriceValidationErrorMessage').slideDown('slow');
                }
            },
            error: function()
            {
                $('#editSpecialPriceErrorMessage').slideDown();
            }
        });

}));
}

{
    $('#submenuVendorDashboard').click(function() {
        $.ajax({
            type: "GET",
            url: "vendor-dashboard",
            success: function(data) {
                $('#content').empty();
                $('#content').html(data);
                return false;
            }
        })
    })
}



//    {
//        $('.uploadbeforeimages').click(function(e){
//            var myvar= e.currentTarget.attributes['data-target'].value;
//            var form=$(myvar+' form').attr('id');
//            $("#"+form).dropzone(
//            {
//                autoProcessQueue: false,
//                init: function() {
//                var submitButton = document.querySelector(".submit-all")
//                    myDropzone = this; // closure
//
//                submitButton.addEventListener("click", function() {
//                    alert('hello'+form);
//                  myDropzone.processQueue(); // Tell Dropzone to process all queued files.
//                });
//
//                // You might want to show the submit button only when
//                // files are dropped here:
//                this.on("addedfile", function() {
//                  // Show submit button here and/or inform user to click it.
//                });
//
//              }
//            });
//        })
//    }
//    {
//
//       Dropzone.options.myDropzone = {
//
//             autoProcessQueue: false,
//
//              init: function() {
//                var submitButton = document.querySelector("#submit-all")
//                    myDropzone = this; // closure
//
//                submitButton.addEventListener("click", function() {
//                    alert('hello');
//                  myDropzone.processQueue(); // Tell Dropzone to process all queued files.
//                });
//
//                // You might want to show the submit button only when
//                // files are dropped here:
//                this.on("addedfile", function() {
//                  // Show submit button here and/or inform user to click it.
//                });
//
//              }
//          };
//
//    }

});

jQuery(document).ready(function() {
    "use strict";
    var options = {
        common: {
            onLoad: function() {

            },
            onKeyUp: function(evt, data) {

            }
        },
        rules: {},
        ui: {
            showPopover: false,
            showErrors: true,
            bootstrap2: true,
        }
    };

    $('#password').pwstrength(options);
});
function viewAsset(asset_id)
{


    $.ajax({
        type: "GET",
        url: baseurl + "/asset-view/" + asset_id,
        success: function(data) {
            $('#showServiceid').empty();
            $('#showServiceid').modal('show')
            
            $('#showServiceid').html(data);
            return false;
        }
    })

}


function showMaintenanceServices(maintenance_request_id)
{

 var over = '<div id="overlay">' +
 '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
 '</div>';
 $(over).appendTo('body');
 
 $.ajax({
    type: "GET",
    url: baseurl + "/show-maintenance-services/" + maintenance_request_id,
    success: function(data) {
       
      $('#overlay').remove();
      $('#showVendorList').html('');
      $('#showVendorList').modal('show')
      
      $('#showVendorList').html(data);
      return false;
  }
})

}



$(document).ready(function() {
    $("[rel='popover']").popover({
        html: 'true',
        content: '<div id="popOverBox">1) Should contain minimum 10 characters. <br> 2)Should contain 2 special characters. <br> 3) Should contain 2 upper case.</div>'

});
});

//Assign services by admin
function assign_request()
{
    var request_id = $('input[name="request_id"]').val();
    var vendor_id = $('input[name="vendor"]:checked').val();
    var services_ids = $('input[name="services[]"]:checked').map(function() {
        return $(this).val();
    }).get();
    if ($('input[name="vendor"]:checked').length <= 0)
    {

     $('#errorMessage').addClass("alert alert-alert");
     $('#errorMessage').slideUp('slow');
     $('#errorMessage').html("Please select vendor").hide();
     $('#errorMessage').slideDown('slow');

 }
 else if ($('input[name="services[]"]:checked').length <= 0)
 {

     $('#errorMessage').addClass("alert alert-alert");
     $('#errorMessage').slideUp('slow');
     $("#errorMessage").html("Please select at least one service");
     $('#errorMessage').slideDown('slow');

 }
 else
 {
    $("#assignRequest").submit();
}


}
function deleteOrderAllImages(order_id, before_image)
{
    $.ajax({
        type: 'POST',
        url: '../delete-order-all-before-image',
        data: {order_id: order_id, before_image: before_image},
        dataType: 'html',
        success: function(data) {
            console.log(data);
            alert('success');
        }
    });
}
function removeImage(order_id, order_detail_id, obj, type)
{

    var filename = $(obj).data('value');
    $.ajax({
        type: 'POST',
        url: '../delete-before-image-id',
        data: {order_id: order_id, order_detail_id: order_detail_id, filename: filename, type: type},
        dataType: 'html',
        success: function(data) {
            var imageData = $(obj).parent('div');
            imageData.slideUp();
        }
    });
}

function popModal(order_id, order_detail_id, type)
{
    $.ajax({
        type: 'POST',
        url: '../display-order-images',
        data: {order_id: order_id, order_detail_id: order_detail_id, type: type},
        dataType: 'html',
        success: function(data) {
            if (data)
            {
                if (type == 'before')
                {
                    $('#before_view_modal_image_' + order_detail_id).html(data);
                }
                else
                {
                    $('#after_view_modal_image_' + order_detail_id).html(data);
                }

            }
            else
            {
                if (type == 'before')
                {
                    $('#before_view_modal_image_' + order_detail_id).html('<h1>There is No Images</h1>');
                }
                else
                {
                    $('#after_view_modal_image_' + order_detail_id).html('<h1>There is No Images</h1>');
                }

            }
        }
    });
}


function popViewModal(order_id, order_detail_id, type)
{
    $.ajax({
        type: 'POST',
        url: '../display-order-images-view',
        data: {order_id: order_id, order_detail_id: order_detail_id, type: type},
        dataType: 'html',
        success: function(data) {
            if (data)
            {
                if (type == 'before')
                {
                    $('#before_view_modal_image_' + order_detail_id).html(data);
                }
                else
                {
                    $('#after_view_modal_image_' + order_detail_id).html(data);
                }

            }
            else
            {
                if (type == 'before')
                {
                    $('#before_view_modal_image_' + order_detail_id).html('<h1>There is No Images</h1>');
                }
                else
                {
                    $('#after_view_modal_image_' + order_detail_id).html('<h1>There is No Images</h1>');
                }

            }
        }
    });
}

function saveVendorNote(order_id, order_detail_id)
{
    var vendor_note = $('#vendor-note-' + order_id + '-' + order_detail_id).val();
    if (vendor_note == '')
    {
        $('#vendor-note-empty-error-' + order_detail_id).slideDown("slow");
        return false;
    }
    else
    {
        $.ajax({
            type: 'POST',
            url: '../save-vendor-note',
            data: {order_id: order_id, order_detail_id: order_detail_id, vendor_note: vendor_note},
            dataType: 'html',
            success: function(data) {
                data += ('<br><button class="btn btn-primary" id="edit-vendor-note-button-' + order_id + '-' + order_detail_id + '" onclick="editVendorNoteButton(' + order_id + ',' + order_detail_id + ')">Edit Note </button>');
                $('#vendor-note-empty-error-' + order_detail_id).slideUp("slow");
                $('#vendor-note-empty-success-' + order_detail_id).slideDown("slow");
//               $('#vendor-note-'+order_id+'-'+order_detail_id).fadeOut("slow");

$('#textarea-vendor-note-' + order_id + '-' + order_detail_id).fadeOut("slow");
$('#show-vendor-note-' + order_id + '-' + order_detail_id).html(data);
$('#show-vendor-note-' + order_id + '-' + order_detail_id).html(data).slideDown('slow');
}
});
    }

}

function editVendorNoteButton(order_id, order_detail_id)
{
    $('#vendor-note-empty-success-' + order_detail_id).slideUp("slow");
    $('#show-vendor-note-' + order_id + '-' + order_detail_id).slideUp('slow');
    $('#textarea-vendor-note-' + order_id + '-' + order_detail_id).fadeIn("slow");
}




//Decline services by Vendor
function decline_request(request_id, vendor_id)
{
  $(".btn-success").attr("disabled","disabled");
  $(".btn-danger").attr("disabled","disabled"); 
  $.ajax({
    type: 'Post',
    url: baseurl + '/decline-request',
    data: {
        request_id: request_id,
        vendor_id: vendor_id
    },
    cache: false,
    success: function(response) {


      
        $('#errorMessage').focus();
        $('#errorMessage').addClass("alert alert-error");
        $('#errorMessage').slideUp('slow');
        $('#errorMessage').html(response).hide();
        $('#errorMessage').slideDown('slow');


    }
});


}
//Accept by Vendor
function accept_request(request_id, vendor_id)
{
  $(".btn-success").attr("disabled","disabled");
  $(".btn-danger").attr("disabled","disabled"); 

  $.ajax({
    type: 'Post',
    url: baseurl + '/accept-request',
    data: {
        request_id: request_id,
        vendor_id: vendor_id
    },
    cache: false,
    success: function(response) {

        $('#errorMessage').focus();
        $('#errorMessage').addClass("alert alert-success");
        $('#errorMessage').slideUp('slow');
        $('#errorMessage').html(response).hide();
        $('#errorMessage').slideDown('slow');



    }
});


}
//Bid Accept by Customer
function accept_bid_request(request_id, vendor_id)
{
  $(".btn-success").attr("disabled","disabled");
  $(".btn-danger").attr("disabled","disabled"); 

  $.ajax({
    type: 'Post',
    url: baseurl + '/accept-bid-request',
    data: {
        request_id: request_id,
        vendor_id: vendor_id
    },
    cache: false,
    success: function(response) {
        $('#errorMessage').focus();
        $('#errorMessage').addClass("alert alert-success");
        $('#errorMessage').slideUp('slow');
        $('#errorMessage').html(response).hide();
        $('#errorMessage').slideDown('slow');

        

    }
});


}

//Bid decline by Customer
function decline_bid_request (request_id, vendor_id)
{
    $(".btn-success").attr("disabled","disabled");
    $(".btn-danger").attr("disabled","disabled"); 

    $.ajax({
        type: 'Post',
        url: baseurl + '/decline-bid-request',
        data: {
            request_id: request_id,
            vendor_id: vendor_id
        },
        cache: false,
        success: function(response) {
         $('#errorMessage').focus();
         $('#errorMessage').addClass("alert alert-error");
         $('#errorMessage').slideUp('slow');
         $('#errorMessage').html(response).hide();
         $('#errorMessage').slideDown('slow');


     }
 });


}

//Bid Accept by Customer
function admin_accept_bid_request(request_id, vendor_id)
{

   $(".btn-success").attr("disabled","disabled");
   $(".btn-danger").attr("disabled","disabled"); 
   $.ajax({
    type: 'Post',
    url: baseurl + '/admin-accept-bid-request',
    data: {
        request_id: request_id,
        vendor_id: vendor_id
    },
    cache: false,
    success: function(response) {

     $('#errorMessage').focus();
     $('#errorMessage').addClass("alert alert-success");
     $('#errorMessage').slideUp('slow');
     $('#errorMessage').html(response).hide();
     $('#errorMessage').slideDown('slow');


 }
});


}

//Bid decline by Customer
function admin_decline_bid_request (request_id, vendor_id)
{
    $(".btn-success").attr("disabled","disabled");
    $(".btn-danger").attr("disabled","disabled"); 
    $.ajax({
        type: 'Post',
        url: baseurl + '/admin-decline-bid-request',
        data: {
            request_id: request_id,
            vendor_id: vendor_id
        },
        cache: false,
        success: function(response) {
         $('#errorMessage').focus();
         $('#errorMessage').addClass("alert alert-error");
         $('#errorMessage').slideUp('slow');
         $('#errorMessage').html(response).hide();
         $('#errorMessage').slideDown('slow');


     }
 });


}

//Change Status to Read for Notifications
function ChangeNotificationStatus(notification_id,notification_url)
{
    var over = '<div id="overlay">' +
    '<img id="loading" src="'+baseurl+'public/assets/img/loader.gif">' +
    '</div>';
    $(over).appendTo('body');

    $.ajax({
        type: 'Post',
        url: baseurl + '/change-notification-status',
        data: {
            notification_id: notification_id,
            notification_url:notification_url
            
        },
        cache: false,
        success: function(response) {
            
          window.location=notification_url;


      }
  });
}

function showDetails(prefix)
{
    if($('#' + prefix).is(":checked"))
    {
       $('#'+prefix+'_configuration').show();
   }
   else
   {
    $('#'+prefix+'_configuration').hide();
}

}

function replicateValues(fieldID,objectvalue)
{

    $("#"+fieldID).val($(objectvalue).val());

}

function appendValues(fieldID,objectvalue)
{
    if($(objectvalue).is(":checked"))
    {
        $("#"+fieldID).val($("#"+fieldID).val()+","+$(objectvalue).val());
    }
    else
    {
        var $stringData=$("#"+fieldID).val();

        var $newDATA = $stringData.replace(","+$(objectvalue).val(), "");

        $("#"+fieldID).val($newDATA);
    }
}

function printDiv(divName) {
   var printContents = document.getElementById(divName).innerHTML;
   var originalContents = document.body.innerHTML;

   document.body.innerHTML = printContents;

   window.print();

   document.body.innerHTML = originalContents;
}

//Accept by Vendor
function accept_single_request(request_id, vendor_id,service_id)
{
  $(".btn-success").attr("disabled","disabled");
  $(".btn-danger").attr("disabled","disabled"); 

  $.ajax({
    type: 'Post',
    url: baseurl + '/accept-single-request',
    data: {
        request_id: request_id,
        vendor_id : vendor_id,
        service_id: service_id
    },
    cache: false,
    success: function(response) {

        $('#errorMessage').focus();
        $('#errorMessage').addClass("alert alert-success");
        $('#errorMessage').slideUp('slow');
        $('#errorMessage').html(response).hide();
        $('#errorMessage').slideDown('slow');



    }
});


}


$('body').delegate( ".btn-minimize", "click", function(){
    $('.requestServices .accordion-group').slideUp(0);
    $(this).closest('.listSlide').find('.accordion-group').slideDown();
});

$('.fieldBox .accrodSlid').hide();
$('.fieldBox .frstStep').show();
$('body').delegate( ".fieldBox .box-header", "click", function(){
    $('.fieldBox .accrodSlid').slideUp(0);
    $(this).next('.accrodSlid').slideDown();
});