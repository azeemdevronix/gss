-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2014 at 07:38 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gssdev`
--
CREATE DATABASE IF NOT EXISTS `gssdev` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gssdev`;

-- --------------------------------------------------------

--
-- Table structure for table `access_functions`
--

CREATE TABLE IF NOT EXISTS `access_functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_number` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `zip` int(10) unsigned DEFAULT NULL,
  `loan_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `agent` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `property_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `electric_status` tinyint(1) DEFAULT NULL,
  `water_status` tinyint(1) DEFAULT NULL,
  `gas_status` tinyint(1) DEFAULT NULL,
  `utility_note` longtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `property_address` text COLLATE utf8_unicode_ci,
  `lock_box` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brokage` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email_address` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carbon_copy_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outbuilding_shed` tinyint(1) DEFAULT NULL,
  `outbuilding_shed_note` text COLLATE utf8_unicode_ci,
  `special_direction_note` text COLLATE utf8_unicode_ci,
  `swimming_pool` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assets_customer_id_foreign` (`customer_id`),
  KEY `assets_city_id_foreign` (`city_id`),
  KEY `assets_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `asset_number`, `customer_id`, `address`, `city_id`, `state_id`, `zip`, `loan_number`, `property_type`, `agent`, `property_status`, `electric_status`, `water_status`, `gas_status`, `utility_note`, `status`, `created_at`, `updated_at`, `property_address`, `lock_box`, `access_code`, `brokage`, `customer_email_address`, `carbon_copy_email`, `outbuilding_shed`, `outbuilding_shed_note`, `special_direction_note`, `swimming_pool`, `latitude`, `longitude`) VALUES
(26, 10001, 2, NULL, 1, 1, 890049, 'Loan Number', 'single family', 'Agent', 'active', 0, 1, 0, 'Utility Note over here', 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', 'Street#20 App B-10 Brooklyn', 'Lock Box', 'Access code over here', 'Brokage', NULL, NULL, 1, 'Out building Shed note over here', 'This is special direction note', 'pool', NULL, NULL),
(27, 14534, 2, NULL, 1, 1, 890049, 'Loan Number', 'single family', 'Agent', 'active', 0, 1, 0, 'Utility Note over here', 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', 'Street#20 App B-10 Brooklyn', 'Lock Box', 'Access code over here', 'Brokage', NULL, NULL, 1, 'Out building Shed note over here', 'This is special direction note', 'pool', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assign_requests`
--

CREATE TABLE IF NOT EXISTS `assign_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `requested_service_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `assign_requests_request_id_foreign` (`request_id`),
  KEY `assign_requests_requested_service_id_foreign` (`requested_service_id`),
  KEY `assign_requests_vendor_id_foreign` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cities_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=61 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Abbeville', 1, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(2, 'Abernant', 1, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(3, 'Adamsville', 1, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(4, 'Addison', 1, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(5, 'Adger', 1, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(6, 'Adak', 2, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(7, 'Akiachak', 2, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(8, 'Chefornak', 2, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(9, 'Buckland', 2, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(10, 'Douglas', 2, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(11, 'Aguila', 3, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(12, 'Arlington', 3, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(13, 'Catalina', 3, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(14, 'Central', 3, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(15, 'Concho', 3, 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(16, 'Acampo', 4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(17, 'Adelanto', 4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(18, 'Angwin', 4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(19, 'Baker', 4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(20, 'Big Bar', 4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_type_id` int(10) unsigned DEFAULT NULL,
  `image_path` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `images_image_type_id_foreign` (`image_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `image_types`
--

CREATE TABLE IF NOT EXISTS `image_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned NOT NULL,
  `request_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `invoices_order_id_foreign` (`order_id`),
  KEY `invoices_request_id_foreign` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_requests`
--

CREATE TABLE IF NOT EXISTS `maintenance_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `asset_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `maintenance_requests_customer_id_foreign` (`customer_id`),
  KEY `maintenance_requests_asset_id_foreign` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_09_08_154521_user_types', 1),
('2014_09_08_155251_user_roles', 1),
('2014_09_08_155516_services', 1),
('2014_09_08_155709_states', 1),
('2014_09_08_155835_image_types', 1),
('2014_09_08_155947_notification_types', 1),
('2014_09_08_160153_role_functions', 1),
('2014_09_08_160725_users', 1),
('2014_09_08_161328_user_role_details', 1),
('2014_09_08_162630_cities', 1),
('2014_09_08_162953_assets', 1),
('2014_09_08_163438_maintenance_requests', 1),
('2014_09_08_163714_requested_services', 1),
('2014_09_08_164310_recurrings', 1),
('2014_09_08_164501_orders', 1),
('2014_09_08_165243_order_details', 1),
('2014_09_08_165602_vendor_services', 1),
('2014_09_08_165821_invoices', 1),
('2014_09_08_170246_order_images', 1),
('2014_09_08_170800_activities', 1),
('2014_09_08_171156_notifications', 1),
('2014_09_08_171607_images', 1),
('2014_09_08_171814_assign_requests', 1),
('2014_09_08_173019_special_prices', 1),
('2014_09_05_133221_update_asset', 2),
('2014_09_09_104018_update_asset1', 3),
('2014_09_09_122802_update_requested_service', 4),
('2014_09_09_150452_update_services', 5),
('2014_09_09_160756_update_orders', 6),
('2014_09_10_161556_update_users', 7),
('2014_09_10_162302_update_users1', 8),
('2014_09_11_140422_update_users2', 9),
('2014_09_15_120131_service_images', 10),
('2014_09_12_134717_update_requested_services', 11),
('2014_09_11_135947_update_role_functions', 12),
('2014_09_12_150713_create_access_functions', 13),
('2014_09_12_152122_update_role_function', 13),
('2014_09_17_160522_services_new_columns', 13),
('2014_09_19_093230_create_password_reminders_table', 14),
('2014_09_22_170135_userupdate22914', 15),
('2014_09_22_170511_updateassets22914', 15),
('2014_09_23_131509_UpdateUser', 16),
('2014_09_23_204319_update_service', 16),
('2014_09_24_144341_UpdateUserRoleDetail', 17),
('2014_09_26_132118_UpdateUserTable', 18),
('2014_09_26_140935_UpdateUserUsernameTable', 19),
('2014_09_30_073153_NotificationForeignKeyDrop', 20),
('2014_09_30_073721_UpdateRequestedFields', 21);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned DEFAULT NULL,
  `recepient_id` int(10) unsigned DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `activity_id` int(10) unsigned DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `notifications_activity_id_foreign` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_types`
--

CREATE TABLE IF NOT EXISTS `notification_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `total_amount` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_request_id_foreign` (`request_id`),
  KEY `orders_vendor_id_foreign` (`vendor_id`),
  KEY `orders_customer_id_foreign` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `order_details_order_id_foreign` (`order_id`),
  KEY `order_details_service_id_foreign` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_images`
--

CREATE TABLE IF NOT EXISTS `order_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `type` enum('before','after') COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `order_images_order_id_foreign` (`order_id`),
  KEY `order_images_service_id_foreign` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_config`
--

CREATE TABLE IF NOT EXISTS `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `module` varchar(40) NOT NULL,
  `cfgkey` varchar(40) NOT NULL,
  `cfgval` varchar(40) NOT NULL,
  `cfgtype` varchar(40) NOT NULL,
  `cfgopts` text NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quickbooks_log`
--

INSERT INTO `quickbooks_log` (`quickbooks_log_id`, `quickbooks_ticket_id`, `batch`, `msg`, `log_datetime`) VALUES
(1, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/vendor/consolibyte/quickbooks/docs/web_connector/example_web_connector.php'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 11:15:22'),
(2, NULL, 0, 'Handler is starting up...: array (\n  ''qb_company_file'' => NULL,\n  ''qbwc_min_version'' => NULL,\n  ''qbwc_wait_before_next_update'' => NULL,\n  ''qbwc_min_run_every_n_seconds'' => NULL,\n  ''qbwc_version_warning_message'' => NULL,\n  ''qbwc_version_error_message'' => NULL,\n  ''qbwc_interactive_url'' => NULL,\n  ''autoadd_missing_requestid'' => true,\n  ''check_valid_requestid'' => true,\n  ''server_version'' => ''PHP QuickBooks SOAP Server v3.0 at /gss-dev/php/qb'',\n  ''authenticate'' => NULL,\n  ''authenticate_dsn'' => NULL,\n  ''map_application_identifiers'' => true,\n  ''allow_remote_addr'' => \n  array (\n  ),\n  ''deny_remote_addr'' => \n  array (\n  ),\n  ''convert_unix_newlines'' => true,\n  ''deny_concurrent_logins'' => false,\n  ''deny_concurrent_timeout'' => 60,\n  ''deny_reallyfast_logins'' => false,\n  ''deny_reallyfast_timeout'' => 600,\n  ''masking'' => true,\n)', '2014-09-22 15:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_oauth`
--

CREATE TABLE IF NOT EXISTS `quickbooks_oauth` (
  `quickbooks_oauth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_username` varchar(255) NOT NULL,
  `app_tenant` varchar(255) NOT NULL,
  `oauth_request_token` varchar(255) DEFAULT NULL,
  `oauth_request_token_secret` varchar(255) DEFAULT NULL,
  `oauth_access_token` varchar(255) DEFAULT NULL,
  `oauth_access_token_secret` varchar(255) DEFAULT NULL,
  `qb_realm` varchar(32) DEFAULT NULL,
  `qb_flavor` varchar(12) DEFAULT NULL,
  `qb_user` varchar(64) DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_oauth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) NOT NULL,
  `msg` text,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_recur`
--

CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `ticket` char(36) NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) DEFAULT NULL,
  `lasterror_msg` varchar(255) DEFAULT NULL,
  `ipaddr` char(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(255) NOT NULL,
  `qb_company_file` varchar(255) DEFAULT NULL,
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quickbooks_user`
--

INSERT INTO `quickbooks_user` (`qb_username`, `qb_password`, `qb_company_file`, `qbwc_wait_before_next_update`, `qbwc_min_run_every_n_seconds`, `status`, `write_datetime`, `touch_datetime`) VALUES
('quickbooks', '725cbb4eb5719aabdf405267531b8e8ac9ac454c', '', 0, 0, 'e', '2014-09-22 11:15:21', '2014-09-22 11:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `recurrings`
--

CREATE TABLE IF NOT EXISTS `recurrings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_service_id` int(10) unsigned DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `assignment_type` enum('single','multiple') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `recurrings_request_service_id_foreign` (`request_service_id`),
  KEY `recurrings_vendor_id_foreign` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `requested_services`
--

CREATE TABLE IF NOT EXISTS `requested_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `required_date` date DEFAULT NULL,
  `required_time` time DEFAULT NULL,
  `service_men` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_note` text COLLATE utf8_unicode_ci,
  `verified_vacancy` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_for_keys_trash_out` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trash_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storage_shed` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_size` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requested_services_requested_id_foreign` (`request_id`),
  KEY `requested_services_service_id_foreign` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `role_functions`
--

CREATE TABLE IF NOT EXISTS `role_functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_function` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_function_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `role_functions`
--

INSERT INTO `role_functions` (`id`, `status`, `created_at`, `updated_at`, `role_function`, `access_function_id`) VALUES
(1, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'User', 1),
(2, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Access Level', 1),
(3, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Access Rights', 1),
(4, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Customer', 2),
(5, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Asset', 3),
(6, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Vendor', 4),
(7, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Maintenance Request', 5),
(8, 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 'Service', 6);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `req_date` tinyint(1) DEFAULT NULL,
  `number_of_men` tinyint(1) DEFAULT NULL,
  `verified_vacancy` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys` tinyint(4) NOT NULL DEFAULT '0',
  `cash_for_keys_trash_out` tinyint(4) NOT NULL DEFAULT '0',
  `trash_size` tinyint(4) NOT NULL DEFAULT '0',
  `storage_shed` tinyint(4) NOT NULL DEFAULT '0',
  `lot_size` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=157 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `price`, `status`, `created_at`, `updated_at`, `service_code`, `req_date`, `number_of_men`, `verified_vacancy`, `cash_for_keys`, `cash_for_keys_trash_out`, `trash_size`, `storage_shed`, `lot_size`) VALUES
(145, 'interior / Exterior Painting', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, 1, 1, 1, 1, 1, 1, 1, 1),
(146, 'Flooring (Carpet/ Wood/ Tile)', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0),
(147, 'Drywall - New/ Repairs', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, 1, 0, 1, 0, 0, 0, 0),
(148, 'Countertops/ Cabinets', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0),
(149, ' Carpentry', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(150, 'Cleaning/ Trush Removal', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, 1, 1, 0, 0, 0, 0, 0, 0),
(151, 'Electrical', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, 1, 0, 0, 0, 0, 0, 0),
(152, 'Garage Doors', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(153, 'Plumbing', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(154, 'Fences', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(155, 'Ceramic Tile', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0),
(156, 'Roofing', 0, 1, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE IF NOT EXISTS `service_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `requested_id` int(10) unsigned DEFAULT NULL,
  `image_name` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `image_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `service_images_requested_id_foreign` (`requested_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Table structure for table `special_prices`
--

CREATE TABLE IF NOT EXISTS `special_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `special_price` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `special_prices_customer_id_foreign` (`customer_id`),
  KEY `special_prices_service_id_foreign` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alabama', 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(2, 'Alaska', 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(3, 'Arizona', 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00'),
(4, 'California', 1, '2014-09-30 07:10:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_1` text COLLATE utf8_unicode_ci,
  `address_2` text COLLATE utf8_unicode_ci,
  `zipcode` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `user_role_id` int(10) unsigned DEFAULT NULL,
  `profile_status` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `profile_picture` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_type_id_foreign` (`type_id`),
  KEY `users_user_role_id_foreign` (`user_role_id`),
  KEY `users_city_id_foreign` (`city_id`),
  KEY `users_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `email`, `username`, `password`, `phone`, `address_1`, `address_2`, `zipcode`, `profile_image`, `type_id`, `user_role_id`, `profile_status`, `status`, `remember_token`, `created_at`, `updated_at`, `city_id`, `state_id`, `profile_picture`, `latitude`, `longitude`) VALUES
(1, 'Admin', 'admin', NULL, 'admin@invortex.com', 'admin', '$2y$10$xMK5r0AoM/KfpWYdVAnk9uDVEZoQmRLBbiDx78bxOWY9Hutdq7l9y', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, '3FtNt051I4pZ1XHOM8i9UtMCyw1eM2P88fv5paDrbNBIeUyYJ5Rro1OLVeeA', '2014-09-30 07:10:42', '2014-09-30 02:35:17', NULL, NULL, NULL, NULL, NULL),
(2, 'Customer', 'customer', NULL, 'customer@invortex.com', 'customer', '$2y$10$nuowUFB2OlCc4ptCOEROp.XO7MTcNDkAQzrIxwbduX5bEH9saDs6m', NULL, NULL, NULL, NULL, NULL, 2, 1, 1, 1, NULL, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL),
(3, 'Vendor', 'Vendor', NULL, 'vendor@invortex.com', 'customer', '$2y$10$LmB6qyoSyPWSYcdaVRHLc.fR9uTaJs8U3l7xyQUOEw7P56XikDovi', NULL, NULL, NULL, NULL, NULL, 3, 1, 1, 1, NULL, '2014-09-30 07:10:42', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'This is the admin role', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(2, 'Customer', 'This is the customer role', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(3, 'Vendors', 'This is the vendor role', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(4, 'Manager', 'This is the manager manaigin the services', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(5, 'Admin Assitant', 'He will be assisting the Admin', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_details`
--

CREATE TABLE IF NOT EXISTS `user_role_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `role_function_id` int(10) unsigned DEFAULT NULL,
  `add` tinyint(1) DEFAULT NULL,
  `edit` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_role_details_role_id_foreign` (`role_id`),
  KEY `user_role_details_role_function_id_foreign` (`role_function_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1478 ;

--
-- Dumping data for table `user_role_details`
--

INSERT INTO `user_role_details` (`id`, `role_id`, `role_function_id`, `add`, `edit`, `delete`, `status`, `created_at`, `updated_at`, `view`) VALUES
(1438, 1, 1, 1, 1, 1, NULL, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 1),
(1439, 1, 2, 1, 1, 1, NULL, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 1),
(1440, 1, 3, 1, 1, 1, NULL, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 1),
(1441, 1, 4, 1, 1, 1, NULL, '2014-09-30 07:10:40', '0000-00-00 00:00:00', 1),
(1442, 1, 5, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1443, 1, 6, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1444, 1, 7, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1445, 1, 8, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1446, 2, 1, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1447, 2, 2, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1448, 2, 3, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1449, 2, 4, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1450, 2, 5, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1451, 2, 6, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1452, 2, 7, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1453, 2, 8, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1454, 3, 1, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1455, 3, 2, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1456, 3, 3, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1457, 3, 4, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1458, 3, 5, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1459, 3, 6, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1460, 3, 7, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1461, 3, 8, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1462, 4, 1, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1463, 4, 2, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1464, 4, 3, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1465, 4, 4, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1466, 4, 5, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1467, 4, 6, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1468, 4, 7, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1469, 4, 8, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1470, 5, 1, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1471, 5, 2, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1472, 5, 3, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1473, 5, 4, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1474, 5, 5, 1, 1, 1, NULL, '2014-09-30 07:10:41', '0000-00-00 00:00:00', 1),
(1475, 5, 6, 1, 1, 1, NULL, '2014-09-30 07:10:42', '0000-00-00 00:00:00', 1),
(1476, 5, 7, 1, 1, 1, NULL, '2014-09-30 07:10:42', '0000-00-00 00:00:00', 1),
(1477, 5, 8, 1, 1, 1, NULL, '2014-09-30 07:10:42', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20018 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(2, 'customer', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(3, 'vendors', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00'),
(4, 'user', 1, '2014-09-30 07:10:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_services`
--

CREATE TABLE IF NOT EXISTS `vendor_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `vendor_services_vendor_id_foreign` (`vendor_id`),
  KEY `vendor_services_service_id_foreign` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assign_requests`
--
ALTER TABLE `assign_requests`
  ADD CONSTRAINT `assign_requests_requested_service_id_foreign` FOREIGN KEY (`requested_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assign_requests_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assign_requests_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_image_type_id_foreign` FOREIGN KEY (`image_type_id`) REFERENCES `image_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `maintenance_requests`
--
ALTER TABLE `maintenance_requests`
  ADD CONSTRAINT `maintenance_requests_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `maintenance_requests_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_images`
--
ALTER TABLE `order_images`
  ADD CONSTRAINT `order_images_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_images_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recurrings`
--
ALTER TABLE `recurrings`
  ADD CONSTRAINT `recurrings_request_service_id_foreign` FOREIGN KEY (`request_service_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recurrings_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `requested_services`
--
ALTER TABLE `requested_services`
  ADD CONSTRAINT `requested_services_requested_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `maintenance_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `requested_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_requested_id_foreign` FOREIGN KEY (`requested_id`) REFERENCES `requested_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `special_prices`
--
ALTER TABLE `special_prices`
  ADD CONSTRAINT `special_prices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `special_prices_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_role_details`
--
ALTER TABLE `user_role_details`
  ADD CONSTRAINT `user_role_details_role_function_id_foreign` FOREIGN KEY (`role_function_id`) REFERENCES `role_functions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_role_details_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vendor_services`
--
ALTER TABLE `vendor_services`
  ADD CONSTRAINT `vendor_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vendor_services_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
