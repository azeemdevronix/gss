<?php

class AjaxController extends \BaseController {

    //Get all cities by state
    public function getCitiesByState() {
        // Get all post data through ajax
        $data = Input::all();
        //Check if request is ajax
        if (Request::ajax()) {
            //Get all cities by state id from City Model
            $cities = City::getCitiesByStateId($data['state_id']);
            return json_encode($cities);
        } else {
            return false;
        }
    }

    //Get asset information by id
    public function getAssetById() {
        // Get all post data through ajax
        $data = Input::all();
        //Check if request is ajax
        if (Request::ajax()) {
            //Get all asset information by asset_id


        

            $asset_information = Asset::getAssetInformationById($data['asset_id']);
           
           return View::make('pages.customer.asset_information')
            ->with('asset_information', $asset_information)
            ->with('latitude', $asset_information->latitude)
            ->with('longitude', $asset_information->longitude);
            // return json_encode($asset_information);
        } else {
            return false;
        }
    }

    /**
     * Get the popup of service through ajax
     * @params none
     * @return error if asset_number is null or load service information view
     */
    public function getServicePopup() {
        $data = Input::all();

        $id = $data['service_id'];
        $asset_number = $data['asset_number'];

        if ($asset_number != "") {
            $string_ids = implode(',', $id);

            $array_id = explode(',', $string_ids);

            $last_service_id = end($array_id);
            $service_data = Service::find($last_service_id);

       
            $serviceTypeArray=array();
            $serviceValueArray=array();
            $serviceTypes=   ServiceFieldDetail::getServiceFieldById($last_service_id);
            $serviceTypeArray['number_of_men']='';
            $serviceTypeArray['verified_vacancy']='';
            $serviceTypeArray['cash_for_keys']='';
            $serviceTypeArray['cash_for_keys_trash_out']=''; 
            $serviceTypeArray['trash_size']='';
            $serviceTypeArray['storage_shed']='';
            $serviceTypeArray['lot_size']='';

            $serviceTypeArray['set_prinkler_system_type']='';
            $serviceTypeArray['install_temporary_system_type']='';
            $serviceTypeArray['carpet_service_type']=''; 
            $serviceTypeArray['pool_service_type']='';
            $serviceTypeArray['boarding_type']='';
            $serviceTypeArray['spruce_up_type']='';
            $serviceTypeArray['constable_information_type']='';
            $serviceTypeArray['remove_carpe_type']='';
            $serviceTypeArray['remove_blinds_type']='';
            $serviceTypeArray['remove_appliances_type']='';
            $serviceTypeArray['due_date']='';


            $serviceValueArray['number_of_men']='';
            $serviceValueArray['verified_vacancy']='';
            $serviceValueArray['cash_for_keys']='';
            $serviceValueArray['cash_for_keys_trash_out']=''; 
            $serviceValueArray['trash_size']='';
            $serviceValueArray['storage_shed']='';
            $serviceValueArray['lot_size']='';

            $serviceValueArray['set_prinkler_system_type']='';
            $serviceValueArray['install_temporary_system_type']='';
            $serviceValueArray['carpet_service_type']=''; 
            $serviceValueArray['pool_service_type']='';
            $serviceValueArray['boarding_type']='';
            $serviceValueArray['spruce_up_type']='';
           

            $serviceValueArray['constable_information_type']='';
            $serviceValueArray['remove_carpe_type']='';
            $serviceValueArray['remove_blinds_type']='';
            $serviceValueArray['remove_appliances_type']='';
             $serviceValueArray['due_date']='';


            foreach($serviceTypes as  $value)
            {
                
                $serviceTypeArray[$value->fieldname]=$value->field_type;
                $serviceValueArray[$value->fieldname]=$value->field_values;
        
            }

            return View::make('pages.customer.service_information_popup')
                         ->with('serviceTypeArray',$serviceTypeArray)
                         ->with('serviceValueArray',$serviceValueArray)
                            ->with('service_data', $service_data);
        } else {

            return "error";
        }
    }

    /**
     * Get the popup of service through ajax
     * @params none
     * @return error if asset_number is null or load service information view
     */
    public function getServiceList() {

        $data = Input::all();

        
        $files = Input::file('service_image_' . $data['service_id']);
        $i = 0;
        foreach ($files as $file) {

            if ($file) {
                $destinationPath = Config::get('app.request_images');
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();

                //$unique_id = . $ext;
                $filename = substr(base_convert(time(), 10, 36) . md5(microtime()), 0, 16) . '.' . $extension;
                //$filename = $data['service_id'] . '-' . $i . '.' . $extension;
                $data['service_images_' . $data['service_id']][] = $filename;
                $file->move($destinationPath, $filename);
            } else {
                //$data['profile_picture'] = Auth::user()->profile_picture;
            }
            $i++;
        }

        return View::make('pages.customer.service_information_list')
                        ->with('data', $data);
        //return $data;
        //return View::make('pages.customer.service_information_list');
    }

    public static function generateAssetNumber() {

        $max_id = DB::table('assets')->max('id');
        $asset_id = $max_id + 1;

        $length = strlen($asset_id);
        $random_number = General::randomNumber(100, 999);

        if ($length < 2)
            $random_number .= '0';

        $asset_number = $random_number . $asset_id;
        return $asset_number;
    }

    public static function removeFile() {
        $data = Input::all();

        $path = Config::get('app.request_images');

        unlink($path.'/'.$data['image_name']);

        return $data['image_name'];
    }


    public function testingLink(){
        return View::make('test');
    }

     /**
     * Get the popup of service through ajax
     * @params none
     * @return error if asset_number is null or load service information view
     */
    public function getVendorServicePopup() {
        $data = Input::all();

        $id = $data['service_id'];
       
            $string_ids = implode(',', $id);

            $array_id = explode(',', $string_ids);

            $last_service_id = end($array_id);
            $service_data = Service::find($last_service_id);



            return View::make('pages.customer.service_information_popup')
                            ->with('service_data', $service_data);
       
    }

}
