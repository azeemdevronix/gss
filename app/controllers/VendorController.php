<?php

/**
 * Vendor Controller Class.
 *
 * Handles Vendor Functionalities & redirects.
 * @copyright Copyright 2014 Devronix Technology Development Team
 * @version $Id: 1.0
 */
use JeroenDesloovere\Geolocation\Geolocation;

class VendorController extends \BaseController {

    /**
     * Get Vendor to Vendor Dashboard.
     * @params none
     * @return Redirect to Vendor Dashboard.
     */
    public function index() {

        return View::make('pages.vendors.dashboard');
    }

    /**
     * Get Vendor to profile complete form.
     * @params none
     * @return Redirect to respective page either its add services or dashboard.
     */
    public function showCompleteProfile() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            $profile_status = Auth::user()->profile_status;
            if ($profile_status < 1) {
                $user_detail = User::find($id);
                $cities = City::getAllCities();
                $states = State::getAllStates();
                return View::make('pages.vendors.vendor_profile_complete')->with('cities', $cities)->with('states', $states)->with('user_detail', $user_detail);
            } else {
                return Redirect::to('edit-profile');
            }
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Handle profile complete data & populate database accordingly.
     * @params none
     * @return Error messages or redirect to Dashboard.
     */
    public function completeProfile() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            if (Input::get('save_continue')) {
                $redirect = 'vendor-profile-service';
            } elseif (Input::get('save_exit')) {
                $redirect = 'vendors';
            }
            if (Input::get('create_by_admin') == 'yes') {
                $username = Input::get('username');
                $rules = array(
                    'username' => 'required|unique:users',
                    'phone' => 'required|numeric',
                    'address_1' => 'required|min:8|max:100',
                    'zipcode' => 'required',
                    'state_id' => 'required',
                    'city_id' => 'required',
                    );
            } else {
                $username = Auth::user()->username;
                $rules = array(
                    'phone' => 'required|numeric',
                    'address_1' => 'required|min:8|max:100',
                    'zipcode' => 'required',
                    'state_id' => 'required',
                    'city_id' => 'required',
                    );
            }

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to('vendor-profile-complete')
                ->withErrors($validator)
                ->withInput(Input::except('profile_picture'));
            } else {

              $street = '';
              $streetNumber = '';
              $city_id = Input::get('city_id');
              $city = City::find($city_id)->name;
              $zip = Input::get('zipcode');
              $country = 'United States';
             //   $result = Geolocation::getCoordinates($street, $streetNumber, $city, $zip, $country);
              $property_address= Input::get('address_1');
              $state_id = Input::get('state_id');
              $state = State::find($state_id)->name;
              $result  =  Asset::getLatLong($property_address.$zip,$city,$state);
              $data = Input::all();
              $data['latitude'] = $result['lat'];
              $data['longitude'] = $result['lng'];


                 //User Notification Email for profile completeness
              
              $email_data = array(
                'user_email_template'=>EmailNotification::$user_email_completeness_template);
              Email::send(Auth::user()->email, 'Your profile has been completed', 'emails.user_email_template', $email_data);
                 //End Nofication Email Code

              $data['profile_status'] = 1;
              $file = Input::file('profile_picture');
                //This section will handel profile pictures.
              if ($file) {
                $destinationPath = Config::get('app.upload_path');
                $filename = $file->getClientOriginalName();
                $filename = str_replace('.', '-' . $username . '.', 'profile-' . $filename);
                $data['profile_picture'] = $filename;
                Input::file('profile_picture')->move($destinationPath, $filename);
            } else {
                $data['profile_picture'] = Auth::user()->profile_picture;
            }
            $save = User::profile($data, $id);
            if ($save) {
                return Redirect::to($redirect);
            }
        }
    } else {
        return Redirect::to('/');
    }
}

    /**
     * List All Assigned requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listAssignedRequests() {

        $user_id = Auth::user()->id;
        $requests = AssignRequest::where('vendor_id', '=', $user_id)->where('status',"=",1)->groupBy('request_id')->orderBy('id', 'desc')->get();
        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = AssignRequest::where('vendor_id', '=', $user_id)->where('request_id','=',$request->maintenanceRequest->id)->where('status','!=',2)->orderBy('id', 'desc')->get();
            $assign_requests[$i]['request_id'] = $request->maintenanceRequest->id;
            $assign_requests[$i]['status'] = $request->maintenanceRequest->status;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->maintenanceRequest->user->first_name .' '. $request->maintenanceRequest->user->last_name;
            $assign_requests[$i]['asset_number'] = $request->maintenanceRequest->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->maintenanceRequest->created_at;
            $assign_requests[$i]['emergency_request']=$request->maintenanceRequest->emergency_request;
            $assign_requests[$i]['due_date'] = $request->maintenanceRequest->created_at;
            foreach ($services as $service) {

                $assign_requests[$i]['service_code'].='&diams; '.$service->requestedService->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->requestedService->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.vendors.list_assign_requests')->with('assign_requests', $assign_requests);
    }

    /**
     * List All Orders.
     * @params none
     * @return List of Orders through AJAX.
     */
    public function listOrders() {
        $user_id = Auth::user()->id;
        $orders = Order::where('vendor_id', '=', $user_id)->orderBy('id', 'desc')->get();
        $list_orders = array();
        $i = 0;
        foreach ($orders as $order) {
            $order_details = ($order->orderDetail);
            $list_orders[$i]['order_id'] = $order->id;
            $list_orders[$i]['customer_name'] = $order->customer->first_name . ' ' . $order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $order->vendor->first_name . ' ' . $order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $order->created_at;
            $list_orders[$i]['service_name'] = '';

            $list_orders[$i]['property_address'] = $order->maintenanceRequest->asset->property_address;
            $list_orders[$i]['city'] = $order->maintenanceRequest->asset->city->name;
            $list_orders[$i]['state'] = $order->maintenanceRequest->asset->state->name;
            $list_orders[$i]['zipcode'] = $order->maintenanceRequest->asset->zip;
          

            $list_orders[$i]['status'] = $order->status;
            $list_orders[$i]['status_class'] = ($order->status==1)? "warning": $order->status_class; ;
            $list_orders[$i]['status_text'] = ($order->status==1)? "In-Process":$order->status_text;;



            foreach ($order_details as $order_detail) {
                $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }
        return View::make('pages.vendors.list_orders')->with('orders', $list_orders);
    }

    /**
     * List All Completed Orders.
     * @params none
     *status=2
     * @return List of Completed Orders through AJAX.
     */
    public function listCompletedOrders() {
        $user_id = Auth::user()->id;
        $orders = Order::where('vendor_id', '=', $user_id)->where('status', '=', '2')->orderBy('id', 'desc')->get();
        $list_orders = array();
        $i = 0;
        foreach ($orders as $order) {
            $order_details = $order->orderDetail;
            $list_orders[$i]['order_id'] = $order->id;
            $list_orders[$i]['customer_name'] = $order->customer->first_name . ' ' . $order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $order->vendor->first_name . ' ' . $order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $order->created_at;
            $list_orders[$i]['service_name'] = '';
            foreach ($order_details as $order_detail) {
                $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }
        return View::make('pages.vendors.list_completed_orders')->with('orders', $list_orders);
    }

    /*
     * Function Name : viewMaintenanceRequest
     * @param:id
     * @description: This function is begin used for  viewing  all over the  details requests of maintenance in admin
     *
     */

    public function viewMaintenanceRequest($maintenance_request_id = "") {


        $request_maintenance = MaintenanceRequest::find($maintenance_request_id);

        $assign_requests = AssignRequest::where('request_id', '=', $maintenance_request_id)
        ->where('status',"!=",2)
        ->where('vendor_id',"=", Auth::user()->id)
        ->get();

        foreach ($assign_requests as $adata) {
            $assigned_request_ids[] = $adata->requestedService->service->id;

                }
      

        return View::make('pages.vendors.viewvendormaintenancerequest')
        ->with(array(
            'request_maintenance' => $request_maintenance,
            'assign_requests'=>$assign_requests
            ));
    }

    public function declineRequest() {
        $data = Input::all();
        $delete_request = AssignRequest::deleteRequest($data['request_id'], $data['vendor_id']);

        if ($delete_request) {
            return "Request has been declined";
        }
    }

    public function acceptRequest() {
        $input = Input::all();
        $accept_request = AssignRequest::acceptRequest($input['request_id'], $input['vendor_id']);


        if ($accept_request) {
            //Creating the work order
            $data['status'] = 1;
            $data['request_id'] = $input['request_id'];
            $data['vendor_id'] = $input['vendor_id'];
            $data['customer_id'] = MaintenanceRequest::find($input['request_id'])->asset->customer_id;
            $order_id = Order::addOrder($data);

            $order_details = array();
            //Getting  services  ids
            $assigned_requests = AssignRequest::where('request_id', '=', $data['request_id'])
            ->where('vendor_id', '=', $data['vendor_id'])
            ->where('status', '!=', 2)
            ->get();

            foreach ($assigned_requests as $request) {
                $order_details['requested_service_id'] = $request->requested_service_id;
                $order_details['order_id'] = $order_id;
                $order_details['status'] = 1;
                OrderDetail::addOrderDetails($order_details);
            }


            return "Request has been accepted";
        }


    }
  public function acceptSingleRequest() {
        $input = Input::all();
        $accept_request = AssignRequest::acceptSingleRequest($input['request_id'], $input['vendor_id'], $input['service_id']);
     if ($accept_request) {
            //Creating the work order
            $data['status'] = 1;
            $data['request_id'] = $input['request_id'];
            $data['vendor_id'] = $input['vendor_id'];
            $data['customer_id'] = MaintenanceRequest::find($input['request_id'])->asset->customer_id;
            $order_id = Order::addOrder($data);

                $order_details = array();
                $order_details['requested_service_id'] = $input['service_id'];
                $order_details['order_id'] = $order_id;
                $order_details['status'] = 1;
                OrderDetail::addOrderDetails($order_details);
      

               //Declined other required
                AssignRequest::deleteSingleRequest($data['request_id'], $data['vendor_id'],$input['service_id']);

           
            return "Request has been accepted";
        }




    }
        /*
        Add bid for a particular workorder
        */
        public function addBidRequest()
        {

      // Get all customer assets to send in view

        $services = Service::getAllServices(); // get all services provided by admin


            //get vendor id
        $vendor_id = Auth::user()->id;
          // All in progress work orders
        $orders = Order::where('vendor_id', '=', $vendor_id)
        ->where('status', '=', '1')
        ->get();

        $order_ids=array();
        foreach($orders as $order):
         $order_ids[$order->id."--".$order->MaintenanceRequest->Asset->id]=  $order->id."-".$order->MaintenanceRequest->Asset->property_address;
     endforeach;

     return View::make('pages.vendors.add-bid')
     ->with('order_ids', $order_ids)
     ->with('services', $services);
 }

                /**
     * Create Bid request
     * @params none
     * @return Redirect to maintenance request page
     */
                public function createBidServiceRequest() {
        $data = Input::all(); // get all submitted data of user
        //$customer_id = Auth::user()->id;
        //for customer id
        $exploded_orderid_asset_id=explode("--",$data['work_order']);
        $orderData= Order::find($exploded_orderid_asset_id[0]);
        $request['vendor_id'] = Auth::user()->id; // assign current logged id to request
        $request['asset_id'] = $exploded_orderid_asset_id[1]; // assign asset number to request
        $request['order_id'] = $exploded_orderid_asset_id[0];
        $request['customer_id'] = $orderData->customer->id;
        $request['status'] = 1; // while generating request status would be active
        // Add maintainence request to main table
        $add_request = BidRequest::addMaintenanceRequest($request);
        // get last id to assign to each service request
        $request_id = DB::getPdo()->lastInsertId();

        //check if request in created then insert services to service table
        if ($add_request) {
            //Select all selected service to
            $selected_services = $data['service_ids_selected'];

            /// loop through all selected services
            foreach ($selected_services as $service_id) {
             $request_detail['maintenance_request_id'] =$orderData->request_id;

                $request_detail['request_id'] = $request_id; // assign request id to $requested detail
                $request_detail['service_id'] = $service_id; // assign service_id to $requested detail
                $request_detail['status'] = 1;

                if (isset($data['service_required_date_' . $service_id])) {
                    $request_detail['required_date'] = General::displayDate($data['service_required_date_' . $service_id]);
                    $request_detail['required_time'] = General::displayTime($data['time_hours_' . $service_id], $data['time_minutes_' . $service_id], $data['time_meridiem_' . $service_id]);
                } else {
                    unset($request_detail['required_time']);
                    unset($request_detail['required_date']);
                }

                if (isset($data['number_of_men_' . $service_id])) {
                    $request_detail['service_men'] = $data['number_of_men_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['service_note_' . $service_id])) {
                    $request_detail['service_note'] = $data['service_note_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['verified_vacancy_' . $service_id])) {
                    $request_detail['verified_vacancy'] = $data['verified_vacancy_' . $service_id];
                } else {
                    unset($request_detail['verified_vacancy']);
                }

                if (isset($data['cash_for_keys_' . $service_id])) {
                    $request_detail['cash_for_keys'] = $data['cash_for_keys_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys']);
                }

                if (isset($data['cash_for_keys_trash_out_' . $service_id])) {
                    $request_detail['cash_for_keys_trash_out'] = $data['cash_for_keys_trash_out_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys_trash_out']);
                }

                if (isset($data['trash_size_' . $service_id])) {
                    $request_detail['trash_size'] = $data['trash_size_' . $service_id];
                } else {
                    unset($request_detail['trash_size']);
                }

                if (isset($data['storage_shed_' . $service_id])) {
                    $request_detail['storage_shed'] = $data['storage_shed_' . $service_id];
                } else {
                    unset($request_detail['storage_shed']);
                }

                if (isset($data['lot_size_' . $service_id])) {
                    $request_detail['lot_size'] = $data['lot_size_' . $service_id];
                } else {
                    unset($request_detail['lot_size']);
                }
                $add_requested_service = BidRequestedService::addRequestedService($request_detail);
                $request_detail_id = DB::getPdo()->lastInsertId(); // get last id of service
                //check if service is created then insert images of service
                if ($add_requested_service) {
                    if (isset($data['service_image_list_' . $service_id])) {
                        $service_images = $data['service_image_list_' . $service_id];
                        foreach ($service_images as $image) {
                            $image_detail['requested_id'] = $request_detail_id;
                            $image_detail['image_name'] = $image;
                            $image_detail['image_type'] = 'request';
                            $image_detail['status'] = 1;
                            $add_image = BidServiceImage::addServiceImage($image_detail);
                        }
                    }
                }
            }
        }
        $customer_assets = Asset::getAssetsByCustomerId(Auth::user()->id);
        $services = Service::getAllServices();
        if ($add_request) {
            $message = FlashMessage::messages('customer.request_service_add');
            return Redirect::to('vendor-bid-requests')
            ->with('message', FlashMessage::displayAlert($message, 'success'));
        }
    }

    /**
     * List All Assigned requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listBidRequests($status=1) {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('vendor_id', '=', $user_id)->where('status',"=", $status)->orderBy('id', 'desc')->get();


        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)->where('status','=',1)->orderBy('id', 'desc')->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->customer->first_name .' '. $request->customer->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {

                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.vendors.list_bid_requests')
        ->with('assign_requests', $assign_requests)
        ->with('status',$status);
    }

     /**
     * List All Assigned requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
     public function listApprovedBidRequests() {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('vendor_id', '=', $user_id)->where('status',"=", 2)->orderBy('id', 'desc')->get();


        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)->where('status','=',1)->orderBy('id', 'desc')->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->customer->first_name .' '. $request->customer->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {

                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.vendors.list_bid_requests')->with('assign_requests', $assign_requests);
    }


    /**
     * List All Assigned requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listDeclinedBidRequests() {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('vendor_id', '=', $user_id)->where('status',"=", 3)->orderBy('id', 'desc')->get();


        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)->where('status','=',1)->orderBy('id', 'desc')->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->customer->first_name .' '. $request->customer->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {

                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.vendors.list_bid_requests')->with('assign_requests', $assign_requests);
    }


}


