<?php

use JeroenDesloovere\Geolocation\Geolocation;

class AssetController extends \BaseController {

//protected $layout = 'layouts.default';
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    public function showAddAsset() {
        //if user is logged in send to page to add assets
        //Get all cities from city
        $cities = City::getAllCities();
        //Get all states from city
        $states = State::getAllStates();
        return View::make('pages.add_assets')
        ->with('cities', $cities)
        ->with('states', $states);
    }

    public function showAddAssetNew() {

        //Get all cities from city
        $cities = City::getAllCities();
        //Get all states from city
        $states = State::getAllStates();
        return View::make('pages.customer.add_assets_customer')
        ->with('cities', $cities)
        ->with('states', $states);
    }

    public function editAsset($id) {
        if (Auth::check()) {
            $method = Request::method();
            //if form is submitted
            if ($method == 'POST') {
                $asset_update = Input::all();
                if ($asset_update['edit_asset'] == 'yes') {
                    //Set rules for validation of assets
                    $rules = Asset::requiredFields(1); //These rules are defined in Asset Model
                    $validator = Validator::make(Input::all(), $rules); // put all rules to validator
                    // if validation is failed redirect to add customer asset with errors
                    if ($validator->fails()) {
                        return Redirect::to('edit-customer-asset/' . $id)
                        ->withErrors($validator)
                                        ->withInput(Input::except('password')); // send back all errors to the login form
                        ; // send back the input (not the password) so that we can repopulate the form
                    } else {
                        // Get all form data in $data variable
                        $data = Input::all();
                        $message = '';
                        $street = '';
                        $streetNumber = '';
                        $city_id = Input::get('city_id');
                        $city = City::find($city_id)->name;
                        $zip = Input::get('zipcode');

                        $property_address= Input::get('property_address');
                        $state_id = Input::get('state_id');
                        $state = State::find($state_id)->name;
                        $country = 'USA';
                        $result  =  Asset::getLatLong($property_address.$zip,$city,$state);
                        $data['latitude'] = $result['lat'];
                        $data['longitude'] = $result['lng'];
                        $edit = Asset::editAsset($data, $id);

                        if ($edit)
                            Session::flash('message', 'Asset Updated successfully.');
                    }
                }
            }
            $asset_data = Asset::find($id);
            //Get all cities from city
            $cities = City::getAllCities();
            //Get all states from city
            $states = State::getAllStates();
            return View::make('pages.customer.edit_assets')
            ->with('cities', $cities)
            ->with('states', $states)
            ->with('asset_data', $asset_data);
        } else {
            return Redirect::to('/'); // else return to login page
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createAsset() {
        $id = Auth::user()->id;
        $input_data = Input::all();
        $one_column = $input_data['one_column'];


        if ($one_column == 'yes') {
            $page_redirect = 'add-customer-asset';
            $succes_redirect = 'customer';
        } else {
            $message = FlashMessage::messages('customer.add_new_asset_success');
            $page_redirect = 'add-new-customer-asset';
            $succes_redirect = 'add-new-customer-asset';
        }
        //Set rules for validation of assets
       
        if(Input::get('outbuilding_shed')==0 || Input::get('outbuilding_shed')=="")
          {
            //outbuilding_shed notes flag
            $rules =  array(
            'asset_number' => 'required|unique:assets',
            'property_address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required',
            'loan_number' => '',
            'property_type' => 'required',
            'lender' => '',
            'property_status' => 'required',
            'electric_status' => 'required',
            'water_status' => 'required',
            'gas_status' => 'required',
            'lock_box' => (Input::get('occupancy_status'))=="Occupied" ? '': 'required',
            'access_code' => '',
            'brokage' => 'required',
            'agent' => 'required',
            'customer_email_address' => '',
            'outbuilding_shed' => 'required',
            
            'special_direction_note' => '',
            'utility_note' => '',
            'swimming_pool',
            'occupancy_status'=>'required'
                );
          }
          else
          {
             $rules = array(
            'asset_number' =>  'required',
            'property_address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required', 
            'loan_number' => '',
            'property_type' => 'required',
            'lender' => '',
            'property_status' => 'required',
            'electric_status' => 'required',
            'water_status' => 'required',
            'gas_status' => 'required',
            'lock_box' => (Input::get('occupancy_status'))=="Occupied" ? '': 'required',
            'access_code' => '',
            'brokage' => 'required',
            'agent' => 'required',
            'customer_email_address' => '',
            'outbuilding_shed' => 'required',
            'outbuilding_shed_note' => 'required',
            'special_direction_note' => '',
            'utility_note' => '',
            'swimming_pool',
            'occupancy_status'=>'required'
        );
          }



        $validator = Validator::make(Input::all(), $rules); // put all rules to validator
        // if validation is failed redirect to add customer asset with errors
        if ($validator->fails()) {
            return Redirect::to($page_redirect)
                            ->withErrors($validator)// send back all errors to the login form
                            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
                        } else {

                            $message = '';
                            $street = '';
                            $streetNumber = '';
                            $city_id = Input::get('city_id');
                            $city = City::find($city_id)->name;
                            $zip = Input::get('zipcode');
                            $country = 'United States';
                            $property_address= Input::get('property_address');
                            $state_id = Input::get('state_id');
                            $state = State::find($state_id)->name;
                            $country = 'USA';
                            $result  =  Asset::getLatLong($property_address.$zip,$city,$state);

           // Get all form data in $data variable
                            $data = Input::all();
                            $data['latitude'] = $result['lat'];
                            $data['longitude'] = $result['lng'];
            //set data in addAsset function
                            $save = Asset::addAsset($data, $id);
                        }
        // if data inserted successfully
                        if ($save) {
                            $message="Property has been added!";
                            Session::flash('message', $message);
            return Redirect::to('view-assets-list'); // return to customer dashboard
        } else {
            return Redirect::to($page_redirect); // return back to add customer asset page
        }

        //
    }

    public static function deleteAsset($id) {
        $delete = Asset::deleteAsset($id);
        return Redirect::to('view-assets-list');
    }

    public static function viewAssetsList() {
        //Call viewAssets function
        $assets_data = Asset::getAssetsByCustomerId(Auth::user()->id);
        return View::make('pages.view_assets')->with('assets_data', $assets_data);  // set assets_data to view to show list of assets
    }

    public function addAdminAsset() {

        $customer = User::getAllCustomers();
        $cities = City::getAllCities();
        //Get all states from city
        $states = State::getAllStates();
        $submitted = Input::get('submitted');
        if ($submitted) {

             if(Input::get('outbuilding_shed')==0 || Input::get('outbuilding_shed')=="")
          {

            //outbuilding_shed notes flag
            $rules =  array(
            'asset_number' => 'required|unique:assets',
            'property_address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required',
            'loan_number' => '',
            'property_type' => 'required',
            'lender' => '',
            'property_status' => 'required',
            'electric_status' => 'required',
            'water_status' => 'required',
            'gas_status' => 'required',
            'lock_box' => (Input::get('occupancy_status'))=="Occupied" ? '': 'required',
            'access_code' => '',
            'brokage' => 'required',
            'agent' => 'required',
            'customer_email_address' => '',
            'outbuilding_shed' => 'required',
            
            'special_direction_note' => '',
            'utility_note' => '',
            'swimming_pool',
            'occupancy_status'=>'required'
                );
          }
          else
          {

   $rules = array(
            'asset_number' =>  'required',
            'property_address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required',
            'loan_number' => '',
            'property_type' => 'required',
            'lender' => '',
            'property_status' => 'required',
            'electric_status' => 'required',
            'water_status' => 'required',
            'gas_status' => 'required',
            'lock_box' => (Input::get('occupancy_status'))=="Occupied" ? '': 'required',
            'access_code' => '',
            'brokage' => 'required',
            'agent' => 'required',
            'customer_email_address' => '',
            'outbuilding_shed' => 'required',
            'outbuilding_shed_note' => 'required',
            'special_direction_note' => '',
            'utility_note' => '',
            'swimming_pool',
            'occupancy_status'=>'required'
        );



           
          }
            $rules['customer_id'] = 'required';
            $validator = Validator::make(Input::all(), $rules);
            //print_r($validator); exit;
            if ($validator->fails()) {
                return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));
            } else {
                $data = Input::all();
                $customer_id = $data['customer_id'];
                $message = '';

                $city_id = Input::get('city_id');
                $city = City::find($city_id)->name;
                $zip = Input::get('zipcode');
                $property_address= Input::get('property_address');
                $state_id = Input::get('state_id');
                $state = State::find($state_id)->name;
                $country = 'USA';
                $result  =  Asset::getLatLong($property_address.$zip,$city,$state);
                $data['latitude'] = $result['lat'];
                $data['longitude'] = $result['lng'];
                $save = Asset::addAsset($data, $customer_id);
            }

            if ($save) {
                return Redirect::to('list-assets')
                ->with('message', FlashMessage::displayAlert('Your property has been added Successfully', 'success'));
            } else {


                return View::make('pages.admin.add_assets')
                ->with('cities', $cities)
                ->with('states', $states)
                ;
            }
        } else {
            return View::make('pages.admin.add_assets')
            ->with('cities', $cities)
            ->with('states', $states)
            ;
        }

        /* if ($save) {
          return Redirect::to('list-asset');
          } else {
          return View::make('pages.admin.add_assets');
      } */
  }

  public function listAdminAssets() {
    $assets = Asset::orderBy('id', 'desc')->get();
    return View::make('pages.admin.list_assets')
    ->with(array(
        'assets_data' => $assets));
}

public function editAdminAsset($asset_id) {
    $submitted = Input::get('submitted');
    if ($submitted) {
        $data = Input::all();


        $message = '';
        
        $city_id = Input::get('city_id');
        $city = City::find($city_id)->name;
        $zip = Input::get('zipcode');
        $property_address= Input::get('property_address');
        $state_id = Input::get('state_id');
        $state = State::find($state_id)->name;
        $country = 'USA';
        $result  =  Asset::getLatLong($property_address.$zip,$city,$state);
        $data['latitude'] = $result['lat'];
        $data['longitude'] = $result['lng'];
        
        $save = Asset::editAsset($data, $asset_id);

        if ($save) {
            $message = FlashMessage::messages('admin_asset.asset_updated');
            return Redirect::to('list-assets')
            ->with('message', FlashMessage::displayAlert($message, 'success'));
        } else {
            return View::make('pages.admin.add_assets');
        }
    } else {
        $asset = Asset::find($asset_id);
            //Get all cities from city
        $cities = City::getAllCities();
            //Get all states from city
        $states = State::getAllStates();
            //Get all customers
        $customers = User::getAllCustomers();
        return View::make('pages.admin.edit_assets')
        ->with(array(
            'asset_data' => $asset,
            'states' => $states,
            'cities' => $cities,
            'customers' => $customers));
    }
}

    //Get Asset map 
public  function getAssetMap()
{ 

  $zip = Input::get('zip');
  $state = Input::get('state');
  $city = Input::get('city');
  $property_address= Input::get('property_address');
  if($state=="Select State")
  {
     $state="";
 }
 if($city=="Select City")
 {
   $city="";
}
$country = 'USA';
         // $result = Geolocation::getCoordinates($property_address, ' ', $city, $zip, $country);
$myresult =     Asset::getLatLong($property_address. $zip,$city,$state);


/*
->with('zipcode',$myresult['overall']->results[0]->address_components[0]->long_name) 
            ->with('City',$myresult['overall']->results[0]->address_components[1]->long_name) 
            */
            return  View::make('pages.admin.add-asset-map')
            ->with('latitude',$myresult['lat'])
            ->with('longitude',$myresult['lng'])
            ->with('zipcode',isset($myresult['overall']->results[0]->address_components[0]->long_name)?$myresult['overall']->results[0]->address_components[0]->long_name:"")
            ->with('city',isset($myresult['overall']->results[0]->address_components[1]->long_name)?$myresult['overall']->results[0]->address_components[1]->long_name:"") 
            ->with('state',isset($myresult['overall']->results[0]->address_components[3]->long_name)?$myresult['overall']->results[0]->address_components[3]->long_name:"")  ;

        }

    }
