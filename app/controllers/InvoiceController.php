<?php

class InvoiceController extends BaseController {


	public function listAdminInvoices($userTypeId=2)
	{
		
		
	 $invoices = Invoice::listAll($userTypeId);


       $list_orders = array();
        $i = 0;


        foreach ($invoices as $invoice) {
            $order_details = ($invoice->order->orderDetail);
 
            $list_orders[$i]['order_id'] = $invoice->order_id;
            $list_orders[$i]['customer_name'] = $invoice->order->customer->first_name . ' ' . $invoice->order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $invoice->order->vendor->first_name . ' ' . $invoice->order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $invoice->order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $invoice->order->created_at;
            $list_orders[$i]['service_name'] = '';
            $list_orders[$i]['status'] = $invoice->status;
            $list_orders[$i]['price'] = $invoice->total_amount;
            foreach ($order_details as $order_detail) {
               $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }


        return View::make('pages.admin.list_invoices')
                        ->with('orders', $list_orders)
                        ->with('status', $userTypeId);
    }

    public function listCustomerInvoices()
	{

	$userTypeId=2;
		
	 $invoices = Invoice::listAll($userTypeId,Auth::user()->id);


       $list_orders = array();
        $i = 0;


        foreach ($invoices as $invoice) {
            $order_details = ($invoice->order->orderDetail);
 
            $list_orders[$i]['order_id'] = $invoice->order_id;
            $list_orders[$i]['customer_name'] = $invoice->order->customer->first_name . ' ' . $invoice->order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $invoice->order->vendor->first_name . ' ' . $invoice->order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $invoice->order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $invoice->order->created_at;
            $list_orders[$i]['service_name'] = '';
            $list_orders[$i]['status'] = $invoice->status;
            $list_orders[$i]['price'] = $invoice->total_amount;
            foreach ($order_details as $order_detail) {
               $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }


        return View::make('pages.customer.list_invoices')
                        ->with('orders', $list_orders)
                        ->with('status', $userTypeId);
    }

    public function listVendorInvoices()
	{

	$userTypeId=3;
		
	 $invoices = Invoice::listAll($userTypeId,Auth::user()->id);


       $list_orders = array();
        $i = 0;


        foreach ($invoices as $invoice) {
            $order_details = ($invoice->order->orderDetail);
 
            $list_orders[$i]['order_id'] = $invoice->order_id;
            $list_orders[$i]['customer_name'] = $invoice->order->customer->first_name . ' ' . $invoice->order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $invoice->order->vendor->first_name . ' ' . $invoice->order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $invoice->order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $invoice->order->created_at;
            $list_orders[$i]['service_name'] = '';
            $list_orders[$i]['status'] = $invoice->status;
            $list_orders[$i]['price'] = $invoice->total_amount;
            foreach ($order_details as $order_detail) {
               $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }


        return View::make('pages.vendors.list_invoices')
                        ->with('orders', $list_orders)
                        ->with('status', $userTypeId);
    }

	

}
