<?php
/**
 * AccessLevel Controller Class.
 *
 * Will be handling all the Access level functionlity here.
 * @copyright Copyright 2014 Invortex Technology Development Team
 * @version $Id: 1.0
 */

class CronJobController extends \BaseController {

	public function index()
	{
	/*
					Can we set a different time limit for vendor acceptance on emergency orders? We are thinking within 2 hours.
					*/
					$emergencyData=  MaintenanceRequest::where('emergency_request','=',1)->get();
					
					$requestIDARray=array();

					foreach ($emergencyData as  $value) {
					 
					 foreach ($value->assignRequest as  $assignRequestss) {
					
						 if($assignRequestss->status==1)
						 {
						
							 $date = new DateTime($assignRequestss->created_at);
							 $date->modify("+2 hours");
							 if ($date->format("Y-m-d H:i:s") <= date("Y-m-d H:i:s") )
							 {
								
								$requestIDARray[$assignRequestss->request_id]= $assignRequestss->vendor_id;   
								
							 }


						}
					}

				}


				foreach ($requestIDARray as $requestID=>$VendorID) {
					AssignRequest::deleteRequest($requestID,$VendorID);
				}
				




				/*
					5.      Add rule: If work order not accepted within 48 hrs, work order goes back to gss user to schedule	*/
					$emergencyData=  MaintenanceRequest::where('emergency_request','!=',1)->get();
					
					$requestIDARray=array();

					foreach ($emergencyData as  $value) {
					 
					 foreach ($value->assignRequest as  $assignRequestss) {
					
						 if($assignRequestss->status==1)
						 {
						
							 $date = new DateTime($assignRequestss->created_at);
							 $date->modify("+48 hours");
							 if ($date->format("Y-m-d H:i:s") <= date("Y-m-d H:i:s") )
							 {
								
								$requestIDARray[$assignRequestss->request_id]= $assignRequestss->vendor_id;   
								
							 }


						}
					}

				}


				foreach ($requestIDARray as $requestID=>$VendorID) {
					AssignRequest::deleteRequest($requestID,$VendorID);
				}
				



				

				$recurring= Recurring::where('start_date','<=',date('Y-m-d'))
				->get();

				foreach ($recurring as $value ) {

				 $date=date_create($value->start_date );
				 date_add($date,date_interval_create_from_date_string("$value->duration days"));
				 $nextDate= date_format($date,"Y-m-d");

				//if date meet with current date it will execute the the cron job and create a service request 
				 if($nextDate==date('Y-m-d'))
				 {
					$request_service_id=$value ->request_service_id;
					$RequestedService  =RequestedService::find($request_service_id);

					$MaintenanceRequestData=array(
					 'customer_id'=>$RequestedService->maintenanceRequest->customer_id,
					 'asset_id'=>$RequestedService->maintenanceRequest->asset_id,
					 'emergency_request'=>$RequestedService->maintenanceRequest->emergency_request,
					 'status'=>1
					 );
							//inserting the new request 
					MaintenanceRequest::create($MaintenanceRequestData);
					$lastMaintenanceRequestId=DB::getPdo()->lastInsertId();
									//End Inserting Maintanance Request

									///Inserting New Requested Service

					$request_detail=array();
						$request_detail['request_id'] = $lastMaintenanceRequestId; // assign request id to $requested detail
								$request_detail['service_id'] = $RequestedService->service_id; // assign service_id to $requested detail
								$request_detail['status'] = 1;
								$request_detail['required_date'] = $RequestedService->required_date;
								$request_detail['required_time'] = $RequestedService->required_time;
								$request_detail['service_men'] = $RequestedService->service_men;
								$request_detail['service_note'] = $RequestedService->service_note;
								$request_detail['verified_vacancy'] = $RequestedService->verified_vacancy;
								$request_detail['cash_for_keys'] = $RequestedService->cash_for_keys;
								$request_detail['cash_for_keys_trash_out'] = $RequestedService->cash_for_keys_trash_out;
								$request_detail['trash_size'] = $RequestedService->trash_size;
								$request_detail['storage_shed'] = $RequestedService->storage_shed;
								$request_detail['lot_size'] = $RequestedService->lot_size;
								$request_detail['due_date'] = $RequestedService->due_date;

								$request_detail['set_prinkler_system_type'] = $RequestedService->set_prinkler_system_type;

								$request_detail['install_temporary_system_type'] = $RequestedService->install_temporary_system_type;
								$request_detail['carpet_service_type'] = $RequestedService->carpet_service_type;
								$request_detail['pool_service_type'] = $RequestedService->pool_service_type;
								$request_detail['boarding_type'] = $RequestedService->boarding_type;

								$request_detail['spruce_up_type'] = $RequestedService->spruce_up_type;




								$add_requested_service = RequestedService::addRequestedService($request_detail);
								$request_detail_id = DB::getPdo()->lastInsertId(); // get last id of service

								foreach ($RequestedService->serviceImages as $serviceImages) {
									$image_detail['requested_id'] = $request_detail_id;
									$image_detail['image_name'] = $serviceImages->image;
									$image_detail['image_type'] = 'request';
									$image_detail['status'] = 1;
									$add_image = ServiceImage::addServiceImage($image_detail);
								}

								Recurring::find($value->id)
								->update(array('start_date'=>$nextDate));
							}

						}





					}

				}



				?>
