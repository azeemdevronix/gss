<?php

class MaintenanceRequestController extends \BaseController {

    /**
     * Redirect to customer maintenace request page
     * @params none
     * @return Redirect to customer maintenace request page
     */
    public function viewRequestForm() {
        // Get all customer assets to send in view
        $customer_assets = Asset::getAssetsByCustomerId(Auth::user()->id);
        $services = Service::getAllServices(); // get all services provided by admin

        return View::make('pages.customer.request_service')// return to page
                        ->with('customer_assets', $customer_assets)
                        ->with('services', $services);
    }

     public function viewAdminRequestForm() {
        // Get all  assets to send in view
        $asset_information = Asset::all();
          
       
        $services = Service::getAllServices(); // get all services provided by admin

        return View::make('pages.customer.request_service')// return to page
                        ->with('customer_assets',  $asset_information)
                        ->with('services', $services);
    }

    /**
     * show maintenace request listing
     * @params none
     * @return Redirect maintenace request listing
     */
    public function listServiceRequest() {

        // Get all maintenance request of current customer logged in
        $requests = MaintenanceRequest::listMaintenanceRequestByCustomerId(Auth::user()->id);
        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {

            $assign_requests[$i]['request_id'] = $request->id; // get reqested id from current table
            // assign first name and last name by relation which is create in model
            $assign_requests[$i]['customer_name'] = $request->user->first_name . ' ' . $request->user->last_name;
            //assign assent number for asset table
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['property_address'] = $request->asset->property_address;
            $assign_requests[$i]['city'] = $request->asset->city->name;
            $assign_requests[$i]['state'] = $request->asset->state->name;
            $assign_requests[$i]['status'] = $request->status;
            $assign_requests[$i]['emergency_request']=$request->emergency_request;
            $assign_requests[$i]['serviceType'] =  '';
           
           
         
            foreach ($request->requestedService as  $value) {
            if(isset( $value->service->title))
             $assign_requests[$i]['serviceType'] .=  $value->service->title  ;

            if(isset($value->due_date))
            { 
                         $assign_requests[$i]['serviceType'] .= "<br>".    $value->due_date . ', <br>';
            }
            else
            {
        $assign_requests[$i]['serviceType'] .=   ', <br>';
         

            }

            }
             $i++;

        }

        return View::make('pages.customer.list_customer_requested_services')
                        ->with('assign_requests', $assign_requests);
    }

    /**
     * show maintenace request listing
     * @params none
     * @return Redirect maintenace request listing
     */
    public function viewServiceRequest($id) {


        // Get all maintenance request of current customer logged in
        $request_detail = MaintenanceRequest::viewDetailByRequestId($id);

        return View::make('pages.customer.view_customer_requested_service')
                        ->with('request_detail', $request_detail);
    }

    /**
     * Create maintenace request
     * @params none
     * @return Redirect to maintenance request page
     */
    public function createServiceRequest() {
        $data = Input::all(); // get all submitted data of user
        $customer_id_for_request = "";
        if(Auth::user()->id==1)
        {
             $customer_id_for_request=Asset::find($data['asset_number'])->customer_id;
        }
        else
        {
             $customer_id_for_request = Auth::user()->id;
        }

        $request['customer_id'] = Auth::user()->id; // assign current logged id to request
        $request['asset_id'] = $data['asset_number']; // assign asset number to request
        $request['status'] = 1; // while generating request status would be active
        $request['emergency_request'] =$data['emergency_request'];
        $requested_selected_services=array(); // array for getting the ids of requested servcies it will be used for auto assigning the emergency request
        
        // Add maintainence request to main table
        $add_request = MaintenanceRequest::addMaintenanceRequest($request);
       
        // get last id to assign to each service request
        $request_id = DB::getPdo()->lastInsertId();


        //check if request in created then insert services to service table
        if ($add_request) {
            //Select all selected service to
            $selected_services = $data['service_ids_selected'];

            /// loop through all selected services
            foreach ($selected_services as $service_id) {
                $request_detail['request_id'] = $request_id; // assign request id to $requested detail
                $request_detail['service_id'] = $service_id; // assign service_id to $requested detail
                $request_detail['status'] = 1;

                if (isset($data['service_required_date_' . $service_id])) {
                    $request_detail['required_date'] = General::displayDate($data['service_required_date_' . $service_id]);
                    $request_detail['required_time'] = General::displayTime($data['time_hours_' . $service_id], $data['time_minutes_' . $service_id], $data['time_meridiem_' . $service_id]);
                } else {
                    unset($request_detail['required_time']);
                    unset($request_detail['required_date']);
                }

                if (isset($data['number_of_men_' . $service_id])) {
                    $request_detail['service_men'] = $data['number_of_men_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['service_note_' . $service_id])) {
                    $request_detail['service_note'] = $data['service_note_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['verified_vacancy_' . $service_id])) {
                    $request_detail['verified_vacancy'] = $data['verified_vacancy_' . $service_id];
                } else {
                    unset($request_detail['verified_vacancy']);
                }

                if (isset($data['cash_for_keys_' . $service_id])) {
                    $request_detail['cash_for_keys'] = $data['cash_for_keys_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys']);
                }

                if (isset($data['cash_for_keys_trash_out_' . $service_id])) {
                    $request_detail['cash_for_keys_trash_out'] = $data['cash_for_keys_trash_out_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys_trash_out']);
                }

                if (isset($data['trash_size_' . $service_id])) {
                    $request_detail['trash_size'] = $data['trash_size_' . $service_id];
                } else {
                    unset($request_detail['trash_size']);
                }

                if (isset($data['storage_shed_' . $service_id])) {
                    $request_detail['storage_shed'] = $data['storage_shed_' . $service_id];
                } else {
                    unset($request_detail['storage_shed']);
                }

                if (isset($data['due_date_' . $service_id])) {
                    $request_detail['due_date'] = $data['due_date_' . $service_id];
                } else {
                    unset($request_detail['due_date']);
                }


                if (isset($data['set_prinkler_system_type_' . $service_id])) {
                    $request_detail['set_prinkler_system_type'] = $data['set_prinkler_system_type_' . $service_id];
                } else {
                    unset($request_detail['set_prinkler_system_type']);
                }

                           if (isset($data['install_temporary_system_type_' . $service_id])) {
                    $request_detail['install_temporary_system_type'] = $data['install_temporary_system_type_' . $service_id];
                } else {
                    unset($request_detail['install_temporary_system_type']);
                }


           if (isset($data['carpet_service_type_' . $service_id])) {
                    $request_detail['carpet_service_type'] = $data['carpet_service_type_' . $service_id];
                } else {
                    unset($request_detail['carpet_service_type']);
                }


           if (isset($data['pool_service_type_' . $service_id])) {
                    $request_detail['pool_service_type'] = $data['pool_service_type_' . $service_id];
                } else {
                    unset($request_detail['pool_service_type']);
                }

                           if (isset($data['boarding_type_' . $service_id])) {
                    $request_detail['boarding_type'] = $data['boarding_type_' . $service_id];
                } else {
                    unset($request_detail['boarding_type']);
                }

           if (isset($data['spruce_up_type_' . $service_id])) {
                    $request_detail['spruce_up_type'] = $data['spruce_up_type_' . $service_id];
                } else {
                    unset($request_detail['spruce_up_type']);
                }

                if (isset($data['constable_information_type_' . $service_id])) {
                    $request_detail['constable_information_type'] = $data['constable_information_type_' . $service_id];
                } else {
                    unset($request_detail['constable_information_type']);
                }

                if (isset($data['remove_carpe_type_' . $service_id])) {
                    $request_detail['remove_carpe_type'] = $data['remove_carpe_type_' . $service_id];
                } else {
                    unset($request_detail['remove_carpe_type']);
                }

                if (isset($data['remove_blinds_type_' . $service_id])) {
                    $request_detail['remove_blinds_type'] = $data['remove_blinds_type_' . $service_id];
                } else {
                    unset($request_detail['remove_blinds_type']);
                }

                if (isset($data['remove_appliances_type_' . $service_id])) {
                    $request_detail['remove_appliances_type'] = $data['remove_appliances_type_' . $service_id];
                } else {
                    unset($request_detail['remove_appliances_type']);
                }






                if (isset($data['lot_size_' . $service_id])) {
                    $request_detail['lot_size'] = $data['lot_size_' . $service_id];
                } else {
                    unset($request_detail['lot_size']);
                }

                //Recurring request information
                $recurringData=array();
                if (isset($data['recurring_' . $service_id])) {
                    $request_detail['recurring'] = $data['recurring_' . $service_id];

                  if (isset($data['end_date_' . $service_id])) {
                    $request_detail['recurring_end_date'] = $data['end_date_' . $service_id];
                    $recurringData['end_date']=date('Y-m-d',strtotime($data['end_date_' . $service_id]));
                    } else {
                    unset($request_detail['recurring_end_date']);
                }

                 if (isset($data['duration_' . $service_id])) {
                    $request_detail['duration'] = $data['duration_' . $service_id];
                    $recurringData['duration']=$data['duration_' . $service_id];
                } else {
                    unset($request_detail['duration']);
                }

                 if (isset($data['start_date_' . $service_id])) {
                    $request_detail['recurring_start_date'] = $data['start_date_' . $service_id];
                    $recurringData['start_date']=date('Y-m-d',strtotime($data['start_date_' . $service_id]));
                } else {
                    unset($request_detail['recurring_start_date']);
                }





                } else {

                    unset($request_detail['recurring']);
                    unset($request_detail['recurring_start_date']);
                    unset($request_detail['recurring_end_date']);
                    unset($request_detail['duration']);
                }

                //End recurring Request

                $add_requested_service = RequestedService::addRequestedService($request_detail);
              
                $request_detail_id = DB::getPdo()->lastInsertId(); // get last id of service
                
                //Inserting recurring data into recurring table
                if (isset($data['recurring_' . $service_id])) {
                     $recurringData['request_service_id']=     $request_detail_id;
                     $recurringData['status']=1;
                     $recurringData['assignment_type']='single';
                     Recurring::create($recurringData);
                    }
                //End inserting
                $requested_selected_services[]=$request_detail_id;
                //check if service is created then insert images of service
                if ($add_requested_service) {
                    if (isset($data['service_image_list_' . $service_id])) {
                        $service_images = $data['service_image_list_' . $service_id];
                        foreach ($service_images as $image) {
                            $image_detail['requested_id'] = $request_detail_id;
                            $image_detail['image_name'] = $image;
                            $image_detail['image_type'] = 'request';
                            $image_detail['status'] = 1;
                            $add_image = ServiceImage::addServiceImage($image_detail);
                        }
                    }
                }
            }
        }
        


        $emergency_request_message="";
 if($data['emergency_request']==1)
      {
     $assetData=Asset::where('id','=',$data['asset_number'])->get();


        $dataEmergencyRequest=array(
                        'customer_id'=>$request['customer_id'],
                        'request_id'=> $request_id,
                        'status'    => '1' //Emergency request triggers
                      );
    EmergencyRequest::create($dataEmergencyRequest);
    $emergency_request_id = DB::getPdo()->lastInsertId();
    $nearest_users   = User::getNearestUsers(3,$assetData[0]->latitude,$assetData[0]->longitude,50);
      
      if(isset($nearest_users[0]))
      {
      
      foreach($nearest_users  as $userdata)
      {
        $dataEmergencyRequestDetail['request_id']          =$request_id;
        $dataEmergencyRequestDetail['vendor_id']           =$userdata->id;
        $dataEmergencyRequestDetail['distance']            =$userdata->distance;
        $dataEmergencyRequestDetail['emergency_request_id']=$emergency_request_id;
        EmergencyRequestDetail::create($dataEmergencyRequestDetail);

      }

      /*
      Commenting below code for making sure emergency request not being auto assigned assigned 
      */

 // foreach ( $requested_selected_services as $service_id) {
    //         $service_data['request_id'] = $request_id;
    //         $service_data['requested_service_id'] = $service_id;
    //         $service_data['vendor_id'] = $nearest_users[0]->id;
    //         $service_data['status'] = 1;

    //         //1.    DON’T auto assign request. The request still needs to come to GSS for us to review/edit before assigning to vendor
    //        // AssignRequest::addRequest($service_data);

            
    //     }
       // $data=User::where('id','=', $service_data['vendor_id'])->select('first_name','last_name')->get();

        //Updating the status
       /* EmergencyRequestDetail::where('vendor_id','=',$nearest_users[0]->id)
                               ->where('emergency_request_id','=',$emergency_request_id)
                               ->update(array('status'=>1)); //Auto Assigned request status, nearest vendor has been assigned
        $emergency_request_message=" Auto assigned to ". $data[0]->first_name." ".$data[0]->last_name;
      */

/*
End Commenting

*/


       }
       else
        { 
           // $emergency_request_message=" Your Property address is far away from system vendors, admin will assign it manually. ";
            
        }
      }



        $customer_assets = Asset::getAssetsByCustomerId(Auth::user()->id);
        $services = Service::getAllServices();
        if ($add_request) {
            $message = FlashMessage::messages('customer.request_service_add');

              if(Auth::user()->id==1)
                        {
                              return Redirect::to('list-maintenance-request')
                            ->with('message', FlashMessage::displayAlert($message.  $emergency_request_message, 'success'));
                        
                        }else{



            return Redirect::to('list-customer-requested-services')
                            ->with('message', FlashMessage::displayAlert($message.  $emergency_request_message, 'success'));
       }

        }
    }

    public function assignServiceRequest() {
        $assignment_data = Input::all();

        foreach ($assignment_data['services'] as $service_id) {
            $service_data['request_id'] = $assignment_data['request_id'];
            $service_data['requested_service_id'] = $service_id;
            $service_data['vendor_id'] = $assignment_data['vendor'];
            $service_data['status'] = 1;

            AssignRequest::addRequest($service_data);
        }
        $message = FlashMessage::messages('customer.request_service_add');
        return Redirect::back()
                        ->with('message', FlashMessage::displayAlert($message, 'success'));
    }

}
