<?php

class OrderController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function editOrder($order_id) {
        //Show dashboard of customer
        $submitted = Input::get('submitted');
        if ($submitted) {

        } else {
            $order = Order::getOrderByID($order_id);
            $order_details = $order->orderDetail;
            
            $before_image_flag=OrderImage::where('order_id','=',$order_id)->where('type','=','before')->first();
            
            if($before_image_flag)
            {
                $before_image=1;
            }
            else
            {
                $before_image=0;
            }
            return View::make('common.edit_order')->with('order', $order)->with('order_details', $order_details)->with('before_image',$before_image);
        }
    }
    
    public function viewOrder($order_id) {
        //Show dashboard of customer
        $order = Order::getOrderByID($order_id);
        $order_details = $order->orderDetail;

        $before_image_flag=OrderImage::where('order_id','=',$order_id)->first();
        return View::make('common.view_order')->with('order', $order)->with('order_details', $order_details);
    }
    public function addBeforeImages() {

        $destinationPath = Config::get('app.order_images_before');   //2
        if (!empty($_FILES)) {
            $data=Input::all();
            $order_id=$data['order_id'];
            $order_details_id=$data['order_details_id'];
            $type=$data['type'];
            $tempFile = $_FILES['file']['tmp_name'];          //3             
            $targetPath = $destinationPath;  //4
            $originalFile=$_FILES['file']['name'];
            $changedFileName=$order_id.'-'.$order_details_id.'-'.$originalFile;
            $targetFile = $targetPath . $changedFileName;  //5
            $moved=move_uploaded_file($tempFile, $targetFile); //6
            if($moved)
            {
                $data['address']=$changedFileName;
                unset($data['file']);
                unset($data['_token']);
                
                setcookie('order_id', $order_id);
                setcookie('order_details_id', $order_details_id);
                setcookie('type', $type);   
                $save=OrderImage::createImage($data);
                if($save)
                {
                    $image='<img id="'.$save->order_id.'-'.$save->order_details_id.'-'.$save->address.'" src="'.Config::get('app.url').'/'.Config::get('app.order_images_before').$save->address.'" width="80px" height="80px" style="padding: 10px" class="img-thumbnail" alt="">';
                    echo $image;
                }
            }
        }
    }
    
    
    public function addAfterImages() {

        $destinationPath = Config::get('app.order_images_after');   //2
        if (!empty($_FILES)) {
            $data=Input::all();
            $order_id=$data['order_id'];
            $order_details_id=$data['order_details_id'];
            $type=$data['type'];
            $tempFile = $_FILES['file']['tmp_name'];          //3             
            $targetPath = $destinationPath;  //4
            $originalFile=$_FILES['file']['name'];
            $changedFileName=$order_id.'-'.$order_details_id.'-'.$originalFile;
            $targetFile = $targetPath . $changedFileName;  //5
            $moved=move_uploaded_file($tempFile, $targetFile); //6
            if($moved)
            {
                $data['address']=$changedFileName;
                unset($data['file']);
                unset($data['_token']);
                
                setcookie('order_id', $order_id);
                setcookie('order_details_id', $order_details_id);
                setcookie('type', $type);   
                $save=OrderImage::createImage($data);
                if($save)
                {
                    echo 'success';
                }
            }
        }
    }
    
    public function deleteOrderAllBeforeImages() {
        $data=Input::all();
        $order_id=$data['order_id'];
        if($data['before_image']==0)
        {
            $images=OrderImage::where('order_id','=',$order_id)->get();
            foreach ($images as $image)
            {
                $filename=$image->address;
                if($image->type=='before')
                {
                    $destinationPath = Config::get('app.order_images_before');   //2
                    unlink($destinationPath . $filename);
                }
                else
                {
                    $destinationPath = Config::get('app.order_images_after');   //2
                    unlink($destinationPath . $filename);
                }
            }
            $delete=OrderImage::where('order_id','=',$order_id)->delete();
        }
        else
        {
            $images=OrderImage::where('order_id','=',$order_id)->where('type','=','after')->get();
            foreach ($images as $image)
            {
                $filename=$image->address;
                if($image->type=='before')
                {
                    $destinationPath = Config::get('app.order_images_before');   //2
                    unlink($destinationPath . $filename);
                }
                else
                {
                    $destinationPath = Config::get('app.order_images_after');   //2
                    unlink($destinationPath . $filename);
                }
            }
            $delete=OrderImage::where('order_id','=',$order_id)->where('type','=','after')->delete();
        }
    }
    
    public function deleteAfterImages() {
        $data = Input::all();
        $order_id = $data['order_id'];
        $order_details_id = $data['order_details_id'];
        $type = $data['type'];
        $filename = $order_id . '-' . $order_details_id . '-' . $data['filename'];
        $delete = OrderImage::where('order_id', '=', $order_id)->where('order_details_id', '=', $order_details_id)->where('type', '=', $type)->where('address', '=', $filename)->delete();
        if ($delete) {
            echo 'delete success';
        }
        $destinationPath = Config::get('app.order_images_after');   //2
        unlink($destinationPath . $filename);
    }
    public function deleteBeforeImages() {
        $data = Input::all();
        $order_id = $data['order_id'];
        $order_details_id = $data['order_details_id'];
        $type = $data['type'];
        $filename = $order_id . '-' . $order_details_id . '-' . $data['filename'];
        $delete = OrderImage::where('order_id', '=', $order_id)->where('order_details_id', '=', $order_details_id)->where('type', '=', $type)->where('address', '=', $filename)->delete();
        if ($delete) {
            echo 'delete success';
        }
        $destinationPath = Config::get('app.order_images_before');   //2
        unlink($destinationPath . $filename);
    }

    public function saveVendorNote() {
        $data = Input::all();
        $vendor_note=array('vendor_note'=>$data['vendor_note']);
        $order_details=OrderDetail::find($data['order_detail_id']);
        $requested_service=RequestedService::find($order_details->requestedService->id);
        $save=$requested_service->vendor_note=$data['vendor_note'];
        $requested_service->save();
        if($save)
        {
            return $requested_service->vendor_note;
        }
    }
    
    public function displayImages() {
        $data = Input::all();
        $order_id = $data['order_id'];
        $order_details_id = $data['order_detail_id'];
        $type = $data['type'];
        $popDiv='';
        $images=OrderImage::where('order_id','=',$order_id)->where('order_details_id','=',$order_details_id)->where('type','=',$type)->get();
        foreach($images as $image)
        {
            $popDiv.='<div class="imageFrame"><img src="'.Config::get('app.url').'/'.Config::get('app.order_images_before').$image->address.'" width="120px" height="120px" class="img-thumbnail" alt="'.$image->address.'"><a href="#" class="deletImg" data-value="'.$image->address.'" onclick="removeImage('.$order_id.','.$order_details_id.',this,\''.$type.'\');" >X</a></div>';
        }
        return $popDiv;
    }
    
    public function displayViewImages() {
        $data = Input::all();
        $order_id = $data['order_id'];
        $order_details_id = $data['order_detail_id'];
        $type = $data['type'];
        $popDiv='';
        $images=OrderImage::where('order_id','=',$order_id)->where('order_details_id','=',$order_details_id)->where('type','=',$type)->get();
        foreach($images as $image)
        {
            $popDiv.='<div class="imageFrame"><img src="'.Config::get('app.url').'/'.Config::get('app.order_images_before').$image->address.'" width="120px" height="120px" class="img-thumbnail" alt="'.$image->address.'"></div>';
        }
        return $popDiv;
    }
    
    public function deleteImageById() {
        $data = Input::all();
        $order_id = $data['order_id'];
        $order_details_id = $data['order_detail_id'];
        $filename = $data['filename'];
        $type=$data['type'];
        $delete=OrderImage::where('order_id','=',$order_id)->where('order_details_id','=',$order_details_id)->where('address','=',$filename)->where('type','=',$type)->delete();
        if($delete)
        {
            if($type=='before')
            {
                $destinationPath = Config::get('app.order_images_before');   //2
                unlink($destinationPath . $filename);
            }
            else
            {
                $destinationPath = Config::get('app.order_images_after');   //2
                unlink($destinationPath . $filename);
            }
            
        }
        return $delete;
    }
    public function changeStatus()
    {
        $order_id= Input::get('order_id');

        $orderdata = array(
            'status'       =>   Input::get('orderstatusid') ,
            'status_class' =>   Input::get('orderstatus_class') ,
            'status_text'  =>   Input::get('orderstatus_text')  
            );

        $save = Order::where('id','=',$order_id)
        ->update($orderdata);

     //send system notifications to customer
     $orders = Order::where('id','=',$order_id)->get();

     //If order status is going to be approved then create invoice
     if(Input::get('orderstatusid')==4)
     {

         $order_details = ($orders[0]->orderDetail);
         $vendor_price=0;
         $customer_price=0;
         foreach ($order_details as $order_detail) {
                $vendor_price+=$order_detail->requestedService->service->vendor_price ;
              //  $customer_price+=$order_detail->requestedService->service->customer_price;
           
            $SpecialPrice=  SpecialPrice::where('service_id','=',$order_detail->requestedService->service->id)
                        ->where('customer_id','=',$orders[0]->customer->id)->get();
            if(!empty($SpecialPrice[0]))
            {
                
                 $customer_price+= $SpecialPrice[0]->special_price;

            }
            else{
                $customer_price+=$order_detail->requestedService->service->customer_price;
            }




            }


     Invoice::create(array( 
            'order_id'=>$order_id,
            'total_amount'=>$vendor_price,
            'request_id'  =>$orders[0]->request_id,
            'user_id'  => $orders[0]->vendor->id,
            'user_type_id'  => $orders[0]->vendor->type_id,
            'status'=>1
                    )
                    );


      Invoice::create(array( 
                                'order_id'=>$order_id,
                                'total_amount'=>   $customer_price,
                                'request_id'  =>$orders[0]->request_id,
                                'user_id'  => $orders[0]->customer->id,
                                'user_type_id'  => $orders[0]->customer->type_id,
                                'status'=>1)
                    );

   
                                
    }

        if(Auth::user()->type_id==3)
        {
             
                //Send to admins
                $message =   "Vendor " .Auth::user()->first_name." ".Auth::user()->last_name. " has changed the status of order to ".Input::get('orderstatus_text')." of order number ". $order_id;
                //send system notifications to admin users
                $recepient_id = User::getAdminUsersId();
                foreach( $recepient_id as $rec_id)
                {
                $notification = NotificationController::doNotification($rec_id,Auth::user()->id, $message, 2,array(),"list-work-order-admin");
                }

                //Send to customer
                $recepient_id = $orders[0]->customer->id;
               
                 NotificationController::doNotification($recepient_id,Auth::user()->id, $message, 2,array(),"customer-list-work-orders");
               


         }
         else
         {
                //Send to vendor
                $message =   Auth::user()->first_name." ".Auth::user()->last_name. " has changed the status of order to ".Input::get('orderstatus_text')." of order number ". $order_id;
                $recepient_id = $orders[0]->vendor->id;
               
                 NotificationController::doNotification($recepient_id,Auth::user()->id, $message, 2,array(),"vendor-list-orders");
                //End comments


         }
    }
}
