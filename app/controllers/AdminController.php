<?php

/**
 * Admin Controller Class.
 *
 * Handles Admin redirects.
 * @copyright Copyright 2014 Invortex Technology Development Team
 * @version $Id: 1.0
 */
use JeroenDesloovere\Geolocation\Geolocation;

class AdminController extends \BaseController {

    /**
     * Redirects the admin to admin dashboard
     * @params none
     * @return redirects admin to admin dashboard.
     */

    public function addOrderReport(){
        

        $data = Input::all();
        $save =OrdersReport::addOrderReport($data);
        $message = FlashMessage::messages('admin_service.service_error');
                     return Redirect::to('/')
                            ->with('message', FlashMessage::DisplayAlert($message,'success'));
   
    }
    public function search(){
        return View::make('common.search');

    }
    public function OrderReport(){

        return View::make('pages.admin.order-report');

    }
    public function index() {
        
        $requests = MaintenanceRequest::take(5)->get();
        $orders_process = Order::where('status','=',0)->take(5)->get();
        $orders_completed = Order::where('status','=',1)->take(5)->get();
        $recent_orders = Order::take(5)->get();
        $recent_assets = Asset::take(5)->get();
        
        return View::make('pages.admin.dashboard')->with(array('requests' => $requests, 
            'orders_process' => $orders_process, 
            'orders_completed' => $orders_completed, 
            'recent_orders' => $recent_orders, 
            'recent_assets' => $recent_assets));
    }

    /**
     * Gets the user to the add user page
     * @params none
     * @return passes user roles to the User Role Drop Down.
     */
    public function addUser() {
        $roles = UserRole::where('status', '=', 1)->get(array('id', 'role_name'));
        $user_roles = array();
        foreach ($roles as $role) {
            $user_roles[$role->id] = $role->role_name;
        }

        return View::make('pages.admin.adduser')->with('user_roles', $user_roles);
    }

    /**
     * Gets the user to the add user page
     * @params none
     * @return passes user roles to the User Role Drop Down.
     */
    public function addTechnicians() {
        $user = Auth::user();
        if ($user) {
            return View::make('pages.admin.addvendor');
        } else {
            return Redirect::to('list-vendors');
        }
    }

    public function processAddVendor() {
        $rules = array(
            'first_name' => 'required|min:2|max:80|alpha',
            'last_name' => 'required|min:2|max:80|alpha',
            'email' => 'required|email|unique:users|between:3,64',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $profile_error_messages = General::validationErrors($validator);
            return $profile_error_messages;
        } else {
            $vendor_add_message = FlashMessage::messages('admin.vendor_add_success');
            $data = Input::all();
            $user_type_id = UserType::where('title', '=', 'vendors')->first();
            $user_types = UserType::find($user_type_id->id);
            $user_roles = UserRole::where('role_name', '=', $user_types->title)->first();
            $data['type_id'] = $user_type_id->id;
            $passowrd = Str::random(5, 'alpha');
            $data['password'] = Hash::make($passowrd);
            $data['status'] = 1;
            $save = User::createUser($data);
            if ($save) {

                $data['password'] = $passowrd;
                $from_email = Config::get('app.admin_email');
              /*  Mail::send('emails.admin_customer_created', $data, function($message) use ($from_email) {

                    $message->to(Input::get('email'), Input::get('first_name') . ' ' . Input::get('last_name'))
                            ->subject('Vendor Created By Admin!')
                            ->from($from_email, 'GSS');
                });
                */
                    Session::flash('message', $vendor_add_message);
                return FlashMessage::displayAlert($vendor_add_message . $passowrd, 'success');
            }
        }
    }

    public function Technicians() {
        $vendors = User::getAllVendors();

        $user = new User;


        $db_table = $user->getTable();
        return View::make('pages.admin.list_vendors')->with(array('vendors' => $vendors, 'db_table' => $db_table));
    }

    public function deleteVendor($vendor_id) {
        $user = User::find($vendor_id);
        //$user->delete();
        $message = FlashMessage::messages('admin.user_deleted');
        return Redirect::back()
                        ->with('message', FlashMessage::displayAlert($message, 'success'));
    }

    /**
     * Gets the user to the edit user page, checks if the form is submitted with updated user data or to
      show just the form pre filled.
     * @params user_id : the user id that needs to be updated.
     * @return redirects with appropriate message of success or error.
     */
    public function editUser($user_id) {
        $update_user = Input::get('update_user');
        if (!$update_user) {
            $user = User::find($user_id);
            $role_id = $user->userRole->id;
            $roles = UserRole::all();
            $user_roles = array();
            foreach ($roles as $role) {
                $user_roles[$role->id] = $role->role_name;
            }
            return View::make('pages.admin.edituser')
                            ->with(
                                    array('user' => $user,
                                        'user_roles' => $user_roles,
                                        'role_id' => $role_id
            ));
        } else {
            $user = Input::all();
            $save = User::updateAdminUser($user, $user_id);
            if ($save) {
                $message = FlashMessage::messages('admin.user_created');
                return Redirect::back()
                                ->with('message', FlashMessage::displayAlert($message, 'success'));
            }
            return $profile_error_messages;
        }
    }

    /**
     * Deletes a user
     * @params user_id : the user id that needs to be deleted.
     * @return redirects with appropriate message of success or error.
     */
    public function deleteUser($user_id) {
        $user = User::find($user_id);
        //$user->delete();
        $message = FlashMessage::messages('admin.user_deleted');
        return Redirect::back()
                        ->with('message', FlashMessage::displayAlert($message, 'success'));
    }

    /**
     * Updates a User Status to active or inavtive.
      called via ajax.
     * @params user_id : the user id that needs to be deleted.
     * @return redirects with appropriate message of success or error.
     */
    public function updateUserStatus($user_id) {
        if (Request::ajax()) {
            $activityId = Input::get('activityId');
            $check = AccessRightController::checkAccessRight($role_function_id->id, 'edit');
            if ($check) {
                if ($activityId == 'active') {
                    DB::update('update users set status = 0 where id = ?', array($user_id));
                } else {
                    DB::update('update users set status = 1 where id = ?', array($user_id));
                }
            } else {
                return Response::json('Access Denied!');
            }
        }
    }

    /**
     * Updates a User Status to active or inavtive.
      called via ajax.
     * @params user_id : the user id that needs to be deleted.
     * @return redirects with appropriate message of success or error.
     */
    public function listUser() {
        $users = User::getAdminUser();
        $user_roles = UserRole::get(array('id', 'role_name'));
        $user_table = new User;
        $db_table = $user_table->getTable();

        foreach ($user_roles as $role) {
            $roles[$role->id] = $role->role_name;
        }

        return View::make('pages.admin.listuser')
                        ->with(array(
                            'users' => $users,
                            'userRoles' => $roles,
                            'db_table' => $db_table));
    }

    /**
     * Adds a new admin user.
     * @params data from the add new user form.
     * @return redirects with appropriate message of success or error.
     */
    public function addNewUser() {

        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $email = Input::get('email');
        $user_role_id = Input::get('role_id');

        $rules = array(
            'first_name' => 'required|between:3,55',
            'last_name' => 'required|between:3,55',
            'email' => 'required|email|unique:users|between:3,64'
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $messages = $validator->messages();

            return Redirect::to('/add-user')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $user = new User;
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user_type_id = UserType::where('title', '=', 'user')->first();
            $user->type_id = $user_type_id->id; // for admin users

            $password = str_random(8);
            $user->password = Hash::make($password);
            $user->user_role_id = $user_role_id;
            $user->status = 1;
            $user_data = Input::all();

            $user_data['password'] = $password;
            //echo '<pre>'; print_r($user_data); exit;

            $user->save();
            $from_email = Config::get('app.admin_email');
            Mail::send('emails.admin_customer_created', $user_data, function($message) use ($from_email) {

                $message->to(Input::get('email'), Input::get('first_name') . ' ' . Input::get('last_name'))
                        ->subject('User Created By Admin!')
                        ->from($from_email, 'GSS');
            });
            $message = FlashMessage::messages('admin.user_created');

            return Redirect::back()
                            ->with('message', FlashMessage::displayAlert($message, 'success'));
        }
        return View::make('pages.admin.adduser');
    }

    /**
     * Shows add new access level/role form.
     * @params data from the add new access level/role form.
     * @return redirects with appropriate message of success or error.
     */
    public function addAccessLevel() {
        $submitted = Input::get('submitted');
        if ($submitted) {
            $message = FlashMessage::messages('admin.access_level_success');
            $rules = array(
                'role_name' => 'required',
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                $messages = $validator->messages();

                return Redirect::to('add-access-level')
                                ->withErrors($validator);
            } else {
                $user_role = new UserRole;
                $user_role->role_name = Input::get('role_name');
                $user_role->description = Input::get('description');
                $user_role->status = Input::get('status');
                $save = $user_role->save();
                if ($save) {
                    $role_id = $user_role->id;
                    // adding defualt access level (i.e 0 for edit update and delete) for the newly created access role.
                    $role_functions = RoleFunction::all()->toArray();
                    foreach ($role_functions as $role_func) {
                        $data['role_id'] = $role_id;
                        $data['role_function_id'] = $role_func['id'];
                        $data['add'] = 0;
                        $data['edit'] = 0;
                        $data['delete'] = 0;
                        $data['view'] = 0;
                        $data['status'] = 1;
                        RoleDetail::addRoleDetail($data);
                    }
                }
                return Redirect::back()
                                ->with('message', FlashMessage::displayAlert($message, 'success'));
            }
        } else {
            return View::make('pages.admin.add_access_level');
        }
    }

    /**
     * List all the roles/access levels.
     * @params none.
     * @return none.
     */
    public function listAccessLevel() {
        $userRoles = UserRole::getAllRoles();
        $role_obj = new UserRole;
        $db_table = $role_obj->getTable();

        return View::make('pages.admin.list_access_level')
                        ->with(array(
                            'userRoles' => $userRoles,
                            'db_table' => $db_table));
    }

    /*
     * Function Name : listMaintenanceRequest
     * @param:none
     * @description: This function is begin used for listing all over the requests of maintenance in admin
     *
     */

    public function listMaintenanceRequest() {

        $request_maintenance = MaintenanceRequest::orderBy('id', 'desc')->get();

        $request_ids = array();
        $request_service_ids = array();
        $assigned_request_ids = array();
        foreach ($request_maintenance as $mdata) {
            $request_service_ids = array();
            $request_ids[] = $mdata->id;
            foreach ($mdata->requestedService as $rqdata) {
                $request_service_ids[] = $rqdata->id;
            }
            $assigned_request_ids = array();
            $assign_requests = AssignRequest::where('request_id', '=', $mdata->id)
                                ->where('status',"!=",2)
                               ->select('request_id')->get();

            foreach ($assign_requests as $adata) {
                $assigned_request_ids[] = $adata->request_id;
            }

            $numberofrequestids['requested_services_count'][$mdata->id] = count($request_service_ids);
            $numberofrequestids['assigned_services_count'][$mdata->id] = count($assigned_request_ids);
        }




        $request_maintenance_obj = new MaintenanceRequest;
        $db_table = $request_maintenance_obj->getTable();

        return View::make('pages.admin.listmaintenancerequest')
                        ->with(array(
                            'request_maintenance' => $request_maintenance,
                            'numberofrequestids' => $numberofrequestids,
                            'db_table' => $db_table
        ));
    }


     /*
     * Function Name : listMaintenanceRequest
     * @param:none
     * @description: This function is begin used for listing all over the requests of maintenance in admin
     *
     */

    public function listAssignedMaintenanceRequest() {

        $request_maintenance = MaintenanceRequest::orderBy('id', 'desc')->get();

        $request_ids = array();
        $request_service_ids = array();
        $assigned_request_ids = array();
        foreach ($request_maintenance as $mdata) {
            $request_service_ids = array();
            $request_ids[] = $mdata->id;
            foreach ($mdata->requestedService as $rqdata) {
                $request_service_ids[] = $rqdata->id;
            }
            $assigned_request_ids = array();
            $assign_requests = AssignRequest::where('request_id', '=', $mdata->id)
                                ->where('status',"!=",2)
                                ->select('request_id')
                                ->get();

            foreach ($assign_requests as $adata) {
                $assigned_request_ids[] = $adata->request_id;
            }

            $numberofrequestids['requested_services_count'][$mdata->id] = count($request_service_ids);
            $numberofrequestids['assigned_services_count'][$mdata->id] = count($assigned_request_ids);
        }
    

        $request_maintenance_obj = new MaintenanceRequest;
        $db_table = $request_maintenance_obj->getTable();

        return View::make('pages.admin.listassignedmaintenancerequest')
                        ->with(array(
                            'request_maintenance' => $request_maintenance,
                            'numberofrequestids' => $numberofrequestids,
                            'db_table' => $db_table
        ));
    }


    /*
     * Function Name : viewMaintenanceRequest
     * @param:id
     * @description: This function is begin used for  viewing  all over the  details requests of maintenance in admin
     *
     */

    public function viewMaintenanceRequest($maintenance_request_id = "") {


        $request_maintenance = MaintenanceRequest::find($maintenance_request_id);


        MaintenanceRequest::where('id','=',$maintenance_request_id)
                            ->update(array('status'=>'2')); //Reviewed by Admin

        return View::make('pages.admin.viewmaintenancerequest')
                        ->with(array(
                            'request_maintenance' => $request_maintenance,
        ));
    }

    /*
     * Function Name : assetView
     * @param:$asset_id
     * @description: This function is being used for asset view popup
     *
     */

    public function assetView($asset_id = "") {
        $asset = Asset::find($asset_id);
        
        return View::make('pages.admin.assetview')
                        ->with(array(
                            'asset' => $asset,
        ));
    }

    /*
     * Function Name : showServices
     * @param:$ma
     * @description: This function is being used for asset view popup
     *
     */

    public function showMaintenanceServices($maintenance_request_id = "") {
        $request_maintenance = MaintenanceRequest::find($maintenance_request_id);

        $already_assigned_users = array();
        foreach ($request_maintenance->assignRequest as $assigned_users) {
            if ($assigned_users->status == 2) {
                $already_assigned_users[] = $assigned_users->vendor_id;
            }
        }

        $assign_requests = AssignRequest::where('request_id', '=', $maintenance_request_id)
                        ->where("status","!=",2)
                        ->select('requested_service_id', 'vendor_id', 'status')->get();


        $assignedservice = array();
        foreach ($assign_requests as $data) {
            $assignedservice[] = $data->requested_service_id;
        }



        //For declined request

        $assign_requests_service = AssignRequest::where('request_id', '=', $maintenance_request_id)
                ->where('status', '=', 2)
                ->orwhereIn('requested_service_id', array(3, 4))
                ->orwhereIn('request_id', array($maintenance_request_id))
                ->select('requested_service_id', 'vendor_id', 'status')
                ->get();


        $assigned_service_request = array();
        foreach ($assign_requests_service as $data) {
            $assigned_service_request[] = $data->requested_service_id;
        }


        $userobj = new User;
        $lat = $request_maintenance->asset->latitude;
        $lon = $request_maintenance->asset->longitude;
        $vendors = $userobj->getUserByTypeId(3, $lat, $lon, 600);

        return View::make('pages.admin.showmaintenanceservices')
                        ->with(array(
                            'request_maintenance' => $request_maintenance,
                            'vendors' => $vendors,
                            'assigned_services' => $assignedservice,
                            'assigned_service_request' => $assigned_service_request,
                            'already_assigned_users' => array_unique($already_assigned_users)
        ));
    }

    public function listWorkOrder() {
        $work_orders = Order::listAllWorkOrder();


       $list_orders = array();
        $i = 0;


        foreach ($work_orders as $order) {
            $order_details = ($order->orderDetail);
 //Property Address, City, State, Zip fields
            $list_orders[$i]['order_id'] = $order->id;
            $list_orders[$i]['customer_name'] = $order->customer->first_name . ' ' . $order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $order->vendor->first_name . ' ' . $order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $order->created_at;
            $list_orders[$i]['service_name'] = '';
            $list_orders[$i]['property_address'] = $order->maintenanceRequest->asset->property_address;
            $list_orders[$i]['city'] = $order->maintenanceRequest->asset->city->name;
            $list_orders[$i]['state'] = $order->maintenanceRequest->asset->state->name;
            $list_orders[$i]['zipcode'] = $order->maintenanceRequest->asset->zip;
            
            $list_orders[$i]['status'] = $order->status;
            $list_orders[$i]['status_class'] = ($order->status==1)? "warning": $order->status_class; ;
            $list_orders[$i]['status_text'] = ($order->status==1)? "In-Process":$order->status_text;;


            foreach ($order_details as $order_detail) {
               $list_orders[$i]['service_name'].=$order_detail->requestedService->service->title . ', <br>';
            }
            $i++;
        }


        return View::make('pages.admin.list_work_order')
                        ->with('orders', $list_orders)
                        ->with('db_table', 'orders');
    }

    public function listCompletedOrders() {
        $orders = Order::listCompletedOrders();

       $list_orders = array();
        $i = 0;

        foreach ($orders as $order) {
            $order_details = ($order->orderDetail);
 
            $list_orders[$i]['order_id'] = $order->id;
            $list_orders[$i]['customer_name'] = $order->customer->first_name . ' ' . $order->customer->last_name;
            $list_orders[$i]['vendor_name'] = $order->vendor->first_name . ' ' . $order->vendor->last_name;
            $list_orders[$i]['asset_number'] = $order->maintenanceRequest->asset->asset_number;
            $list_orders[$i]['order_date'] = $order->created_at;
            $list_orders[$i]['service_name'] = '';
            $list_orders[$i]['status'] = $order->status;
            $list_orders[$i]['status_class'] = ($order->status==1)? "warning": $order->status_class; ;
            $list_orders[$i]['status_text'] = ($order->status==1)? "In-Process":$order->status_text;;

            $i++;
        }


        return View::make('pages.admin.list_completed_orders')
        ->with('orders', $list_orders);
    }

    public function editProfileAdmin($id) {
        $user_data = User::find($id);
        $user_type = UserType::getUserTypeByID($user_data->type_id);
        $cities = City::getAllCities();
        $states = State::getAllStates();
        return View::make('pages.admin.edit_profile')->with('cities', $cities)->with('states', $states)->with('user_data', $user_data)->with('user_type', $user_type);
    }

    /**
     * Process edit profile data & update database accordingly.
     * @params none
     * @return return success & error message through AJAX.
     */
    public function saveUserProfile($id) {

        $user_data = User::find($id);
        // $id = Auth::user()->id;
        $username = $user_data->username;
        Validator::extend('hashmatch', function($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->$parameters[0]);
        });

        $messages = array(
            'hashmatch' => 'Your current password must match your account password.'
        );

        if (Input::get('change_password')) {
            $rules = array(
                'first_name' => 'required|min:2|max:80|alpha',
                'last_name' => 'required|min:2|max:80|alpha',
                'phone' => 'required|numeric',
                'address_1' => 'required|min:8|max:100',
                'zipcode' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
                'current_password' => 'hashmatch:password',
                'password' => 'required|between:4,20|confirmed',
                'password_confirmation' => 'same:password',
            );
        } else {
            $rules = array(
                'first_name' => 'required|min:2|max:80|alpha',
                'last_name' => 'required|min:2|max:80|alpha',
                'phone' => 'required|numeric',
                'address_1' => 'required|min:8|max:100',
                'zipcode' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
            );
        }
        $street = '';
        $streetNumber = '';
        $city_id = Input::get('city_id');
        $city = City::find($city_id)->name;
        $zip = Input::get('zipcode');
        $country = 'United States';
        $result = Geolocation::getCoordinates($street, $streetNumber, $city, $zip, $country);

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            $validation_messages = $validator->messages()->all();
            $profile_error_messages = '';
            foreach ($validation_messages as $validation_message) {
                $profile_error_messages.="<h4 class='alert alert-error'>" . $validation_message . "</h4>";
            }
            return $profile_error_messages;
        } else {
            $profile_message = FlashMessage::messages('vendor.profile_edit_success');
            $data = Input::all();
            $data['latitude'] = $result['latitude'];
            $data['longitude'] = $result['longitude'];
            if (!Input::get('change_password')) {
                $data['password'] = Auth::user()->password;
            } else {
                $data['password'] = Hash::make($data['password']);
            }
            $file = Input::file('profile_picture');
            if ($file) {
                $destinationPath = Config::get('app.upload_path');
                $filename = $file->getClientOriginalName();
                $filename = str_replace('.', '-' . $username . '.', 'profile-' . $filename);
                $data['profile_picture'] = $filename;
                Input::file('profile_picture')->move($destinationPath, $filename);
            } else {
                $data['profile_picture'] = Auth::user()->profile_picture;
            }
            $save = User::profile($data, $id);
            if ($save) {
                return FlashMessage::displayAlert($profile_message, 'success');
            }
        }
    }



    /**
     * List All bid requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listBidRequests($status=1) {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('status',"=", $status)
                        ->orderBy('id', 'desc')->get();

                 
        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)
            ->where('status','=',1)
            ->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->user->first_name .' '. $request->user->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {
                
                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.admin.list_bid_requests')
        ->with('assign_requests', $assign_requests)
        ->with('status',$status);
    }


    /**
     * List All bid requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listApprovedBidRequests() {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('status',"=", 2)
                        ->orderBy('id', 'desc')->get();

                 
        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)
            ->where('status','=',1)
            ->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->user->first_name .' '. $request->user->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {
                
                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.admin.list_bid_requests')->with('assign_requests', $assign_requests);
    }


    /**
     * List All bid requests by admin.
     * @params none
     * @return List of assigned requests through AJAX.
     */
    public function listDeclinedBidRequests() {

        $user_id = Auth::user()->id;

        $requests = BidRequest::where('status',"=", 3)
                        ->orderBy('id', 'desc')->get();

                 
        $assign_requests = array();
        $i = 0;
        foreach ($requests as $request) {
            $services = BidRequestedService::where('request_id', '=', $request->id)
            ->where('status','=',1)
            ->get();
            $assign_requests[$i]['request_id'] = $request->id;
            $assign_requests[$i]['service_code'] = '';
            $assign_requests[$i]['service_name'] = '';
            $assign_requests[$i]['customer_name'] = $request->user->first_name .' '. $request->user->last_name;
            $assign_requests[$i]['asset_number'] = $request->asset->asset_number;
            $assign_requests[$i]['request_date'] = $request->created_at;
            $assign_requests[$i]['due_date'] = $request->created_at;
            foreach ($services as $service) {
                
                $assign_requests[$i]['service_code'].='&diams; '.$service->service->service_code . ', <br>';
                $assign_requests[$i]['service_name'].='&diams; '.$service->service->title . ', <br>';
            }
            $i++;
        }

        return View::make('pages.admin.list_bid_requests')->with('assign_requests', $assign_requests);
    }


         /*
        Add bid for a particular workorder
        */
          public function addBidRequest()
        {

      // Get all customer assets to send in view
     
        $services = Service::getAllServices(); // get all services provided by admin
     

            //get vendor id
            $vendor_id = Auth::user()->id;
          // All in progress work orders
            $orders = Order::where('status', '=', '1')
                     ->get();
           
           $order_ids=array();
           foreach($orders as $order):
           $order_ids[$order->id."--".$order->MaintenanceRequest->Asset->id]=  $order->id."-".$order->MaintenanceRequest->Asset->property_address;
            endforeach;

             return View::make('pages.admin.add-bid')
             ->with('order_ids', $order_ids)
             ->with('services', $services);
            }

             /**
     * Create Bid request
     * @params none
     * @return Redirect to maintenance request page
     */
    public function createBidServiceRequest() {
        $data = Input::all(); // get all submitted data of user
        //$customer_id = Auth::user()->id;
        //for customer id
        $exploded_orderid_asset_id=explode("--",$data['work_order']);
        $orderData= Order::find($exploded_orderid_asset_id[0]);
        $request['vendor_id'] = $orderData->vendor_id; // assign current logged id to request
        $request['asset_id'] = $exploded_orderid_asset_id[1]; // assign asset number to request
        $request['order_id'] = $exploded_orderid_asset_id[0];
        $request['customer_id'] = $orderData->customer->id;
        $request['status'] = 1; // while generating request status would be active
        // Add maintainence request to main table
        $add_request = BidRequest::addMaintenanceRequest($request);
        // get last id to assign to each service request
        $request_id = DB::getPdo()->lastInsertId();

        //check if request in created then insert services to service table
        if ($add_request) {
            //Select all selected service to
            $selected_services = $data['service_ids_selected'];

            /// loop through all selected services
            foreach ($selected_services as $service_id) {
               $request_detail['maintenance_request_id'] =$orderData->request_id;

                $request_detail['request_id'] = $request_id; // assign request id to $requested detail
                $request_detail['service_id'] = $service_id; // assign service_id to $requested detail
                $request_detail['status'] = 1;

                if (isset($data['service_required_date_' . $service_id])) {
                    $request_detail['required_date'] = General::displayDate($data['service_required_date_' . $service_id]);
                    $request_detail['required_time'] = General::displayTime($data['time_hours_' . $service_id], $data['time_minutes_' . $service_id], $data['time_meridiem_' . $service_id]);
                } else {
                    unset($request_detail['required_time']);
                    unset($request_detail['required_date']);
                }

                if (isset($data['number_of_men_' . $service_id])) {
                    $request_detail['service_men'] = $data['number_of_men_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['service_note_' . $service_id])) {
                    $request_detail['service_note'] = $data['service_note_' . $service_id];
                } else {
                    unset($request_detail['service_men']);
                }
                if (isset($data['verified_vacancy_' . $service_id])) {
                    $request_detail['verified_vacancy'] = $data['verified_vacancy_' . $service_id];
                } else {
                    unset($request_detail['verified_vacancy']);
                }

                if (isset($data['cash_for_keys_' . $service_id])) {
                    $request_detail['cash_for_keys'] = $data['cash_for_keys_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys']);
                }

                if (isset($data['cash_for_keys_trash_out_' . $service_id])) {
                    $request_detail['cash_for_keys_trash_out'] = $data['cash_for_keys_trash_out_' . $service_id];
                } else {
                    unset($request_detail['cash_for_keys_trash_out']);
                }

                if (isset($data['trash_size_' . $service_id])) {
                    $request_detail['trash_size'] = $data['trash_size_' . $service_id];
                } else {
                    unset($request_detail['trash_size']);
                }

                if (isset($data['storage_shed_' . $service_id])) {
                    $request_detail['storage_shed'] = $data['storage_shed_' . $service_id];
                } else {
                    unset($request_detail['storage_shed']);
                }

                if (isset($data['lot_size_' . $service_id])) {
                    $request_detail['lot_size'] = $data['lot_size_' . $service_id];
                } else {
                    unset($request_detail['lot_size']);
                }
                $add_requested_service = BidRequestedService::addRequestedService($request_detail);
                $request_detail_id = DB::getPdo()->lastInsertId(); // get last id of service
                //check if service is created then insert images of service
                if ($add_requested_service) {
                    if (isset($data['service_image_list_' . $service_id])) {
                        $service_images = $data['service_image_list_' . $service_id];
                        foreach ($service_images as $image) {
                            $image_detail['requested_id'] = $request_detail_id;
                            $image_detail['image_name'] = $image;
                            $image_detail['image_type'] = 'request';
                            $image_detail['status'] = 1;
                            $add_image = BidServiceImage::addServiceImage($image_detail);
                        }
                    }
                }
            }
        }
        $customer_assets = Asset::getAssetsByCustomerId(Auth::user()->id);
        $services = Service::getAllServices();
        if ($add_request) {
            $message = FlashMessage::messages('customer.request_service_add');
            return Redirect::back()
                            ->with('message', FlashMessage::displayAlert($message, 'success'));
        }
    }

     /*
     * Function Name : viewMaintenanceRequest
     * @param:id
     * @description: This function is begin used for  viewing  all over the  details requests of maintenance in admin
     *
     */

    public function viewBidRequest($maintenance_request_id = "") {


            $request_maintenance = BidRequest::find($maintenance_request_id);
        
            $assign_requests = BidRequestedService::where('request_id', '=', $maintenance_request_id)
                            
                              ->get();
          
            

        return View::make('pages.admin.viewadminbidrequest')
                        ->with(array(
                            'request_maintenance' => $request_maintenance,
                             'assign_requests'=>$assign_requests
        ));
    }

    /*
    Bid request declined by Admin
    @dafault status =3
    */
    public function DeclineBidRequest() {
        $input = Input::all();
        // declined bid request status
          $data = array('status' => 3 );
            $save = BidRequest::find($input['request_id'])->update($data);

           
        return "Bid Request has been declined";
       
    }


    /*
         Bid request accepted by admin
         @dafault status =2
        */
         public function acceptBidRequest() {
        $input = Input::all();
        
        $BidRequestedService = BidRequestedService::where('request_id','=',$input['request_id'])
                            ->get();
                          

                            $bidData=array();
        foreach($BidRequestedService as $biddatavalue)
        {
          
           $bidData['request_id']=$biddatavalue->maintenance_request_id;
           $bidData['service_id']=$biddatavalue->service_id;
           $bidData['status']=1;
           $bidData['created_at']=$biddatavalue->created_at;
           $bidData['updated_at']=$biddatavalue->updated_at;
           $bidData['required_date']=$biddatavalue->required_date;
           $bidData['required_time']=$biddatavalue->required_time;
           $bidData['service_men']=$biddatavalue->service_men;
           $bidData['service_note']=$biddatavalue->service_note;
           $bidData['customer_note']=$biddatavalue->customer_note;
           $bidData['vendor_note']=$biddatavalue->vendor_note;
           $bidData['verified_vacancy']=$biddatavalue->verified_vacancy;
           $bidData['cash_for_keys']=$biddatavalue->cash_for_keys;
           $bidData['cash_for_keys_trash_out']=$biddatavalue->cash_for_keys_trash_out;
           $bidData['trash_size']=$biddatavalue->trash_size;
           $bidData['storage_shed']=$biddatavalue->storage_shed;
           $bidData['lot_size']=$biddatavalue->lot_size;


           $add_requested_service = RequestedService::addRequestedService($bidData);
           $request_detail_id = DB::getPdo()->lastInsertId(); // get last id of service
              
          
              $imageDataArray=BidServiceImage::where('requested_id','=',$biddatavalue->id)->get();
                        
                        foreach ( $imageDataArray as $imageData) {
                         
                            $image_detail['requested_id'] = $request_detail_id;
                            $image_detail['image_name'] = $imageData->image_name;
                            $image_detail['image_type'] = 'request';
                            $image_detail['status'] = 1;
                            $add_image = ServiceImage::addServiceImage($image_detail);
                        }
                    $dataRequests['request_id']=$biddatavalue->maintenance_request_id;
                    $dataRequests['requested_service_id']=$request_detail_id;
                    $dataRequests['vendor_id']=$input['vendor_id'];
                    $dataRequests['status']=3;
                   

                $accept_request = AssignRequest::create($dataRequests);
             
                $orderDATA= Order::where('request_id','=',$biddatavalue->maintenance_request_id)
                                 ->where('vendor_id','=',$input['vendor_id'])
                                 ->first();

                $order_details['requested_service_id'] =  $request_detail_id;
                $order_details['order_id'] =$orderDATA->id;
                $order_details['status'] = 1;
                OrderDetail::create($order_details);


        }

            // accepted bid request status
          $data = array('status' => 2 );
            $save = BidRequest::find($input['request_id'])->update($data);

           
        return "Bid Request has been accepted";
       
    }


}
