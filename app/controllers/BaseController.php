<?php
class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *sdfsd
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}



	public function __construct()
	{
		if(Auth::check())
		{
			$role_id = Auth::user()->user_role_id;
			$role_detail = RoleDetail::where('role_id', '=', $role_id)
			->get();
            $access_roles=array();

				foreach($role_detail as $role)
				{
						$rol_func = $role->roleFunctions->role_function;
					
						$access_roles[$rol_func]['add'] = $role['add'];
						$access_roles[$rol_func]['edit'] = $role['edit'];
						$access_roles[$rol_func]['delete'] = $role['delete'];
						$access_roles[$rol_func]['view'] = $role['view'];
					
				}
			
			View::share('access_roles', $access_roles);
			$getNotifications    =	Notification::getNotifications(Auth::user()->id);
			$unreadnotifications =	Notification::where('recepient_id','=',Auth::user()->id)
												->where('is_read','=',1)
												->skip(0)
                                         		->take(5)
												->count();

										
			View::share('get_notifications', $getNotifications);
			View::share('unreadnotifications', $unreadnotifications);
	  }
	}

}
