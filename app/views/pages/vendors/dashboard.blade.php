@if(!Request::ajax())
@extends('layouts.vendordashboard')
@section('content')
<div id="content" class="span11">
@endif
<!-- start: Content -->


    <div class="row-fluid">

        <div class="box span12 main-title">
            <h1>Vendor Dashboard</h1>
        </div><!--/span-->

    </div><!--/row-->


    <div class="row-fluid">

        <div class="span">

            <div class="row-fluid">
                <div class="box span12">

                    <h1>Quick View</h1>

                    <div class="box-content main-button">

                        <a href="{{URL::to('vendor-assigned-requests')}}" class="quick-button span4">
                            <i class="fa-icon-briefcase"></i>
                            <p>View Assign Request</p>
                        </a>
                        <a href="{{URL::to('vendor-list-orders')}}" class="quick-button span4">
                            <i class="fa-icon-cog"></i>
                            <p>Work Orders</p>
                        </a>
                        <a href="{{URL::to('vendor-list-completed-orders')}}" class="quick-button span4">
                            <i class="fa-icon-tasks"></i>
                            <p>Completed Work Orders</p>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">

                <div class="box span6">
                    <div class="box-header">
                        <h2>Recent Service Request</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Req No</th>
                                    <th><i class="fa-icon-caret-up"></i> Asset No</th>
                                    <th>Loan</th>
                                    <th>Status</th>                                          
                                </tr>
                            </thead>   
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-important">Assign</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-warning">Pending</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>                                   
                            </tbody>
                        </table>  
                        <div class="pagination pagination-centered">
                            <ul>
                                <li><a href="#">Prev</a></li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>     
                    </div>
                </div><!--/span-->


                <div class="box span6">
                    <div class="box-header">
                        <h2>Recent Completed Request</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Req No</th>
                                    <th><i class="fa-icon-caret-up"></i> Asset No</th>
                                    <th>Loan</th>
                                    <th>Status</th>                                          
                                </tr>
                            </thead>   
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-important">Assign</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-warning">Pending</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>                                   
                            </tbody>
                        </table>  
                        <div class="pagination pagination-centered">
                            <ul>
                                <li><a href="#">Prev</a></li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>     
                    </div>
                </div>

            </div>

            <div class="row-fluid">

                <div class="box span6">
                    <div class="box-header">
                        <h2>Recent Workorders Inprocess</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Req No</th>
                                    <th><i class="fa-icon-caret-up"></i> Asset No</th>
                                    <th>Loan</th>
                                    <th>Status</th>                                          
                                </tr>
                            </thead>   
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-important">Assign</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-warning">Pending</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>                                   
                            </tbody>
                        </table>  
                        <div class="pagination pagination-centered">
                            <ul>
                                <li><a href="#">Prev</a></li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>     
                    </div>
                </div>

                <div class="box span6">
                    <div class="box-header">
                        <h2>Recent Assets</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Req No</th>
                                    <th><i class="fa-icon-caret-up"></i> Asset No</th>
                                    <th>Loan</th>
                                    <th>Status</th>                                          
                                </tr>
                            </thead>   
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-important">Assign</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-warning">Pending</span>
                                    </td>                                       
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td class="middle">36</td>
                                    <td>123</td>
                                    <td>
                                        <span class="label label-success">Review</span>
                                    </td>                                        
                                </tr>                                   
                            </tbody>
                        </table>  
                        <div class="pagination pagination-centered">
                            <ul>
                                <li><a href="#">Prev</a></li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>     
                    </div>
                </div>

            </div>

        </div><!--/span-->

    </div><!--/row-->


<!-- end: Content -->
@if(!Request::ajax())
</div>
@stop
@endif
