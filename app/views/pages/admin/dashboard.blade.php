@extends('layouts.default')
@section('content')
<div id="content" class="span11">
									
			<div class="row-fluid">
            	
                <div class="box span12 main-title">
                	<h1>Home</h1>
                </div><!--/span-->
            
            </div><!--/row-->
            
            
            <div class="row-fluid">
                
				<div class="span">
                 @if(Session::has('message'))
    				{{Session::get('message')}}
				 @endif
                                    <div class="row-fluid">
				
				<div class="widget span12" onTablet="span12" onDesktop="span12">
					<h2><span class="glyphicons usd"><i></i></span>Invoices</h2>
					<hr>
					<div class="content">
						<div id="donutchart" style="height:300px" ></div>
					</div>
				</div><!--/span-->
				
				<div class="widget span4" onTablet="span4" onDesktop="span4">
					<h2><span class="glyphicons user"><i></i></span>Tvs,Electronics and Home Theater</h2>
					<hr>
					<div class="content">
						<div id="ad" style="height:300px" ></div>
					</div>
				</div><!--/span-->
			       <div class="widget span4" onTablet="span4" onDesktop="span4">
          <h2><span class="glyphicons user"><i></i></span>Computer and related peripherals</h2>
          <hr>
          <div class="content">
            <div id="ad" style="height:300px" ></div>
          </div>
        </div><!--/span-->
      

        <div class="widget span4" onTablet="span4" onDesktop="span4">
          <h2><span class="glyphicons user"><i></i></span>Appliances, Pump, heating and A/C</h2>
          <hr>
          <div class="content">
            <div id="ad" style="height:300px" ></div>
          </div>
        </div><!--/span-->
      

			</div>
                                    
                            <div class="row-fluid">
				
				<div class="box">
					<div class="box-header">
						<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Customers</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div id="sincos"  class="center" style="height:300px;" ></div>
						<p id="hoverdata">Mouse position at (<span id="x">0</span>, <span id="y">0</span>). <span id="clickdata"></span></p>
					</div>
				</div>
				

				
				

			</div><!--/row-->        
                                    
                                    
			  <div class="row-fluid">		
                     <div class="box span12">
                            <div class="box-header">
                                <h2>Recent Maintenance Request</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Sr#</th>
                                              <th>Req No</th>
                                              <th><i class="fa-icon-caret-up"></i> Property #</th>
                                              <th>Customer Name</th>
                                              <th>Request Date</th> 
                                              <th>No. of Services</th> 
                                          </tr>
                                      </thead>   
                                      <tbody>
                                        {{--*/ $loop = 1 /*--}}
                                        @foreach ($requests as $request)
                                            <tr>
                                                <td>{{ $loop }}</td>
                                                <td>{{ $request->id }}</td>
                                                <td>{{ $request->asset->asset_number }}</td>
                                                <td>{{ $request->user->first_name }}</td>
                                                <td>{{ date("d F Y",strtotime($request->created_at)) }}</td>
                                                <td>{{ $request->requestedService->count() }}</td> 
                                            </tr>
                                        {{--*/ $loop++ /*--}}
                                        @endforeach                  
                                      </tbody>
                                 </table>      
                            </div>
                        </div><!--/span-->
                          </div>
                    
                          <div class="row-fluid">
                        <div class="box span12">
                            <div class="box-header">
                                <h2>Work Order in Process</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Sr#</th>
                                              <th>Order ID</th>
                                              <th><i class="fa-icon-caret-up"></i>Customer Name</th>
                                              <th>Property #</th>
                                              <th>Vendor Name</th> 
                                              <th>Order Date</th> 
                                          </tr>
                                      </thead>   
                                      <tbody>
                                      {{--*/ $loop = 1 /*--}}
                                        @foreach ($orders_process as $order)
                                        <tr>
                                            <td>{{ $loop }}</td>
                                            <td>{{ $order->id }}</td>
                                            <td class="middle">
                                                {{ $order->customer->first_name }} {{ $order->customer->last_name }}
                                            </td>
                                            <td >{{ $order->maintenanceRequest->asset->asset_number }}</td>
                                            <td>{{ $order->vendor->first_name }} {{ $order->vendor->last_name }}</td> 
                                            <td>{{ date("d F Y",strtotime($order->created_at)) }}</td> 
                                        </tr>
                                        {{--*/ $loop++ /*--}}
                                        @endforeach                       
                                      </tbody>
                                 </table>      
                            </div>
                        </div><!--/span-->
                          </div>
                                    
                                    <div class="row-fluid">
                        <div class="box span12">
                            <div class="box-header">
                                <h2>Work Order Approval Request</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Sr#</th>
                                              <th>Order ID</th>
                                              <th><i class="fa-icon-caret-up"></i>Customer Name</th>
                                              <th>Property #</th>
                                              <th>Vendor Name</th> 
                                              <th>Order Date</th> 
                                          </tr>
                                      </thead>   
                                      <tbody>
                                        {{--*/ $loop = 1 /*--}}
                                        @foreach ($orders_completed as $order)
                                        <tr>
                                            <td>{{ $loop }}</td>
                                            <td>{{ $order->id }}</td>
                                            <td class="middle">
                                                {{ $order->customer->first_name }} {{ $order->customer->last_name }}
                                            </td>
                                            <td >{{ $order->maintenanceRequest->asset->asset_number }}</td>
                                            <td>{{ $order->vendor->first_name }} {{ $order->vendor->last_name }}</td> 
                                            <td>{{ date("d F Y",strtotime($order->created_at)) }}</td> 
                                        </tr>
                                        {{--*/ $loop++ /*--}}
                                        @endforeach 
                                                            
                                      </tbody>
                                 </table>     
                            </div>
                        </div><!--/span-->
                          </div>
                    
                    <div class="row-fluid">
     
                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Workorders</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Req No</th>
                                              <th><i class="fa-icon-caret-up"></i> Property No</th>
                                              <th>Customer</th>
                                              <th>Status</th>                                          
                                          </tr>
                                      </thead>   
                                      <tbody>
                                        <tr>
                                                                                   
                                        </tr> 
                                        {{--*/ $loop = 1 /*--}}
                                        @foreach ($recent_orders as $order)
                                        <tr>
                                            <td>{{ $loop }}</td>
                                            <td>{{ $order->maintenanceRequest->asset->asset_number }}</td>
                                            <td>{{ $order->customer->first_name }} {{ $order->customer->last_name }}</td>
                                            <td>
                                             <span class="label label-@if($order->status==1){{'warning'}}@else{{$order->status_class}}@endif">@if($order->status==1) In-Progress @else {{$order->status_text}} @endif</span>
                                                   
                                            </td> 
                                        </tr>
                                        {{--*/ $loop++ /*--}}
                                        @endforeach 
                                      </tbody>
                                 </table>       
                            </div>
                        </div>
      
                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Properties</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Req No</th>
                                              <th><i class="fa-icon-caret-up"></i> Property No</th>
                                              <th>Loan</th>
                                              <th>Status</th>                                          
                                          </tr>
                                      </thead>   
                                      <tbody>
                                        {{--*/ $loop = 1 /*--}}
                                            @foreach ($recent_assets as $recent_asset)
                                              <tr>
                                                <td>{{$recent_asset->id}}</td>
                                                <td class="middle">{{$recent_asset->asset_number}}</td>
                                                <td>{{$recent_asset->loan_number}}</td>
                                                <td>
                                                    @if($recent_asset->status == 1)
                                                       <span class="label label-success">Active</span>
                                                    @else
                                                        <span class="label label-error">InActive</span>
                                                    @endif
                                                </td>                                       
                                            </tr>
                                            {{--*/ $loop++ /*--}}
                                        @endforeach                                   
                                      </tbody>
                                 </table>      
                            </div>
                        </div>
                        
                    </div>
                    
				</div><!--/span-->

			</div><!--/row-->
					
			</div>
@stop