@extends('layouts.default')
@section('content')
<div id="content" class="span11">
<p id="message" style="display:none">Saved...</p>
  <div class="row-fluid">
    <div class="box span12">
     
        <div class="box-header" data-original-title>
						<h2>Maintenance Request Details</h2>
					</div>
      					@if(Session::has('message'))
                            {{Session::get('message')}}
                        @endif
      <div class="box-content">
        <table class="table"> 
							  <tbody>
								<tr>
									<td class="center span3"><h2>Request ID:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->id }}</h2></td>
									<td class="center span3"><h2>Request Date:</h2></td>
									<td class="center span3"><h2>{{ date('d/m/Y', strtotime($request_maintenance->created_at)) }}</h2></td>
								</tr>
								<tr>
									<td class="center span3"><h2>Asset #:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->asset->asset_number }}  <button class="btn btn-small btn-success" data-target="#showServiceid"  onclick="viewAsset({{ $request_maintenance->asset->id }})">View Property</button></h2> </td>
									<td class="center span3"><h2>Customer Name:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->user->first_name.' '.$request_maintenance->user->last_name }} </h2></td>
								</tr>
								<tr>
									<td class="center span3"><h2>City:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->asset->city->name }}</h2></td>
									<td class="center span3"><h2>State:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->asset->state->name }}</h2></td>
								</tr>
								<tr>
									<td class="center span3"><h2>Zip:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->asset->zip }}</h2></td>
									<td class="center span3"><h2>Email:</h2></td>
									<td class="center span3"><h2>{{ $request_maintenance->user->email}}</h2></td>
								</tr>
							  </tbody>
						 </table>    
      </div>
    </div>
    <!--/span--> 
    
    <span><h1 class="text-center">Requested Services</h1></span>
			<button data-toggle="modal" data-target="#assign" class="btn btn-large btn-success" onclick="showMaintenanceServices('{{ $request_maintenance->id }}')">Assign Vendor</button>
<?php
$totalPriceCustomer=0;
$totalPriceVendor=0;

?>
		@foreach ($request_maintenance->requestedService as $services)


                <div class="row-fluid">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>{{$services->service->title}}</h2>
						<div class="box-icon">
						 <?php
  $priceData=SpecialPrice::getSpecialCustomerPrice($request_maintenance->user->id,$services->service->id);
   $servicePrice="";
 
    if(!empty($priceData) )
    {
        $servicePrice=$priceData->special_price;
        $totalPriceCustomer += $priceData->special_price;
    }
    else {
        
        $servicePrice=$services->service->customer_price;
          $totalPriceCustomer += $services->service->customer_price;      
         }    
    $totalPriceVendor += $services->service->vendor_price;
    ?>
               Customer Price :  ${{$servicePrice}} ::::: Vendor Price: ${{$services->service->vendor_price}}
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table> 
								<tr>
									<td class="center"><h2>Customer Note:</h2>{{$services->service_note}}</td>
								</tr>
								<tr>
									<div class="row-fluid browse-sec">
                                                   

            
                                                                            
                              @if(count($services->serviceImages)!=0)<h2>Images</h2>@endif
                                                       
                                                        
                                                          <ul class="media-list ">
                            @foreach($services->serviceImages as $images)

                            <li>{{ HTML::image(Config::get('app.request_images').'/'.$images->image_name) }}</li>
                            @endforeach


                        </ul>
                                                        
                                                      </div>
								</tr>
								
						 </table>      
					</div>
				</div><!--/span-->

			</div><!--/row-->
                @endforeach
			
			
			
			
  <div style="float:right;"><h2>Total Customer Price: ${{$totalPriceCustomer}} Total Vendor Price: ${{$totalPriceVendor}} </h2>
    </div>  
    
  </div>

<div class="modal  hide fade modelForm larg-model"  id="showVendorList">
        	
                 </div>

<div class="modal hide fade modelForm larg-model"  id="showServiceid"></div>
		
  
  
 
</div>
@parent
@include('common.delete_alert')
@stop
