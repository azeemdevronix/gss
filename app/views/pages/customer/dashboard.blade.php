@extends('layouts.customer_dashboard')
@section('content')

<div id="content" class="span11">

			<div class="row-fluid">

                <div class="box span12 main-title">
                	<h1>Customer Dashboard</h1>
                </div><!--/span-->

            </div><!--/row-->


            <div class="row-fluid">

				<div class="span">

                    <div class="row-fluid">
                    	<div class="box span12">

                            <h1>Quick View</h1>

                            <div class="box-content main-button">

                                <a href="{{URL::to('view-assets-list')}}" class="quick-button span4">
                                    <i class="fa-icon-briefcase"></i>
                                    <p>View My Properties</p>
                                </a>
                                <a href="{{URL::to('add-new-service-request')}}" class="quick-button span4">
                                    <i class="fa-icon-cog"></i>
                                    <p>Request A Service</p>
                                </a>
                                <a href="{{URL::to('customer-list-work-orders')}}" class="quick-button span4">
                                    <i class="fa-icon-tasks"></i>
                                    <p>View Work Orders</p>
                                </a>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">

                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Service Request</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Request ID</th>
                                              <th>Customer</th>
                                              <th>Property #</th>
                                              <th>City</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach($maintenanceRequest as $request)
                                        <tr>
                                            <td>{{$request->id}}</td>
                                            <td>{{$request->user->first_name}} {{$request->user->last_name}}</td>
                                            <td>{{$request->asset->asset_number}}</td>
                                            <td>
                                             {{$request->asset->city->name}}
                                            </td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                 </table>
     
                            </div>
                        </div><!--/span-->


                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Completed Request</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Req No</th>
                                              <th><i class="fa-icon-caret-up"></i> Property #</th>
                                              <th>Services</th>
                                              <th>Vendor</th>
                                             
                                          </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($completeorder as $orders)
                                        <tr>
                                            <td>{{ $orders['request_id']}}</td>
                                            <td class="middle">{{ $orders['asset_number']}}</td>
                                            <td>{{$orders['service_name']}}</td>
                                            <td>
                                               {{$orders['vendor_name']}}
                                            </td>
                                                           </tr>
                                        @endforeach
                                      </tbody>
                                 </table>
                               
                            </div>
                        </div>

                    </div>

                    <div class="row-fluid">

                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Workorders</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            
                              <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Req No</th>
                                              <th><i class="fa-icon-caret-up"></i> Property #</th>
                                              <th>Services</th>
                                              <th>Vendor</th>
                                             
                                          </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($list_complete_orders as $orders)
                                        <tr>
                                            <td>{{ $orders['request_id']}}</td>
                                            <td class="middle">{{ $orders['asset_number']}}</td>
                                            <td>{{$orders['service_name']}}</td>
                                            <td>
                                               {{$orders['vendor_name']}}
                                            </td>
                                            <td class="center"> <span class="label label-{{ $orders['status_class'] }}">{{ $orders['status_text'] }}</span> </td>
                                                           </tr>
                                        @endforeach
                                      </tbody>
                                 </table>
                               
                            </div>
                        </div>

                        <div class="box span6">
                            <div class="box-header">
                                <h2>Recent Properties</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-condensed">
                                      <thead>
                                          <tr>
                                              <th>Property #</th>
                                              <th>Property Address</th>
                                              <th>Loan #</th>
                                              <th>Status</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($assets as $assetData)
                                        <tr>
                                            <td>{{$assetData->asset_number}}</td>
                                            <td class="middle">{{$assetData->property_address}}</td>
                                            <td>{{$assetData->loan_number}}</td>
                                            <td class="center"> {{ isset($assetData->status) && $assetData->status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label">Inactive</span>' }}

                                        </tr>
                                        @endforeach
                                        
                                      </tbody>
                                 </table>
                              
                            </div>
                        </div>

                    </div>

				</div><!--/span-->

			</div><!--/row-->

			</div>
@stop