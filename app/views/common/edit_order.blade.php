@extends('layouts.default')
@section('content')
<!-- start: Content -->
<a class="btn btn-info" href="#" onclick="printDiv('content')" style="float:right"> Print  </a>

<div id="content" class="span11">

    <h1 class="text-center">Work Order</h1>

    <div class="row-fluid">
        <div class="box span12">
            <table class="table table-bordered customeTable"> 
                <tbody>
                    <tr>
                        <td class="center span3"><h2><span>Property #:</span>{{$order->maintenanceRequest->asset->asset_number}}</h2></td>
                        <td class="center span3"><h2><span>Order #:</span>{{$order->id}}</h2></td>
                        <td class="center span3"><h2><span>Recurring:</span> No</h2></td>
                        <td class="center span3"><h2><span>Status:</span> @if($order->status==1) In-Progress @else {{$order->status_text}} @endif</h2></td>
                        <td class="center span3"><h2><span>Due Date:</span> 25/6/2013</h2></td>
                        <td class="center text-center span3"><h2><button class="btn btn-small btn-success" data-target="#showAsset" data-toggle="modal">View Property</button></h2></td>
                    </tr>
                </tbody>
            </table>      
        </div><!--/span-->
    </div><!--/row-->
    <div class="row-fluid">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2>Customer Details</h2>
            </div>
            <div class="box-content">
                <table class="table"> 
                    <tbody>
                        <tr>
                            <td class="center span3"><h2>Customer First Name: <span class="green">{{$order->maintenanceRequest->user->first_name}}</span></h2></td>
                            <td class="center span3"><h2>Customer Last Name: <span class="green">{{$order->maintenanceRequest->user->last_name}}</span></h2></td>
                        </tr>
                        <tr>
                            <td class="center span3"><h2>Company: <span class="green">{{$order->maintenanceRequest->user->company}}</span></h2></td>
                            <td class="center span3"><h2>Email: <span class="green">{{$order->maintenanceRequest->user->email}}</span></h2></td>
                        </tr>
                    </tbody>
                </table>      
            </div>

        </div><!--/span-->
    </div><!--/row-->	

    <div class="row-fluid">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2>Property Details</h2>
            </div>
            <div class="box-content">
                <table class="table"> 
                    <tbody>
                        <tr>
                            <td class="center span3"><h2>Property #: <span class="green">{{$order->maintenanceRequest->asset->asset_number}}</span></h2></td>
                            <td class="center span3"><h2>Email: <span class="green">{{$order->maintenanceRequest->user->email}}</span></h2></td>
                        </tr>
                        <tr>
                            <td class="center span3"><h2>City: <span class="green">{{$order->maintenanceRequest->asset->city->name}} </span></h2></td>
                            <td class="center span3"><h2>State: <span class="green">{{$order->maintenanceRequest->asset->state->name}}</span></h2></td>
                        </tr>
                        <tr>
                            <td class="center span3"><h2>Zip: <span class="green"> {{$order->maintenanceRequest->asset->zip}}</span> </h2></td>
                            <td class="center span3"><h2>Loan #: <span class="green">{{$order->maintenanceRequest->asset->loan_number}}</span></h2></td>
                        </tr>
                    </tbody>
                </table>      
            </div>

        </div><!--/span-->
    </div><!--/row-->	
    <span><h1 class="text-center">Requested Services</h1></span>
    <?php //echo $before_image; die;

$totalPriceCustomer=0;
$totalPriceVendor=0;
$totalPrice=0;
     ?>
    @foreach($order_details as $order_detail)
    <div class="row-fluid">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>{{$order_detail->requestedService->service->title}}</h2>
                <div class="box-icon">
                <?php if( Auth::user()->type_id==3) {?>
                Price : ${{$order_detail->requestedService->service->vendor_price}}
                <?php
$totalPrice+=$order_detail->requestedService->service->vendor_price

                ?>
                <?php }else if( Auth::user()->type_id==2) {?>
           Price : ${{$order_detail->requestedService->service->customer_price}}
               <?php
$totalPrice+=$order_detail->requestedService->service->customer_price

                ?>
                <?php }
                else {?>
Customer Price:${{$order_detail->requestedService->service->customer_price}} Vendor Price:${{$order_detail->requestedService->service->vendor_price}}
 <?php
$totalPriceCustomer+=$order_detail->requestedService->service->customer_price;
$totalPriceVendor+=$order_detail->requestedService->service->vendor_price

     ?>
                <?php
                }
                ?>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                </div>
            </div>
            
            <div class="box-content">
                <div id="vendor-note-empty-error-{{$order_detail->id}}" class="hide">
                    <h4 class="alert alert-error">Vendor Note Can not be Empty</h4>
                </div>
                <div id="vendor-note-empty-success-{{$order_detail->id}}" class="hide">
                    <h4 class="alert alert-success">Saved Successful</h4>
                </div>
                <table class="table"> 
                    <tr>
                        <td colspan="2" class="center"><h2>Customer Note:</h2>{{$order_detail->requestedService->customer_note}}   {{$order_detail->id}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <span class="pull-left">
                            <button data-toggle="modal" data-backdrop="static" data-target="#before_{{$order_detail->id}}" class="btn btn-large btn-success">Upload Before Images</button>
                            <button data-toggle="modal" data-backdrop="static" data-target="#before_view_image_{{$order_detail->id}}" onclick="popModal({{$order->id}}, {{$order_detail->id}}, 'before')" class="btn">View Before Images</button>
                            </span>
                            <span class="pull-right">
                            <button data-toggle="modal" data-backdrop="static" data-target="#after_view_image_{{$order_detail->id}}" onclick="popModal({{$order->id}}, {{$order_detail->id}}, 'after')" class="btn">View After Images</button>
                            <button data-toggle="modal" data-backdrop="static" data-target="#after_{{$order_detail->id}}" class="btn btn-large btn-success">Upload After Images</button>
                            </span>
                        </td>
                    </tr>

                    <!-- <tr>
                      <td colspan="2" class="center"><label class="control-label" for="typeahead">Vendor Note:</label><textarea style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 139px;" rows="6" id="limit"></textarea></td>
                    </tr> -->
                    <tr>
                        <td colspan="2" class="center"><h2>Vendor Note:</h2>
                            @if($order_detail->requestedService->vendor_note)
                            <span id="show-vendor-note-{{$order->id}}-{{$order_detail->id}}">{{$order_detail->requestedService->vendor_note}}<br><button class="btn btn-primary" id="edit-vendor-note-button-{{$order->id}}-{{$order_detail->id}}" onclick="editVendorNoteButton({{$order->id}},{{$order_detail->id}})"> Edit Note </button> </span >
                            <span class="hide" id="textarea-vendor-note-{{$order->id}}-{{$order_detail->id}}">{{Form::textarea('vendor_note', $order_detail->requestedService->vendor_note ,array('class'=>'span','id'=>'vendor-note-'.$order->id.'-'.$order_detail->id))}}</span></td>
                            @else
                            <span id="show-vendor-note-{{$order->id}}-{{$order_detail->id}}"></span >
                            <span id="textarea-vendor-note-{{$order->id}}-{{$order_detail->id}}">{{Form::textarea('vendor_note','',array('class'=>'span','id'=>'vendor-note-'.$order->id.'-'.$order_detail->id))}}</span></td>
                            @endif
                    </tr>
                    <tr>
                        <td class="center" colspan="2"><button class="btn btn-large btn-warning pull-right" onclick="saveVendorNote({{$order->id}}, {{$order_detail->id}})">Save {{$order_detail->requestedService->service->title}}</button></td>

                    </tr>
                </table>      
            </div>
        </div><!--/span-->

    </div><!--/row-->
    
    
    
    <!--/   Modal-Section Start   -->
    <!--/   Modal-Section Add Before Images Start   -->
    <div style="padding: 10px;" class="modal hide fade modelForm"  id="before_{{$order_detail->id}}">
        <div class="row-fluid dragImage">
                {{ Form::open(array('url' => 'add-before-images', 'class'=>'dropzone', 'id'=>'form-before-'.$order_detail->id)) }}
                {{ Form::hidden('order_id', $order->id,array("id"=>"order_id_for_change"))}}
                {{ Form::hidden('order_details_id', $order_detail->id)}}
                {{ Form::hidden('type', 'before')}}
                <button class="btn btn-large btn-success" data-dismiss="modal">Save & Close</button> 
                {{ Form::close() }} 
        </div>
<!--        <div class="row-fluid">
                
        </div>-->
    </div>
    <!--/   Modal-Section Add Before Images End   -->
    
    <!--/   Modal-Section Add After Images Start   -->
    <div style="padding: 10px;" class="modal hide fade modelForm"  id="after_{{$order_detail->id}}">
        <div class="row-fluid dragImage">
                {{ Form::open(array('url' => 'add-after-images', 'class'=>'dropzone', 'id'=>'form-after-'.$order_detail->id)) }}
                {{ Form::hidden('order_id', $order->id)}}
                {{ Form::hidden('order_details_id', $order_detail->id)}}
                {{ Form::hidden('type', 'after')}}
                <button class="btn btn-large btn-success" data-dismiss="modal">Save & Close</button> 
                {{ Form::close() }} 
        </div>
    </div>
    <!--/   Modal-Section Add After Images End   -->
    
    
    <!--/   Modal-Section Show Before Images Start   -->
    <div style="padding: 10px;" role="dialog" class="modal hide fade modelForm"  id="before_view_image_{{$order_detail->id}}">
        <div class="well text-center"><h1>View Before Image</h1></div>
        <div class="row-fluid" id="before_view_modal_image_{{$order_detail->id}}">
        </div>
        <div class="row-fluid">
                <button data-dismiss="modal" class="btn btn-large btn-success">Close</button> 
        </div>
    </div>
    <!--/   Modal-Section Show Before Images End   -->
    
    
    <!--/   Modal-Section Show After Images Start   -->
    <div style="padding: 10px;" class="modal hide fade modelForm"  id="after_view_image_{{$order_detail->id}}">
        <div class="well text-center"><h1>View After Image</h1></div>
        <div class="row-fluid" id="after_view_modal_image_{{$order_detail->id}}">
        </div>
        <div class="row-fluid">
                <button data-dismiss="modal" class="btn btn-large btn-success">Close</button> 
        </div>
    </div>
    <!--/   Modal-Section Show After Images End   -->
    <!--/   Modal-Section End   -->
    
    @endforeach
       <?php if( Auth::user()->type_id==3 || Auth::user()->type_id==2) {?>
         <div style="float:right;"><h2>Total Price: ${{$totalPrice}} </h2>
    </div>  

        <?php }else { ?>
                
  <div style="float:right;"><h2>Total Customer Price: ${{$totalPriceCustomer}} Total Vendor Price: ${{$totalPriceVendor}} </h2>
    </div>  

    <?php } ?>
    <!--/   Asset Modal-Section Start   -->
    <div class="modal  hide fade modelForm larg-model"  id="showAsset">
        	<div class="modal-dialog">
        		<div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h1>Property Detail</h1>
                    </div>
                    <div class="modal-body">
                        
                        <div class="row-fluid">
                          <div class="box span12 noMarginTop">
                            <div class="custome-form assets-form">
                              <form class="form-horizontal">
                                <fieldset>
                                  <div class="row-fluid">
                                    <div class="span6">
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Property Number:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->asset_number}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Property Address:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->address}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">City: </label>
                                        <label class="control-label" for="typeahead"> {{$order->maintenanceRequest->asset->city->name}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">State:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->state->name}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Zip :</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->zip}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Lockbox </label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->lock_box}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Customer: </label>
                                        <label class="control-label" for="typeahead"> {{$order->maintenanceRequest->asset->user->first_name}} </label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Get / Access Code: </label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->access_code}} </label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Property Status: </label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->property_status}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Customer Email Adress: </label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->customer_email}} </label>
                                      </div>
                                    </div>
                                    <!--/span-6-->
                                    
                                    <div class="span6">
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Loan Number:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->loan_number}} </label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="selectError3">Property Type:</label>
                                        <label class="control-label" for="selectError3">{{$order->maintenanceRequest->asset->property_type}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="selectError3">County:</label>
                                        <label class="control-label" for="selectError3"> USA</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Company:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->user->company}}</label>
                                      </div>
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Lender : </label>
                                        <label class="control-label" for="typeahead">Lender</label>
                                      </div>
                                      <div class="control-group multiRadio row-sep">
                                        <label class="control-label">Property:</label>
                                        <label class="control-label">Occupied</label>
                                      </div>
                                      
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Agent Email Address:</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->agent}}</label>
                                      </div>
                                      
                                      <div class="control-group row-sep">
                                        <label class="control-label" for="typeahead">Send Carbon Copy Email To :</label>
                                        <label class="control-label" for="typeahead">{{$order->maintenanceRequest->asset->carbon_copy_email}}</label>
                                      </div>
                                      
                                      <div class="clearfix"></div>
                                    </div>
                                    <!--/span-6--> 
                                    
                                  </div>
                                  <div class="row-fluid">
                                  
                                  
                                  <br>
                                  <div class="control-group">
                                    <label class="control-label">Outbuilding / Shed *</label>
                                    @if($order->maintenanceRequest->asset->outbuilding_shed == 1)
                                    <label class="control-label">Yes</label>
                                    @else
                                    <label class="control-label">No</label>
                                    @endif
                                    <div style="clear:both"></div>
                                    <div class="control-group hidden-phone">
                                      <div class="controls">
                                        <label class="control-label" for="textarea2">Note:</label>
                                        @if($order->maintenanceRequest->asset->outbuilding_shed_note != '')
                                        <label class="control-label label-auto" for="textarea2">{{$order->maintenanceRequest->asset->outbuilding_shed_note}}</label>
                                        @else
                                        <label class="control-label label-auto" for="textarea2">There is no note regarding Outbuilding Shed</label>
                                        @endif
                                      </div>
                                    </div>
                                    <div class="control-group hidden-phone">
                                      <div class="controls">
                                        <label class="control-label" for="textarea2">Directions or Special Notes:</label>
                                        @if($order->maintenanceRequest->asset->outbuilding_shed_note != '')
                                        <label class="control-label label-auto" for="textarea2">{{$order->maintenanceRequest->asset->special_direction}}</label>
                                        @else
                                        <label class="control-label label-auto" for="textarea2">There is no Special Direction</label>
                                        @endif
                                      </div>
                                    </div>
                                  </div>
                                  <h4>Utility - On inside Property?</h4>
                                  <div class="control-group">
                                  <label class="control-label">Electric :</label>
                                  @if($order->maintenanceRequest->asset->electric_status == 1)
                                  <label class="control-label">Yes</label>
                                  @else
                                  <label class="control-label">No</label>
                                  @endif
                                  <div style="clear:both"></div>
                                  <div class="control-group hidden-phone">
                                    <div class="controls">
                                      <label class="control-label label-auto" for="textarea2">This note over here</label>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                  <label class="control-label">Water *</label>
                                  <div class="controls">
                                    <label class="radio">
                                      <input type="radio" name="pool" id="optionsRadios1" value="yes" checked="">
                                      Yes </label>
                                    <label class="radio property-pool">
                                      <input type="radio" name="pool" id="optionsRadios2" value="no">
                                      No </label>
                                  </div>
                                  <div class="control-group hidden-phone">
                                    <div class="controls">
                                      <label class="control-label" for="textarea2">Notes:</label>
                                      <label class="control-label label-auto" for="textarea2">This is directions or special notes</label>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Gas *</label>
                                    <div class="controls">
                                      <label class="radio">
                                        <input type="radio" name="pool" id="optionsRadios1" value="yes" checked="">
                                        Yes </label>
                                      <label class="radio property-pool">
                                        <input type="radio" name="pool" id="optionsRadios2" value="no">
                                        No </label>
                                    </div>
                                    <div class="control-group hidden-phone">
                                      <div class="controls">
                                        <label class="control-label" for="textarea2">Notes:</label>
                                        <label class="control-label label-auto" for="textarea2">This is directions or special notes</label>
                                      </div>
                                    </div>
                                    <div class="control-group multiRadio">
                                      <label class="control-label">Swimming *</label>
                                      <div class="controls">
                                        <label class="radio">
                                          <input type="radio" name="Swimming" id="" value="yes" checked="">
                                          Poo </label>
                                        <label class="radio property-pool">
                                          <input type="radio" name="Swimming" id="" value="no">
                                          Spa </label>
                                        <label class="radio">
                                          <input type="radio" name="Swimming" id="" value="yes" checked="">
                                          Koi Pond </label>
                                        <label class="radio property-pool">
                                          <input type="radio" name="Swimming" id="" value="no">
                                          Pond </label>
                                      </div>
                                    </div>
                                    
                                  </div>
                                </fieldset>
                              </form>
                            </div>
                          </div>
                          <!--/span--> 
                          
                        </div>
                        <!--/row-->
                        
                    </div>
                    <div class="modal-footer">
                        <div class="text-right">
                          <button type="button" class="btn btn-large btn-inverse" data-dismiss="modal">Close</button>
                        </div>
                    </div>
             	</div>
             </div>   
        </div>
    <!--/   Asset Modal-Section End   -->
    <div class="row-fluid">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Order Status</h2>
            </div>
            <div class="box-content">

                <tr>

<p id="errorMessage" style="display:none"></p>
                <div class="btn-group">
                    <button class="btn btn-large orderstatus">@if($order->status==1) In-Progress @else {{$order->status_text}} @endif</button>
                    <button class="btn btn-large dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu mystatusclass">
                        <li><a href="#">In-Progress</a></li>
                        <li><a href="#">Completed</a></li>
                       @if(Auth::user()->type_id==1||Auth::user()->type_id==2)
                        <li><a href="#">Approved</a></li>
                        @endif
                       
                    </ul>
                </div>
						

                </tr>
                <tr>
                    <td class="center" colspan="2"></td>
                </tr>      
            </div>
        </div><!--/span-->

    </div><!--/row-->

    

</div>
<!-- end: Content -->

<!--Upload Before Image-->

<!--/Upload Before Image-->
@stop
