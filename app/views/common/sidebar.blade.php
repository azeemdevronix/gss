<div class="nav-collapse sidebar-nav">
    <ul class="nav nav-tabs nav-stacked main-menu">

        <li> <a class="submenu" href="{{URL::to('admin')}}"><i class="fa-icon-user"></i><span class="hidden-tablet">Home</span></a>
        </li>
         <!-- @if($access_roles['User']['view'] == 1)
        <li> <a href="{{URL::to('list-user')}}"><i class="fa-icon-user"></i><span class="hidden-tablet">Apply</span></a>
            
        </li>
 		@endif
         @if($access_roles['Access Level']['view'] == 1)
        <li> <a  href="{{URL::to('list-access-level')}}"><i class="fa-icon-user-md"></i><span class="hidden-tablet">Acces Levels</span></a>
            
        </li>
         @endif
         @if($access_roles['Access Rights']['view'] == 1)
        <li> <a href="{{URL::to('access-rights')}}"><i class="fa-icon-lock"></i><span class="hidden-tablet">Access Rights</span></a>
        </li>
        @endif -->
      <!--   @if($access_roles['Customer']['view'] == 1)
        <li> <a href="{{URL::to('list-customer')}}"><i class="fa-icon-user"></i><span class="hidden-tablet">Technicians</span></a>
           
        </li>
        @endif -->
         @if($access_roles['Vendor']['view'] == 1)
        <li> <a href="{{URL::to('Technicians')}}"><i class="fa-icon-user"></i><span class="hidden-tablet">Technicians</span></a>
           
        </li>
        @endif
       <!-- @if($access_roles['Asset']['view'] == 1)
        <li> <a  href="{{URL::to('list-assets')}}"><i class="fa-icon-home"></i><span class="hidden-tablet">Properties</span></a>
        
        </li>
        @endif -->
        <!-- @if($access_roles['Service']['view'] == 1)
        <li> <a href="{{URL::to('list-services')}}"><i class="fa-icon-home"></i><span class="hidden-tablet">Orders</span></a>
    
        </li>
        @endif -->
      <!--   @if($access_roles['Special Price']['view'] == 1)
        <li> <a  href="{{URL::to('list-special-prices')}}"><i class="fa-icon-money"></i><span class="hidden-tablet">Special Price</span></a>

        </li>
        @endif -->
        @if($access_roles['Maintenance Request']['view'] == 1)
        <li> <a href="{{URL::to('list-maintenance-request')}}"><i class="fa-icon-wrench"></i><span class="hidden-tablet">Claim Request</span></a>
        
        </li>
        @endif
        <!-- @if($access_roles['Maintenance Request']['view'] == 1)
        <li> <a href="{{URL::to('list-assigned-maintenance-request')}}"><i class="fa-icon-home"></i><span class="hidden-tablet">Services</span></a>
    
        </li>
        @endif -->
        @if($access_roles['Order']['view'] == 1)
        <li> <a href="{{URL::to('list-work-order-admin')}}"><i class="fa-icon-retweet"></i><span class="hidden-tablet">Orders</span></a>
            
        </li>
        @endif
        @if($access_roles['Completed Request']['view'] == 1)
        <li> <a href="{{URL::to('admin-list-completed-order')}}"><i class="fa-icon-tasks"></i><span class="hidden-tablet">Completed claim request</span></a>
        
        </li>
        @endif
        @if($access_roles['Invoice']['view'] == 1)
        <li> <a  href="{{URL::to('admin-list-invoice')}}"><i class="fa-icon-money"></i><span class="hidden-tablet">Invoices</span></a>
            
        </li>
        @endif
<!-- 
 -->
<!--         <li> <a  href="{{URL::to('admin-bid-requests')}}"><i class="fa-icon-money"></i><span class="hidden-tablet">Bid Requests</span></a>
            
        </li>
         -->
    </ul>
</div>
