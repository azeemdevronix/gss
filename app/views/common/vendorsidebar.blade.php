<div class="nav-collapse sidebar-nav">
    <ul class="nav nav-tabs nav-stacked main-menu">
        <li><a id="submenuVendorDashboard" class="submenu" href="{{URL::to('vendors')}}"><i class="fa-icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>

        <li>
            <a href="{{URL::to('vendor-assigned-requests')}}"><i class="fa-icon-wrench"></i><span class="hidden-tablet">Service Request</span></a>
            
        </li>


        <li>
            <a  href="{{URL::to('vendor-list-orders')}}"><i class="fa-icon-file"></i><span class="hidden-tablet"> Work Order</span></a>
           
        </li>



        <li>
            <a  href="{{URL::to('vendor-list-completed-orders')}}"><i class="fa-icon-file"></i><span class="hidden-tablet">Completed Orders</span></a>
            
        </li>

 <li> <a  href="{{URL::to('vendor-list-invoice')}}"><i class="fa-icon-money"></i><span class="hidden-tablet">Invoice</span></a>
            
        </li>
        <li> <a href="{{URL::to('vendor-bid-requests')}}"><i class="fa-icon-money"></i><span class="hidden-tablet">Bid Requests</span></a>
            
        </li>
    </ul>
</div>
