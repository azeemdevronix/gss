@include('common.head')
@section('content')
<body>

    <div class="container-fluid">
     <div class="wrapper">
        <!-- header -->
        <header class="header">
            <div class="shell">
                <div class="header-top">
                    <h1 id="logo"><img src="public/assets/css/images/logo.png" width="292" height="100">></h1>
                    <nav id="navigation">
                        <a href="#" class="nav-btn">Home<span></span></a>
                        <ul>
                            <li><a href="#">About US</a></li>
                            <li><a href="#">Tech Opportunities</a></li>
                            <li><a href="#">Contacts</a></li>
                        </ul>
                    </nav>
                    <div class="cl">&nbsp;</div>
                </div>
                <div class="slider">
                    <div id="bg"></div>
                    <div id="carousel">
                        <div>
                            <h5>Need Diagnostics Report for your Insurance of</h5>
                            <h3>Home Theater</h3>
                            <p>DI-Report Quality Technicians are one call away from you.</p>
                            <a href="{{URL::to('order_report')}}" class="green-btn">Order Claim Now</a>
                            <img class="img-front" src="public/assets/css/images/hometh.png" alt="dot3" width="500" height="304" />
                            <img class="img-back" src="public/assets/css/images/3rd.png" alt="dot3" width="300" height="304" />
                
                        </div>
                        <div>
                            <h5>Need Diagnostics Report for your Insurance of</h5>
                            <h3>iMac?</h3>
                            <p>DI-Report Quality Technicians are one call away from you.</p>
                            <a href="#" class="green-btn">Order Claim Now</a>
                            
                            <img class="img-back" src="public/assets/css/images/mac222.png" alt="dot3" width="300" height="304" />
                
                        </div>
                    
                    </div>
                    <div class="pagination"></div>
                    <a id="prev" href="#"></a>
                    <a id="next" href="#"></a>
                </div>
            </div>
        </header>
        <!-- end of header -->
        <!-- shell -->
        <div class="shell">
            <!-- main -->
            <div class="main">
                <!-- cols -->
                <section class="cols">

                    <div class="col">
                        <img src="public/assets/css/images/col-img1.png" alt="" />
                        <div class="col-cnt">
                            <h2>TV,Electronics And Home Theater</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing dolor emor</p>
                            <a href="#" class="more">view more</a>
                        </div>
                    </div>
                    <div class="col">
                        <img src="public/assets/css/images/col-img2.png" alt="" />
                        <div class="col-cnt">
                            <h2>Computer And Related Peripherals</h2>
                            <p>Duis risus elit, imperdiet eget sollicitudin quis, gravida sed mi. </p>
                            <a href="#" class="more">view more</a>
                        </div>
                    </div>
                    <div class="col">
                        <img src="public/assets/css/images/col-img3.png" alt="" />
                        <div class="col-cnt">
                            <h2>Appliances, Pumps, Heating & A/C </h2>
                            <p>Proin quis sem in mauris fringilla commodo ac a felis.</p>
                            <a href="#" class="more">view more</a>
                        </div>
                    </div>
                    <div class="cl">&nbsp;</div>

                </section>
                <!-- end of cols -->
               <!--  <section class="post">
                    <img src="public/assets/css/images/post-img.png" alt="" />
                    <div class="post-cnt">
                        <h2>Yes, that would be the logical choice</h2>
                        <p>
                            <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit adispicing amet </strong><br />
                            Cras molestie condimentum consequat. Nam leo libero, scelerisque tincidunt amet nsectetur adipiscing elit. Cras molestie condimentum nsectetur adipiscing elit. Cras molestie condimentum consequat pretium donec Duis risus elit, imperdiet eget sollicitudin quis, <strong>some of the features:</strong>
                        </p>

                        <ul>
                            <li><a href="#">jQuery Carousel Slideshow</a></li>
                            <li><a href="#">Various Column Options</a></li>
                            <li><a href="#">Valid XHTML and CSS Tableless Design</a></li>
                            <li><a href="#">Tested on Mac &amp; PC</a></li>
                        </ul>
                    </div>
                    <div class="cl">&nbsp;</div>
                </section> -->

                <section class="content">
                    <h2>WHO WE ARE</h2>
                    <p>Maecenas vel est sit amet massa dignissim <span>hendrerit</span> sit amet ac ante. Praesent dapibus neque vel enim adipiscing vel feugiat leo luctus. Nulla semper varius nulla, eu euismod turpis volutpat in. Nullam neque diam, facilisis vitae suscipit eu, gravida ut tortor. Duis faucibus lectus nec metus elementum ac <a href="#">adipiscing</a> lorem consectetur. Nulla dui ipsum, <em>iaculis a mollis vel</em>, sodales a ipsum. Nam tincidunt consequat lacus eget imperdiet. Nullam posuere convallis libero, vitae lobortis libero fermentum eu. Ut id venenatis lectus. <strong>Donec eget</strong> quam et dolor facilisis commodo nullam tempor aliquet vulputate phasellus ac libero sed justo luctus vehicula.<br /> <a href="#" class="more">meet the team</a></p>
                </section>

                <section class="partners">
                    
                    <div id="partners-slider">
                        <div class="slider-holder2">
                            <img src="public/assets/css/images/partners1.png" width="92" height="52" />
                            <img src="public/assets/css/images/partners2.png" width="172" height="52" />
                            <img src="public/assets/css/images/partners3.png" width="236" height="52" />
                            <img src="public/assets/css/images/partners4.png" width="121" height="52" />
                            <img src="public/assets/css/images/partners1.png" width="92" height="52" />
                            <img src="public/assets/css/images/partners2.png" width="172" height="52" />
                            <img src="public/assets/css/images/partners3.png" width="236" height="52" />
                            <img src="public/assets/css/images/partners4.png" width="121" height="52" />
                        </div>
                    </div>
                    <div class="slider-arr">
                        <a class="prev-arr arr-btn" href="#"></a>
                        <a class="next-arr arr-btn" href="#"></a>
                    </div>
                </section>

                <div class="socials">
                    <p>We are currently <strong>available</strong> for work. Please, contact us for a quote at <span><a href="#">contact [at] websitename [dot] com</a></span></p>

                    <ul>
                        <li><a href="#" class="facebook-ico">facebook-ico</a></li>
                        <li><a href="#" class="twitter-ico">twitter-ico</a></li>
                        <li><a href="#" class="skype-ico">skype-ico</a></li>
                        <li><a href="#" class="rss-ico">rss-ico</a></li>
                    </ul>
                </div>
            </div>
            <!-- end of main -->
        </div>
        <!-- end of shell -->   
        <!-- footer -->
        <div id="footer">
            <!-- shell -->
            <div class="shell">
                <!-- footer-cols -->
            
                <!-- end of footer-cols -->
                <div class="footer-bottom">
                    <div class="footer-nav">
                        <ul>
                            <li><a hrerf="#">Home</a></li>
                            <li><a hrerf="#">Services</a></li>
                            <li><a hrerf="#">Projects</a></li>
                            <li><a hrerf="#">Solutions</a></li>
                            <li><a hrerf="#">Jobs</a></li>
                            <li><a hrerf="#">Blog</a></li>
                            <li><a hrerf="#">Contacts</a></li>
                        </ul>
                    </div>
                    <p class="copy">Copyright &copy; 2012<span>|</span>Design by: <a href="http://chocotemplates.com" target="_blank">@@@@@</a></p>
                    <div class="cl">&nbsp;</div>
                </div>
            </div>
            <!-- end of shell -->
        </div>
        
        <!-- footer -->
    </div>
    <!-- end of wrapper -->
 @include('common.footerbottom')
  