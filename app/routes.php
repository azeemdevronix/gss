<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
// Home Routs
 // Route::get('/', function()
 //        {
 //            return View::make('home');
 //        });

 Route::get('/', array('uses' => 'HomeController@showWelcome'));
 Route::get('Technicians', array('uses' => 'AdminController@Technicians'));
 Route::get('show-add-Technicians', array('uses' => 'AdminController@addTechnicians'));
 Route::get('order_report', array('uses' => 'AdminController@OrderReport'));
 Route::get('Search', array('uses' => 'AdminController@search'));

   Route::match(array('GET', 'POST'), 'add-order-reports', array('uses' => 'AdminController@addOrderReport'));


 Route::get('login', array('uses' => 'UserController@showLogin'));
 Route::get('active-customer/{id}', array('uses' => 'CustomerController@activeCustomer'));
 Route::get('active-user/{id}', array('uses' => 'UserController@activeUser'));
 Route::match(array('GET', 'POST'), 'get-asset-map', array('uses' => 'AssetController@getAssetMap'));
   

// Common Routes

 Route::get('logout', function() {
    Auth::logout();
    return View::make('home');
 });


 #########################################################  Admin Routes ####################################################3
 Route::group(array('before' => 'auth|adminCheck|adminRightsCheck'), function() {
    // Login Secure Admin Routes
    Route::get('admin', array('uses' => 'AdminController@index'));
    Route::get('access-rights', array('uses' => 'AccessRightController@index'));
    Route::match(array('GET', 'POST'), 'add-asset', array('uses' => 'AssetController@addAdminAsset'));
    Route::match(array('GET', 'POST'), 'add-service', array('uses' => 'ServiceController@addAdminService'));
    Route::match(array('GET', 'POST'), 'list-assets', array('uses' => 'AssetController@listAdminAssets'));
    Route::match(array('GET', 'POST'), 'list-services', array('uses' => 'ServiceController@listAdminServices'));
    Route::match(array('GET', 'POST'), 'add-special-prices', array('uses' => 'SpecialPriceController@addSpecialPrice'));
    Route::match(array('GET', 'POST'), 'list-special-prices', array('uses' => 'SpecialPriceController@listSpecialPrice'));
    Route::match(array('GET', 'POST'), 'edit-special-price/{special_price_id}', array('uses' => 'SpecialPriceController@editSpecialPrice'));
    Route::match(array('GET', 'POST'), 'edit-service/{service_id}', array('uses' => 'ServiceController@updateAdminService'));
    Route::match(array('GET', 'POST'), 'edit-asset/{asset_id}', array('uses' => 'AssetController@editAdminAsset'));
    // Admin Routes
    Route::match(array('GET', 'POST'), 'list-user', array('uses' => 'AdminController@listUser'));
    Route::match(array('GET', 'POST'), 'add-access-level', array('uses' => 'AdminController@addAccessLevel'));
    Route::get('add-user', array('uses' => 'AdminController@addUser'));
    Route::post('addUser', array('uses' => 'AdminController@addNewUser'));
    Route::match(array('GET', 'POST'), 'edit-user/{user_id}', array('uses' => 'AdminController@editUser'));
    Route::get('list-access-level', array('uses' => 'AdminController@listAccessLevel'));
    // Route::get('show-add-Technicians', array('uses' => 'AdminController@addTechnicians'));
    Route::post('process-add-vendor', array('uses' => 'AdminController@processAddVendor'));
    Route::match(array('GET', 'POST'), 'list-maintenance-request', array('uses' => 'AdminController@listMaintenanceRequest'));
        Route::match(array('GET', 'POST'), 'list-assigned-maintenance-request', array('uses' => 'AdminController@listAssignedMaintenanceRequest'));
    Route::match(array('GET', 'POST'), 'view-maintenance-request/{id}', array('uses' => 'AdminController@viewMaintenanceRequest'));

    Route::match(array('GET', 'POST'), 'add-new-customer', array('uses' => 'CustomerController@createCustomerAdmin'));
    Route::get('list-customer', array('uses' => 'CustomerController@listCustomerAdmin'));
    Route::match(array('GET', 'POST'), 'edit-customer/{id}', array('uses' => 'CustomerController@editCustomerAdmin'));
    //Work Order Routes
    Route::get('list-work-order-admin', array('uses' => 'AdminController@listWorkOrder'));
    Route::get('admin-list-completed-order', array('uses' => 'AdminController@listCompletedOrders'));
    Route::get('admin-list-invoice', array('uses' => 'InvoiceController@listAdminInvoices'));
      
    Route::get('admin-list-invoice/{id}', array('uses' => 'InvoiceController@listAdminInvoices'));
     

//    Route::get('list-completed-order-admin', array('uses' => 'AdminController@listCompletedOrders'));
    //WorkOrder Routes



    Route::get('edit-profile-admin/{id}', array('uses' => 'AdminController@editProfileAdmin'));

Route::post('save-profile-admin/{id}', array('uses' => 'AdminController@saveUserProfile'));
Route::match(array('GET', 'POST'), 'assign-service-request', array('uses' => 'MaintenanceRequestController@assignServiceRequest'));
Route::match(array('GET', 'POST'), 'edit-access-level/{role_id}', array('uses' => 'AccessLevelController@editAccessLevel'));
Route::match(array('GET', 'POST'), 'delete-user/{user_id}', array('uses' => 'AdminController@deleteUser'));
Route::get('admin-add-bid-request', array('uses' => 'AdminController@addBidRequest'));

Route::get('admin-bid-requests', array('uses' => 'AdminController@listBidRequests'));

Route::get('admin-bid-requests/{id}', array('uses' => 'AdminController@listBidRequests'));
 Route::get('admin-approved-bid-requests', array('uses' => 'AdminController@listApprovedBidRequests'));

 Route::get('admin-declined-bid-requests', array('uses' => 'AdminController@listDeclinedBidRequests'));
 Route::post('admin-create-bid-service-request', array('uses' => 'AdminController@createBidServiceRequest'));
 Route::match(array('GET', 'POST'), 'view-admin-bid-request/{id}', array('uses' => 'AdminController@viewBidRequest'));
 
 Route::match(array('GET', 'POST'), 'admin-accept-bid-request/', array('uses' => 'AdminController@acceptBidRequest'));
 Route::match(array('GET', 'POST'), 'admin-decline-bid-request/', array('uses' => 'AdminController@DeclineBidRequest'));
 
Route::get('admin-add-new-service-request', array('uses' => 'MaintenanceRequestController@viewAdminRequestForm'));

});


/* Maintenance Request */
Route::post('create-service-request', array('uses' => 'MaintenanceRequestController@createServiceRequest'));
Route::post('create-bid-service-request', array('uses' => 'VendorController@createBidServiceRequest'));



Route::get('list-customer-requested-services', array('uses' => 'MaintenanceRequestController@listServiceRequest'));

Route::get('view-customer-request-service/{id}', array('uses' => 'MaintenanceRequestController@viewServiceRequest'));
/* Asset Routes */
Route::get('add-customer-asset', array('uses' => 'AssetController@showAddAsset'));
Route::post('create-customer-asset', array('uses' => 'AssetController@createAsset'));
Route::get('add-new-customer-asset', array('uses' => 'AssetController@showAddAssetNew'));
Route::post('edit-customer-asset/{id}', array('uses' => 'AssetController@editAsset'));
Route::get('edit-customer-asset/{id}', array('uses' => 'AssetController@editAsset'));


############################################# Admin Routes for ajax call ######################################
Route::post('update-access-level', array('uses' => 'AccessLevelController@updateUserAccessLevel'));
Route::post('update-access-rights', array('uses' => 'AccessRightController@updateAccessRights'));
Route::post('get-role-details', array('uses' => 'AccessRightController@getRoleDetails'));
Route::post('update-user-status/{user_id}', array('uses' => 'AdminController@updateUserStatus'));

Route::match(array('GET', 'POST'), 'delete-vendor/{vendor_id}', array('uses' => 'AdminController@deleteVendor'));
Route::match(array('GET', 'POST'), 'delete-access-level/{role_id}', array('uses' => 'AccessLevelController@deleteAccessLevel'));


Route::match(array('GET', 'POST'), 'asset-view/{id}', array('uses' => 'AdminController@assetView'));
Route::match(array('GET', 'POST'), 'show-maintenance-services/{id}', array('uses' => 'AdminController@showMaintenanceServices'));


Route::group(array('before' => 'auth|customerCheck'), function() {
    Route::get('customer', array('uses' => 'CustomerController@index'));
    Route::get('view-customer-assets', array('uses' => 'AssetController@viewAssetsList'));
    Route::post('create-customer-new-asset', array('uses' => 'AssetController@createAssetNew'));
    Route::get('customer-profile-complete', array('uses' => 'CustomerController@showCompleteProfile'));
    Route::post('customer-profile-add', array('uses' => 'CustomerController@completeProfile'));
    Route::get('view-assets-list', array('uses' => 'AssetController@viewAssetsList'));
    Route::get('delete-customer-asset/{id}', array('uses' => 'AssetController@deleteAsset'));
    Route::get('add-new-service-request', array('uses' => 'MaintenanceRequestController@viewRequestForm'));
     Route::get('customer-list-work-orders', array('uses' => 'CustomerController@listWorkOrder'));
     Route::get('customer-list-completed-work-orders', array('uses'=>'CustomerController@listCompletedWorkOrder'));

     Route::get('customer-approval-completion-request', array('uses' => 'CustomerController@listApprovalCompletion'));

     Route::get('customer-process-work-order', array('uses' => 'CustomerController@listProcessWorkOrder'));
     Route::get('customer-bid-requests', array('uses' => 'CustomerController@listBidRequests'));
    Route::get('customer-list-invoice', array('uses' => 'InvoiceController@listCustomerInvoices'));
    
    Route::get('customer-bid-requests/{id}', array('uses' => 'CustomerController@listBidRequests'));
    Route::get('customer-approved-bid-requests', array('uses' => 'CustomerController@listApprovedBidRequests'));
    Route::get('customer-declined-bid-requests', array('uses' => 'CustomerController@listDeclinedBidRequests'));
    Route::match(array('GET', 'POST'), 'view-customer-bid-request/{id}', array('uses' => 'CustomerController@viewBidRequest'));
    Route::match(array('GET', 'POST'), 'accept-bid-request/', array('uses' => 'CustomerController@acceptBidRequest'));
    Route::match(array('GET', 'POST'), 'decline-bid-request/', array('uses' => 'CustomerController@DeclineBidRequest'));
});




//Ajax Customer Routes
Route::post('get-cities-by-state-id', array('uses' => 'AjaxController@getCitiesByState'));
Route::post('ajax-get-asset-by-asset-id', array('uses' => 'AjaxController@getAssetById'));
Route::post('ajax-service-information-popup', array('uses' => 'AjaxController@getServicePopup'));
Route::post('ajax-vendor-service-information-popup', array('uses' => 'AjaxController@getVendorServicePopup'));
Route::post('ajax-service-information-list', array('uses' => 'AjaxController@getServiceList'));
Route::post('remove-file-from-directory', array('uses' => 'AjaxController@removeFile'));
//Ajax Route end
################################## Vendor Routes #############################################
Route::group(array('before' => 'auth|vendorCheck'), function() {
    Route::get('vendors', array('uses' => 'VendorController@index'));
    Route::get('vendor-profile-complete', array('uses' => 'VendorController@showCompleteProfile'));
    Route::post('vendor-profile-add', array('uses' => 'VendorController@completeProfile'));
    Route::get('vendor-profile-service', array('uses' => 'ServiceController@index'));
    Route::post('vendor-service-complete', array('uses' => 'ServiceController@assignVendorService'));
    Route::get('vendor-assigned-requests', array('uses' => 'VendorController@listAssignedRequests'));
    Route::get('vendor-list-orders', array('uses' => 'VendorController@listOrders'));
    Route::get('vendor-list-completed-orders', array('uses' => 'VendorController@listCompletedOrders'));
    Route::match(array('GET', 'POST'), 'view-vendor-maintenance-request/{id}', array('uses' => 'VendorController@viewMaintenanceRequest'));
    Route::match(array('GET', 'POST'), 'decline-request/', array('uses' => 'VendorController@declineRequest'));
    Route::match(array('GET', 'POST'), 'accept-request/', array('uses' => 'VendorController@acceptRequest'));
        Route::match(array('GET', 'POST'), 'accept-single-request/', array('uses' => 'VendorController@acceptSingleRequest'));
    Route::get('add-bid-request', array('uses' => 'VendorController@addBidRequest'));
    Route::get('vendor-bid-requests', array('uses' => 'VendorController@listBidRequests'));
    
    Route::get('vendor-bid-requests/{id}', array('uses' => 'VendorController@listBidRequests'));
    Route::get('vendor-list-invoice', array('uses' => 'InvoiceController@listVendorInvoices'));
    
    Route::get('vendor-approved-bid-requests', array('uses' => 'VendorController@listApprovedBidRequests'));
    Route::get('vendor-declined-bid-requests', array('uses' => 'VendorController@listDeclinedBidRequests'));
  
    });



################################## Other Routes #############################################

Route::post('delete-order-before-image', array('uses' => 'OrderController@deleteBeforeImages'));
Route::post('delete-before-image-id', array('uses' => 'OrderController@deleteImageById'));
Route::post('save-vendor-note', array('uses' => 'OrderController@saveVendorNote'));
Route::post('delete-order-after-image', array('uses' => 'OrderController@deleteAfterImages'));
Route::post('delete-order-all-before-image', array('uses' => 'OrderController@deleteOrderAllBeforeImages'));
Route::post('add-before-images', array('uses' => 'OrderController@addBeforeImages'));
Route::post('add-after-images', array('uses' => 'OrderController@addAfterImages'));
Route::post('display-order-images', array('uses' => 'OrderController@displayImages'));
Route::post('display-order-images-view', array('uses' => 'OrderController@displayViewImages'));
Route::match(array('GET', 'POST'), 'edit-order/{order_id}', array('uses' => 'OrderController@editOrder'));
Route::get('view-order/{order_id}', array('uses' => 'OrderController@viewOrder'));
Route::match(array('GET', 'POST'), 'change-status', array('uses' => 'OrderController@changeStatus'));
Route::match(array('GET', 'POST'), 'change-notification-status', array('uses' => 'NotificationController@ChangeNotificationStatus'));


Route::post('generate-asset-number', array('uses' => 'AjaxController@generateAssetNumber'));
Route::post('user-create', array('uses' => 'UserController@createUser'));
Route::get('register-page-redirect', array('uses' => 'UserController@whereRedirect'));
//Ajax Customer Routes
Route::post('get-cities-by-state-id', array('uses' => 'AjaxController@getCitiesByState'));
Route::post('ajax-get-asset-by-asset-id', array('uses' => 'AjaxController@getAssetById'));
Route::post('ajax-service-information-popup', array('uses' => 'AjaxController@getServicePopup'));
Route::post('ajax-service-information-list', array('uses' => 'AjaxController@getServiceList'));
Route::post('remove-file-from-directory', array('uses' => 'AjaxController@removeFile'));
//Ajax Route end
Route::post('login', array('uses' => 'UserController@doLogin'));
Route::get('edit-profile', array('uses' => 'UserController@editProfile'));
Route::post('save-profile', array('uses' => 'UserController@saveProfile'));

Route::get('thankyou/{user_id}', array('uses' => 'UserController@showThankYou'));
Route::match(array('GET', 'POST'), 'update-status', array('uses' => 'CommonController@updateStatus'));
Route::match(array('GET', 'POST'), 'delete-record', array('uses' => 'CommonController@deleteRecord'));
Route::get('testing-link', array('uses' => 'AjaxController@testingLink'));



Route::group(array('before' => 'loginCheck'), function() {
    Route::get('user-register', array('uses' => 'UserController@showRegistration'));
});



Route::match(array('POST', 'GET'), 'forgot-password', array('uses' => 'RemindersController@getRemind'));
Route::controller('password', 'RemindersController');


Route::get('qb', array('uses' => 'QuickBookController@index'));
Route::get('cron-job', array('uses' => 'CronJobController@index'));
