<?php

class Service extends Eloquent {

    protected $table = 'services';
	protected $fillable = array('id', 'title', 'customer_price', 'vendor_price', 'service_code', 'req_date',
	 'number_of_men','verified_vacancy','cash_for_keys','cash_for_keys_trash_out','trash_size','storage_shed','lot_size','recurring','emergency','desc','status'
,'set_prinkler_system_type','install_temporary_system_type',	'carpet_service_type',	'pool_service_type',	'boarding_type','spruce_up_type','constable_information_type',
'remove_carpe_type','remove_blinds_type','remove_appliances_type','due_date');
    // Defining Function to get User Profile Status. ---- Start
    public static function getAllServices() {
        $service = Self::all();
        return $service; //return user type
    }

    public function requestedService() {
        return $this->hasMany('RequestedService', 'service_id');
        //return user type
    }
    
    
    public function specialPrice() {
        return $this->hasMany('SpecialPrice', 'service_id');
        //return user type
    }
    
     public function orderDetail() {
        return $this->hasMany('OrderDetail', 'service_id');
        //return user type
    }
		
	public static function addAdminService($data)
	{
		$data['status'] = 1;
		$save = Self::create($data);		
		return ($save) ? $save : false; 
	}
	
	public static function updateAdminService($data, $id)
	{
		
		$nulldata=array('service_code' =>'' ,
			'title' => '',
			'customer_price' =>  '',
			'vendor_price' => '',
			'desc' => '',
			'recurring' => '',
			'emergency' => '' ,
			'req_date' => '',
			'number_of_men' =>'' ,
			'verified_vacancy' =>'' ,
			'cash_for_keys' =>'' ,
			'cash_for_keys_trash_out' =>'' ,
			'trash_size' => '',
			'storage_shed' =>''  ,
			'lot_size' => '',
			'set_prinkler_system_type' =>'' ,
			'install_temporary_system_type' =>'' ,
			'carpet_service_type' =>'' ,
			'pool_service_type' =>'' ,
			'boarding_type' => '',
			'spruce_up_type' =>'', 
			'constable_information_type' =>'',
			'remove_carpe_type' =>'',
			'remove_blinds_type' =>'',  
			'remove_appliances_type'=>''

			);
		Self::find($id)->update($nulldata);
		
		$save = Self::find($id)->update($data);		
		return ($save) ? $save : false; 
	}
	
	public static function getServices()
	{
	  	$services = Self::orderBy('id', 'desc')->get();
	  	return $services;
	}
	
	public static function getServiceById($id)
	{
	  	$service = Self::find($id);
	  	return $service;
	}
	
	public function getTable()
	{
		return $this->table;
	}
        
        public static function addServicePrice($data, $id)
	{
            $save = Self::find($id)->update($data);
            return $save;
	}
    // Defining Function to get User Profile Status. ---- End
}