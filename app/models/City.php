<?php

class City extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function state(){
       return $this->belongsTo('State','state_id');
    }
    
    public function user(){
       return $this->hasMany('User','city_id');
    }
    
    public static function getCitiesByStateId($id){
       $cities = Self::whereRaw('state_id = ?', array($id))->get(array('id','name'));
       return $cities;
    }
    public static function getAllCities(){
        $cities = Self::all();
         return $cities;
    }
}
