<?php

class State extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function city(){
       return $this->hasMany('City','state_id');
    }
  
    public static function getAllStates(){
        
        $states = Self::all();
        return $states;
    }
}
