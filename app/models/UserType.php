<?php

class UserType extends Eloquent {

    protected $table = 'user_types';

    // Defining Function to get User Profile Status. ---- Start
    public static function getUserTypeByID($id) {
        $user_type = Self::find($id);
        return  $user_type->title; //return user type
    }

    // Defining Function to get User Profile Status. ---- End

    public function user() {
        return $this->belongsTo('Users');
    }

    public static function getUserTypeIdByTitle($title) {
        $type_id = Self::whereRaw('title = ?', array($title))->get(array('id'))->first();

        return $type_id->id;
    }

}
