<?php

class Recurring extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'recurrings';

    protected $fillable = array('id', 'request_service_id', 'start_date', 'end_date', 'duration', 'vendor_id','status', 'assignment_type');
}