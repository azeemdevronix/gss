<?php

class Asset extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assets';
    protected $fillable = array('id', 'asset_number', 'customer_id', 'address', 'city_id', 'state_id', 'zip', 'loan_number', 'property_type', 'lender', 'property_status', 'electric_status', 'water_status', 'gas_status', 'electric_note', 'gas_note', 'water_note', 'status', 'created_at', 'updated_at', 'property_address', 'lock_box', 'access_code', 'brokage', 'agent', 'customer_email_address', 'carbon_copy_email', 'outbuilding_shed', 'outbuilding_shed_note', 'special_direction_note', 'utility_note', 'swimming_pool' ,'occupancy_status','occupancy_status_note','UNIT','property_status_note', 'customer_type','latitude','longitude');

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function user() {
        return $this->belongsTo('User', 'customer_id');
    }

    public function state() {
        return $this->belongsTo('State', 'state_id');
    }

    public function city() {
        return $this->belongsTo('City', 'city_id');
    }

    public static function addAsset($data, $customer_id) {
        $data['status'] = 1;
        $data['customer_id'] = $customer_id;
        $asset = Asset::create($data);
        return ($asset) ? true : false;
    }

    public function maintenanceRequest() {
        return $this->hasMany('MaintenanceRequest', 'asset_id');
        //return user type
    }

    public static function editAsset($data, $id) {
        
        $data['status'] = 1;
        $updateOrder = Asset::find($id)->update($data);
        return ($updateOrder) ? true : false;
    }

    public static function deleteAsset($id) {
        $data['status'] = 0;
        $updateOrder = Asset::find($id)->update($data);
        return ($updateOrder) ? true : false;
    }

    public static function viewAssets() {
        //View all asset
        $assets_data = Asset::all();
        return $assets_data;
    }

    public static function viewAssetsByStatus($status) {
        $assets = Self::whereRaw('status = ?', array($status))->get();
        return $assets;
    }

    public static function getAssetsByCustomerId($id) {
        $assets = Self::whereRaw('customer_id = ? and status = 1', array($id))->orderBy('id', 'desc')->get();
        return $assets;
    }

    public static function getAssetInformationById($id) {
        $asset_information = Asset::find($id);
        return $asset_information;
    }

    public static function requiredFields($flag = 0) {
   
        $fields = array(
            'asset_number' =>   $flag == 0 ? 'required|unique:assets': 'required',
            'property_address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required',
            'loan_number' => '',
            'property_type' => 'required',
            'lender' => '',
            'property_status' => 'required',
            'electric_status' => 'required',
            'water_status' => 'required',
            'gas_status' => 'required',
            'lock_box' => 'required',
            'access_code' => '',
            'brokage' => 'required',
            'agent' => 'required',
            'customer_email_address' => '',
            'outbuilding_shed' => 'required',
            'outbuilding_shed_note' => 'required',
            'special_direction_note' => '',
            'utility_note' => '',
            'swimming_pool',
            'occupancy_status'=>'required'
        );

        return $fields;
    }

    public static function getLatLong($address="", $city="", $state=""){
    $fullAddress = $address." ".$city." ".$state;
    $urlAddress = str_replace(" ", "+",$fullAddress);
    $url = "http://maps.google.com/maps/api/geocode/json?address=";
    $url = $url.$urlAddress;

    $url = $url;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $result['lat'] = isset($response_a->results[0]->geometry->location->lat)? $response_a->results[0]->geometry->location->lat:"00";
    $result['lng'] = isset($response_a->results[0]->geometry->location->lng)? $response_a->results[0]->geometry->location->lng:"00";
    $result['overall'] = $response_a ;
    return $result;
}

}
