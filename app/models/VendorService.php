<?php


class VendorService extends Eloquent  {

	
        protected $table = 'vendor_services';
      
        protected $fillable = array('id', 'vendor_id', 'service_id', 'status', 'created_at' , 'updated_at');
        
        public function user() {
            return $this->belongsTo('User');
            }
        
        // Defining Function to get User Profile Status. ---- Start
        public static function addVendorServices($data)
        {
            $service = Self::create($data);
            return ($service) ? true : false;
        }
        // Defining Function to get User Profile Status. ---- End
}


