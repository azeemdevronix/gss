<?php

class Invoice extends Eloquent {

    protected $table = 'invoices';
	protected $fillable = array('order_id', 'total_amount', 'request_id', 'user_id' , 'user_type_id' , 'status');


	 public function vendor()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function customer()
    {
        return $this->belongsTo('User', 'user_id');
    }


	public function order()
	{
		  return $this->belongsTo('Order', 'order_id');
	}

	  public function maintenanceRequest()
    {
        return $this->belongsTo('MaintenanceRequest', 'request_id');
    }
	public static function listAll($userTypeId=2,$user_id="")
    {
    	if($user_id=="")
    	{
        $orders = Self::where('user_type_id','=',$userTypeId)
        ->orderBy('id', 'desc')
        ->get();
    	}
    	else
    	{
    	$orders = Self::where('user_type_id','=',$userTypeId)
    	->where('user_id','=',$user_id)
        ->orderBy('id', 'desc')
        ->get();
    	}
        return $orders;
    }

}