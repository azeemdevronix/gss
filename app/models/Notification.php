<?php

class Notification extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    protected $fillable = array('id', 'sender_id', 'recepient_id', 'message', 'notification_type_id', 'notification_url','is_read', 'created_date');

    public static function add($data) {
        $notification = Self::create($data);
        return $notification;
    }

      //Gettting notifications for login uses
    public static function getNotifications($user_id=1)
    {
        $notification =   Self::where('recepient_id','=',$user_id)
                                         ->orderBy('id', 'desc')
                                         ->skip(0)
                                         ->take(5)
                                         ->get();
        return  $notification;

    }

}
