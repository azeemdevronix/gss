<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Email {

    public static function send($to_email = array(), $subject, $template, $email_data = array()) {

        $from_email = Config::get('app.admin_email');
        $from_name = Config::get('app.from_email_name');
        Mail::send($template, $email_data, function($message) use ($from_email, $to_email, $subject, $from_name) {
            $message->to($to_email)
                    ->subject($subject)
                    ->from($from_email, $from_name);
        });
    }

}
