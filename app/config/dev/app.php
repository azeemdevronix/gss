<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Application Debug Mode
      |--------------------------------------------------------------------------
      |
      | When your application is in debug mode, detailed error messages with
      | stack traces will be shown on every error that occurs within your
      | application. If disabled, a simple generic error page is shown.
      |
     */

    'debug' => true,
    'url' => 'http://localhost/di-report/php',
    'request_images' => 'public/assets/uploads/request',
    'upload_path' => 'public/assets/uploads/',
    'admin_email' => 'azeem.devronix@gmail.com'
);
